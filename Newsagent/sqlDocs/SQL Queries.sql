# SQL Queries

SELECT 
    newsagentdb.PUBLICATION.publicationName
FROM
    newsagentdb.PUBLICATION
        INNER JOIN
    newsagentdb.SUBSCRIPTION ON newsagentdb.SUBSCRIPTION.publicationID = newsagentdb.PUBLICATION.publicationID
WHERE
    newsagentdb.SUBSCRIPTION.customerID = '36222'
        AND newsagentdb.SUBSCRIPTION.received = 'true';



SELECT 
    newsagentdb.PUBLICATION.publicationType
FROM
    newsagentdb.PUBLICATION
        INNER JOIN
    newsagentdb.SUBSCRIPTION ON newsagentdb.SUBSCRIPTION.publicationID = newsagentdb.PUBLICATION.publicationID
WHERE
    newsagentdb.SUBSCRIPTION.customerID = '36222'
        AND newsagentdb.SUBSCRIPTION.received = 'true';
        
        
SELECT 
    newsagentdb.PUBLICATION.publicationGenre
FROM
    newsagentdb.PUBLICATION
        INNER JOIN
    newsagentdb.SUBSCRIPTION ON newsagentdb.SUBSCRIPTION.publicationID = newsagentdb.PUBLICATION.publicationID
WHERE
    newsagentdb.SUBSCRIPTION.customerID = '36222'
        AND newsagentdb.SUBSCRIPTION.received = 'true';


SELECT 
    newsagentdb.PUBLICATION.price
FROM
    newsagentdb.PUBLICATION
        INNER JOIN
    newsagentdb.SUBSCRIPTION ON newsagentdb.SUBSCRIPTION.publicationID = newsagentdb.PUBLICATION.publicationID
WHERE
    newsagentdb.SUBSCRIPTION.customerID = '36222'
        AND newsagentdb.SUBSCRIPTION.received = 'true';
        
        
SELECT 
    newsagentdb.BILL.deliveryFee
FROM
    newsagentdb.BILL
        INNER JOIN
    newsagentdb.SUBSCRIPTION ON newsagentdb.BILL.customerID = newsagentdb.SUBSCRIPTION.customerID
WHERE
    newsagentdb.SUBSCRIPTION.customerID = '36222'
        AND newsagentdb.SUBSCRIPTION.received = 'true';


SELECT * FROM SUBSCRIPTION where customerID = '36222' && received = false;

SELECT * FROM SUBSCRIPTION where customerID = '12345' && received = true;

SELECT * FROM SUBSCRIPTION where customerID = '36222' && received = 'true';

SELECT 
    newsagentdb.CUSTOMER.firstName,
    newsagentdb.CUSTOMER.lastName
FROM
    newsagentdb.CUSTOMER
        INNER JOIN
    newsagentdb.SUBSCRIPTION ON newsagentdb.CUSTOMER.customerID = newsagentdb.SUBSCRIPTION.customerID
WHERE
    newsagentdb.SUBSCRIPTION.customerID = '36222'
        AND newsagentdb.SUBSCRIPTION.received = 'true';
        
        
SELECT 
    newsagentdb.BILL.billDate
FROM
    newsagentdb.BILL
    WHERE
    newsagentdb.BILL.customerID = '36222';
    
    
SELECT 
    newsagentdb.CUSTOMER.houseAddress,
    newsagentdb.CUSTOMER.streetAddress
FROM
    newsagentdb.CUSTOMER
WHERE
    newsagentdb.CUSTOMER.customerID = '36222';
    
    
SELECT 
    newsagentdb.CUSTOMER.region
FROM
    newsagentdb.CUSTOMER
WHERE
    newsagentdb.CUSTOMER.customerID = '36222';
    
    
SELECT 
    newsagentdb.CUSTOMER.email
FROM
    newsagentdb.CUSTOMER
WHERE
    newsagentdb.CUSTOMER.customerID = '36222';
    
    
SELECT
    newsagentdb.DELIVERYPERSON.firstName,
    newsagentdb.DELIVERYPERSON.lastName
FROM
    newsagentdb.SUBSCRIPTION
        INNER JOIN
    newsagentdb.DELIVERYPERSON ON newsagentdb.DELIVERYPERSON.deliveryPersonID = newsagentdb.SUBSCRIPTION.deliveryPersonID
WHERE
    newsagentdb.SUBSCRIPTION.customerID = '36222'
        AND newsagentdb.SUBSCRIPTION.received = 'true';


SELECT
    newsagentdb.DELIVERYPERSON.deliveryPersonID
FROM
    newsagentdb.SUBSCRIPTION
        INNER JOIN
    newsagentdb.DELIVERYPERSON ON newsagentdb.DELIVERYPERSON.deliveryPersonID = newsagentdb.SUBSCRIPTION.deliveryPersonID
WHERE
    newsagentdb.SUBSCRIPTION.customerID = '36222'
        AND newsagentdb.SUBSCRIPTION.received = 'true';