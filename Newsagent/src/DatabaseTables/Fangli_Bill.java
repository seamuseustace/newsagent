package DatabaseTables;

public class Fangli_Bill {
	int billID;
	int subscriptionID;
	int customerID;
	int newsagentID;
	double totalPrice;
	String billDate;
	double deliveryFee;
	public Fangli_Bill(int billID, int subscriptionID, int customerID,
			int newsagentID, double totalPrice, String billDate,
			double deliveryFee) {
		super();
		this.billID = billID;
		this.subscriptionID = subscriptionID;
		this.customerID = customerID;
		this.newsagentID = newsagentID;
		this.totalPrice = totalPrice;
		this.billDate = billDate;
		this.deliveryFee = deliveryFee;
	}
	public int getBillID() {
		return billID;
	}
	public void setBillID(int billID) {
		this.billID = billID;
	}
	public int getSubscriptionID() {
		return subscriptionID;
	}
	public void setSubscriptionID(int subscriptionID) {
		this.subscriptionID = subscriptionID;
	}
	public int getCustomerID() {
		return customerID;
	}
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}
	public int getNewsagentID() {
		return newsagentID;
	}
	public void setNewsagentID(int newsagentID) {
		this.newsagentID = newsagentID;
	}
	public double getTotalPrice() {
		return totalPrice;
	}
	public void setTotalPrice(double totalPrice) {
		this.totalPrice = totalPrice;
	}
	public String getBillDate() {
		return billDate;
	}
	public void setBillDate(String billDate) {
		this.billDate = billDate;
	}
	public double getDeliveryFee() {
		return deliveryFee;
	}
	public void setDeliveryFee(double deliveryFee) {
		this.deliveryFee = deliveryFee;
	}
	
}
