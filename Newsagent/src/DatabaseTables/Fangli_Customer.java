package DatabaseTables;

import java.util.Date;

public class Fangli_Customer {

	int customerID;
	int publicationID;
	String firstName;
	String lastName;
	String houseAddress;
	String streetAddress;
	String region;
	String holidayDate;
	String email;
	String phoneNumber;
	public Fangli_Customer(int customerID, int publicationID, String firstName,
			String lastName, String houseAddress, String streetAddress,
			String region, String holidayDate, String email, String phoneNumber) {
		super();
		this.customerID = customerID;
		this.publicationID = publicationID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.houseAddress = houseAddress;
		this.streetAddress = streetAddress;
		this.region = region;
		this.holidayDate = holidayDate;
		this.email = email;
		this.phoneNumber = phoneNumber;
	}
	

	
	

	public Fangli_Customer() {
		super();
	}

	public int getCustomerID() {
		return customerID;
	}
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}
	public int getPublicationID() {
		return publicationID;
	}
	public void setPublicationID(int publicationID) {
		this.publicationID = publicationID;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getHouseAddress() {
		return houseAddress;
	}
	public void setHouseAddress(String houseAddress) {
		this.houseAddress = houseAddress;
	}
	public String getStreetAddress() {
		return streetAddress;
	}
	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}
	public String getRegion() {
		return region;
	}
	public void setRegion(String region) {
		this.region = region;
	}
	public String getHolidayDate() {
		return holidayDate;
	}
	public void setHolidayDate(String holidayDate) {
		this.holidayDate = holidayDate;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getPhoneNumber() {
		return phoneNumber;
	}
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
