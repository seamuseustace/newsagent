package DatabaseTables;

import java.util.Date;

public class Fangli_Publication {
	int publicationID;
	String publicationType;
	String publicationName;
	String publicationGenre;
	double price;
	Date date;
	public Fangli_Publication(int publicationID, String publicationType,
			String publicationName, String publicationGenre, double price,
			Date date) {
		super();
		this.publicationID = publicationID;
		this.publicationType = publicationType;
		this.publicationName = publicationName;
		this.publicationGenre = publicationGenre;
		this.price = price;
		this.date = date;
	}
	public int getPublicationID() {
		return publicationID;
	}
	public void setPublicationID(int publicationID) {
		this.publicationID = publicationID;
	}
	public String getPublicationType() {
		return publicationType;
	}
	public void setPublicationType(String publicationType) {
		this.publicationType = publicationType;
	}
	public String getPublicationName() {
		return publicationName;
	}
	public void setPublicationName(String publicationName) {
		this.publicationName = publicationName;
	}
	public String getPublicationGenre() {
		return publicationGenre;
	}
	public void setPublicationGenre(String publicationGenre) {
		this.publicationGenre = publicationGenre;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	
}
