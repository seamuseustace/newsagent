package DatabaseTables;

import java.util.Date;

public class Fangli_Subscription {
	int subscriptionID;
	int publicationID;
	int customerID;
	int newsagentID;
	Date lastPaymentDate;
	boolean received;
	int amount;
	Date deliveryDate;
	public Fangli_Subscription(int subscriptionID, int publicationID, int customerID,
			int newsagentID, Date lastPaymentDate, boolean received,
			int amount, Date deliveryDate) {
		super();
		this.subscriptionID = subscriptionID;
		this.publicationID = publicationID;
		this.customerID = customerID;
		this.newsagentID = newsagentID;
		this.lastPaymentDate = lastPaymentDate;
		this.received = received;
		this.amount = amount;
		this.deliveryDate = deliveryDate;
	}
	public int getSubscriptionID() {
		return subscriptionID;
	}
	public void setSubscriptionID(int subscriptionID) {
		this.subscriptionID = subscriptionID;
	}
	public int getPublicationID() {
		return publicationID;
	}
	public void setPublicationID(int publicationID) {
		this.publicationID = publicationID;
	}
	public int getCustomerID() {
		return customerID;
	}
	public void setCustomerID(int customerID) {
		this.customerID = customerID;
	}
	public int getNewsagentID() {
		return newsagentID;
	}
	public void setNewsagentID(int newsagentID) {
		this.newsagentID = newsagentID;
	}
	public Date getLastPaymentDate() {
		return lastPaymentDate;
	}
	public void setLastPaymentDate(Date lastPaymentDate) {
		this.lastPaymentDate = lastPaymentDate;
	}
	public boolean isReceived() {
		return received;
	}
	public void setReceived(boolean received) {
		this.received = received;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	
}
