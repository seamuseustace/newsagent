package Fangli;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import DatabaseTables.Fangli_Bill;
import DatabaseTables.Fangli_Customer;
import DatabaseTables.Fangli_Publication;
import DatabaseTables.Fangli_Subscription;

public class Fangli {
	
	private static final String Customer = null;
	private FangliDAO temp=new FangliDAO();
	
	public Fangli() {
		
	}

	//check login
	public boolean Login(String name, String pwd) throws NewsagentException {
		boolean b=false;
		
		if (name.length() < 4) {
			throw new NewsagentException("Login length is too short");   //check the length of name
		}
		if (name.length() > 20) {
			throw new NewsagentException("Login length is too long");
		}
		if (pwd.length() < 4) {
			throw new NewsagentException("Password length is too short");  //check the length of password
		}
		if (pwd.length() > 20) {
			throw new NewsagentException("Password length is too long");
		}
		else{
			 try {
				b= temp.login(name, pwd);   //If get the boolean is false then return false
				 
			} catch (NewsagentException e) {
				e.printStackTrace();
			}
				}
		return b;
		
		}
	public  String viewOrder(int customerid) throws NewsagentException {
		List<Fangli_Subscription> orders = new ArrayList<Fangli_Subscription>();
		String str="";
		boolean b=false;		
		if(temp.getOrders(customerid)!=null){ //if get some orders from database it stands for the customer details is stored in database      
			 orders=temp.getOrders(customerid);
			 Fangli_Customer customer = temp.CustomerDetails(customerid);	
			 String strH="Customer Name: "+customer.getFirstName()+" "+customer.getLastName()+"\n"+
			 "Customer Address: "+customer.getHouseAddress()+"\n"+
			 "Customer phoneNumber: "+customer.getPhoneNumber()+"\n";
			 String h="--------------------------"+"\n";
			 str=strH+"Oders"+"\n"+h;
			
			for(Fangli_Subscription order:orders){
				 b=true; 
				 int publicationId=order.getPublicationID();
				 Fangli_Publication publication=temp.PublicationDetails(publicationId);
				 String o="Publication Name:"+publication.getPublicationName()+"\n"+
						 "Publication Type:"+publication.getPublicationType()+"\n"+
						 "Publication Genre:"+publication.getPublicationGenre()+"\n"+
						 "Delivery Date:"+order.getDeliveryDate()+"\n"+
						 "Delivery Date:"+order.getDeliveryDate()+"\n"+
						 "Publication Amout:"+order.getAmount()+"\n"+h;
				 str=str+o;  //get all orders' details and put all of them in String and return the string
			  }	  
		    }
		else throw new NewsagentException("No subscriptions in database");
		return str;   //return all orders information.
	}
		
	public String getallpaidbills()throws NewsagentException{
		List<Fangli_Subscription> bills = new ArrayList<Fangli_Subscription>();
		boolean b=false;
		String str="";
		if(temp.getAllBills()!=null){
			bills=temp.getAllBills();	
			String strH="Have been paid bills"+"\n";
			String H="--------------------"+"\n";
			str=strH+H;
			for(Fangli_Subscription bill:bills){
			b=true;
			int subscriptionID=bill.getSubscriptionID();
			Fangli_Bill pBill=temp.Bill(subscriptionID);
			int customerID=bill.getCustomerID();
			Fangli_Customer customer=temp.CustomerDetails(customerID);
			int publicationID=bill.getPublicationID();
			Fangli_Publication publication=temp.PublicationDetails(publicationID);
			Date lastPaidDate=bill.getLastPaymentDate();
			String strO="Customer Name:  "+customer.getFirstName()+"  "+customer.getLastName()+"\n"+
			"Customer address: "+customer.getHouseAddress()+"\n"+
			"Customer Phone: "+customer.getPhoneNumber()+"\n"+
			"Publication: "+publication.getPublicationName()+"\n"+
			"Publication type: "+publication.getPublicationType()+"\n"+
			"The last payment date: "+lastPaidDate+"\n"+
			"The deliveryFee: "+pBill.getDeliveryFee()+"\n"+
			"The total price: "+pBill.getTotalPrice()+"\n";
			str=str+H+strO;
			}	
		}
		else throw new NewsagentException("No bills have paid in database");
		return str;
	}
}


