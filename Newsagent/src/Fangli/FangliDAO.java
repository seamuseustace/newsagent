
package Fangli;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import DatabaseTables.Fangli_Bill;
import DatabaseTables.Fangli_Customer;
import DatabaseTables.Fangli_Publication;
import DatabaseTables.Fangli_Subscription;



public class FangliDAO{

	private static FangliDAO fangliTest = null;
	public FangliDAO() {
		super();
	}
	public static FangliDAO getInstance() {
		if (fangliTest == null) {
			fangliTest = new FangliDAO();
			return fangliTest;
		} else {
			return fangliTest;
		}
	}

	public boolean login(String login, String password) throws NewsagentException {
		Connection connection = (Connection) Utils.getConnection();
		boolean b=false;
		if (login.length() < 4) {
			throw new NewsagentException("Login length is too short");
		}
		if (login.length() > 20) {
			throw new NewsagentException("Login length is too long");
		}
		if (password.length() < 4) {
			throw new NewsagentException("Password length is too short");
		}
		if (password.length() > 20) {
			throw new NewsagentException("Password length is too long");
		}
		else {
			try {
		      PreparedStatement psmt = connection.prepareStatement("SELECT * FROM newsagent WHERE username = '" + login + "' AND userPassword = '" + password + "';");
		         ResultSet rs = psmt.executeQuery();
		         if (rs.next()) {
				 b = true;
			 } else {
				 b = false;
			 }
		} catch (Exception e) {
			System.out.println(e);   
		}
		return b;
		}
	}
	
	public List<Fangli_Subscription> getOrders(int customerid) throws NewsagentException {
		Connection connection = (Connection) Utils.getConnection();
		List<Fangli_Subscription> orders = new ArrayList<Fangli_Subscription>();
		try {
			String sql="select * from subscription where customerID=?";
			PreparedStatement psmt = connection.prepareStatement(sql);
			 psmt.setInt(1, customerid);
			ResultSet resultSet = psmt.executeQuery();
			 while (resultSet.next()) {
				 int subscriptionID = resultSet.getInt("subscriptionID");
				 int publicationID = resultSet.getInt("publicationID");
				 int customerID = resultSet.getInt("customerID");
				 int deliveryPersonID = resultSet.getInt("deliveryPersonID");
				 Date lastPaymentDate = resultSet.getDate("lastPaymentDate");
				 boolean received = resultSet.getBoolean("received");
				 int amount = resultSet.getInt("amount");
				 Date deliveryDate = resultSet.getDate("deliveryDate");			 
				 Fangli_Subscription order = new Fangli_Subscription(subscriptionID,publicationID,customerID,deliveryPersonID,lastPaymentDate,received,amount,deliveryDate);
				 orders.add(order);
			 }
		} catch (Exception e) {
			System.out.println(e);
		}
		if (orders.isEmpty()) {
			throw new NewsagentException("No subscriptions in database");
		}
		return orders;
	}
	public List<Fangli_Customer> getAllCustomers() throws NewsagentException {
		Connection connection = (Connection) Utils.getConnection();
		List<Fangli_Customer> customers = new ArrayList<Fangli_Customer>();
		try {
			PreparedStatement psmt = connection.prepareStatement("select * from customer");
			ResultSet resultSet = psmt.executeQuery();
			 while (resultSet.next()) {
				 int ID = resultSet.getInt("customerID");
				 String firstName = resultSet.getString("firstName");	
				 String lastName = resultSet.getString("lastName");	
				 int publicationID = resultSet.getInt("publicationID");
				 String houseAddress = resultSet.getString("houseAddress");
				 String streetAddress =  resultSet.getString("streetAddress");
				 String region = resultSet.getString("region");
                 String holidayDate = resultSet.getString("holidayDate");
				 String email = resultSet.getString("email");
				 String phoneNumber = resultSet.getString("phoneNumber");
				 Fangli_Customer customer = new Fangli_Customer(ID,publicationID,firstName,lastName,houseAddress,streetAddress,region,holidayDate,email,phoneNumber);
				 customers.add(customer);
			 }
		} catch (Exception e) {
			System.out.println(e);
		}
		if (customers.isEmpty()) {
			throw new NewsagentException("No customers in database");
		}
		return customers;
	}
	
	public List<Fangli_Subscription> getAllBills() throws NewsagentException {
		Connection connection = (Connection) Utils.getConnection();
		List<Fangli_Subscription> bills = new ArrayList<Fangli_Subscription>();
		try {
			PreparedStatement psmt = connection.prepareStatement("select * from subscription where received=1");
			ResultSet resultSet = psmt.executeQuery();
			 while (resultSet.next()) {
				 int subscriptionID = resultSet.getInt("subscriptionID");
				 int publicationID = resultSet.getInt("publicationID");
				 int customerID = resultSet.getInt("customerID");
				 int deliveryPersonID = resultSet.getInt("deliveryPersonID");
				 Date lastPaymentDate = resultSet.getDate("lastPaymentDate");
				 boolean received = resultSet.getBoolean("received");
				 int amount = resultSet.getInt("amount");
				 Date deliveryDate = resultSet.getDate("deliveryDate");			 
				 Fangli_Subscription order = new Fangli_Subscription(subscriptionID,publicationID,customerID,deliveryPersonID,lastPaymentDate,received,amount,deliveryDate);
				 bills.add(order);
			 }
		} catch (Exception e) {
			System.out.println(e);
		}
		if (bills.isEmpty()) {
			throw new NewsagentException("No bills have paid in database");
		}
		else return bills;
	}
	
	public Fangli_Bill Bill(int subscriptionID) throws NewsagentException {
		Connection connection = (Connection) Utils.getConnection();
		Fangli_Bill bill=null;
		try {
			PreparedStatement psmt = connection.prepareStatement("SELECT * FROM bill where subscriptionID=?");
			 psmt.setInt(1, subscriptionID);
			ResultSet resultSet = psmt.executeQuery();
			 while (resultSet.next()) {
				 int BILLID = resultSet.getInt("BILLID");
				 int customerID = resultSet.getInt("customerID");
				 int newsagentID = resultSet.getInt("newsagentID");
				 String billDate = resultSet.getString("billDate");
				 Double totalPrice = resultSet.getDouble("totalPrice");
				 Double deliveryFee = resultSet.getDouble("deliveryFee");
				 bill = new Fangli_Bill(BILLID,subscriptionID,customerID,newsagentID,totalPrice,billDate,deliveryFee);
				 
			 }
		} catch (Exception e) {
			System.out.println(e);
		}
		if (bill==null) {
			throw new NewsagentException("No bills have paid in database");
		}
		else return bill;
	}
	
	
	public Fangli_Customer CustomerDetails(int customerID) throws NewsagentException {
		Connection connection = (Connection) Utils.getConnection();
		Fangli_Customer customer = null;
		try {
			String sql="select * from customer where customerID=?";
			PreparedStatement psmt = connection.prepareStatement(sql);
			 psmt.setInt(1, customerID);
	         ResultSet rs = psmt.executeQuery();
	         while(rs.next()){
				 int ID = rs.getInt("customerID");
				 String firstName = rs.getString("firstName");	
				 String lastName = rs.getString("lastName");	
				 int publicationID = rs.getInt("publicationID");
				 String houseAddress = rs.getString("houseAddress");
				 String streetAddress =  rs.getString("streetAddress");
				 String region = rs.getString("region");
                 String holidayDate = rs.getString("holidayDate");
				 String email = rs.getString("email");
				 String phoneNumber = rs.getString("phoneNumber");
				 customer = new Fangli_Customer(ID,publicationID,firstName,lastName,houseAddress,streetAddress,region,holidayDate,email,phoneNumber);
	         }
	         } catch (Exception e) {
			System.out.println(e);
		}
		if(customer==null) {throw new NewsagentException("No customer details in database");}
		
		return customer;
	}
	
	public Fangli_Publication PublicationDetails(int PublicationId) throws NewsagentException {
		Fangli_Publication publication = null;

	      Connection connection = Utils.getConnection();
	      try {
	         String sql = "SELECT * FROM Publication WHERE PublicationId = ?";
	         PreparedStatement psmt = connection.prepareStatement(sql);
	         psmt.setInt(1, PublicationId);
	         ResultSet rs = psmt.executeQuery();
	         while (rs.next()){
	         int publicationID = rs.getInt("publicationID");
	 		 String publicationType = rs.getString("publicationType");
	 		 String publicationName = rs.getString("publicationName");
	 		String publicationGenre = rs.getString("publicationGenre");
	         Double price = rs.getDouble("price");
	         Date publish=rs.getDate("publicationDate");
	         publication = new Fangli_Publication(publicationID,publicationType,publicationName,publicationGenre,price,publish);}
	      } catch (Exception e) {
	         e.printStackTrace();
	      }
	      if(publication==null) {throw new NewsagentException("No publication details in database");}
	      return publication;
	   }
}
