package Fangli;
import java.util.List;

import DatabaseTables.Fangli_Bill;
import DatabaseTables.Fangli_Customer;
import DatabaseTables.Fangli_Publication;
import DatabaseTables.Fangli_Subscription;
import junit.framework.TestCase;

public class FangliDAOTest extends TestCase{

	protected void setUp() throws Exception {
		super.setUp();
	}
	
	    // Login Test
		// Test No: 001
		// Objective: Test length of login and password
		// Inputs: login = "yefangli", password = "111111"
		// Expected Output: true
	public void testLogin001() throws Exception {
		String login = "yefangli";
		String password = "111111";
		assertEquals(true,FangliDAO.getInstance().login(login, password));
	}
	    // Login Test
		// Test No: 002
		// Objective: Test length of login and password
		// Inputs: login = "", password = "123"
		// Expected Output: exception
		public void testLogin002() throws Exception {
			String login = "";
			String password = "123";
			try {
				FangliDAO.getInstance().login(login, password);
			   fail("Exception expected");
			} catch (NewsagentException e) {
				//assertSame("Login length is too short", e.getMessage());
				assertSame("Login length is too short", e.getMessage());
			}
		}
		// Login Test
		// Test No: 003
		// Objective: Test length of login and password
		// Inputs: login = "yefangli", password = "123"
		// Expected Output: exception
		public void testLogin003() throws Exception {
			String login = "yefangli";
			String password = "123";
			try {
				FangliDAO.getInstance().login(login, password);
				fail("Exception expected");
			} catch (NewsagentException e) {
				assertSame("Password length is too short", e.getMessage());
			}
		}
		// Login Test
		// Test No: 004
		// Objective: Test length of login and password
		// Inputs: login = "pioeeeeeeeeeeeeeeeeee", password = "123"
		// Expected Output: exception
		public void testLogin004() throws Exception {
			String login = "pioeeeeeeeeeeeeeeeeee";
			String password = "123";
			try {
				FangliDAO.getInstance().login(login, password);
				fail("Exception expected");
			} catch (NewsagentException e) {
				//assertSame("Login length is too long", e.getMessage());
				assertSame("Login length is too long", e.getMessage());
			}
		}
		        // Login Test
				// Test No: 005
				// Objective: Test length of login and password
				// Inputs: login = "yefangli", password = "123123123123123123123"
				// Expected Output: exception
				public void testLogin005() throws Exception {
					String login = "yefangli";
					String password = "12312312312312312312312";
					try {
						
						FangliDAO.getInstance().login(login, password);
						fail("Exception expected");
					} catch (NewsagentException e) {
						//assertSame("Passwaord length is too long", e.getMessage());
						assertSame("Password length is too long", e.getMessage());
					}
				}
				
				 // Login Test
				// Test No: 006
				// Objective: Test the user is not a valid user
				// Inputs: login = "Three", password = "123456"
				// Expected Output: false
				public void testLogin006() throws Exception {
						String login = "Three";
						String password = "123456";
						assertEquals(false,FangliDAO.getInstance().login(login, password));
					}
				
				
				// getOrders Test
				// Test No: 001
				// Objective: Test of possibility to get  orders from database 
				// Inputs: customerID=1
				// Expected Output: not null list with orders
				public void testGetOrders001() throws Exception {
					List<Fangli_Subscription> subscriptions = null;
					subscriptions = FangliDAO.getInstance().getOrders(1);
					assertNotNull(subscriptions);
				}
				
				// getOrders Test
				// Test No: 002
				// Objective: Test of possibility to get  orders from database 
				// Inputs: customerID=2
				// Expected Output: null list with no orders
				public void testGetOrders002() throws Exception {
					List<Fangli_Subscription> subscriptions = null;
					try {
					subscriptions = FangliDAO.getInstance().getOrders(2);
					 fail("Exception expected");
					}catch (NewsagentException e) {
						assertSame("No subscriptions in database",e.getMessage());	}			
					}
				

// getAllCustomers Test
// Test No: 001
// Objective: Test of possibility to get all customers from database
// Inputs: none
// Expected Output: not null list with customers
public void testGetAllCustomers001() throws Exception {
	List<Fangli_Customer> customers = null;
	customers = FangliDAO.getInstance().getAllCustomers();
	assertNotNull(customers);
}

//getAllCustomers Test
//Test No: 002
//Objective: Test of possibility to get all customers from database(If there are no any customer in database)
//Inputs: none
//Expected Output: null list with customers
//public void testGetAllCustomers002() throws Exception {
//	List<Customer> customers = null;
//	try{
//		customers = FangliDAO.getInstance().getAllCustomers();
//	    fail("Exception expected");
//	}catch (NewsagentException e) {
//	
//	assertSame("No customers in database",e.getMessage());
//	}
//}
//BillDetails Test
//Test No: 001
//Objective: Test of possibility to get Bill details from database 
//Inputs: subscriptionID=1
//Expected Output: not null record with Bill details
public void testBill001() throws Exception {
	Fangli_Bill bill = null;
	int subscriptionID=1;
	bill = FangliDAO.getInstance().Bill(subscriptionID);
	assertNotNull(bill);
}

//BillDetails Test
//Test No: 002
//Objective: Test of possibility to get bill details from database 
//Inputs: subscriptionID=2
//Expected Output: null details with bill details
public void testBill002() throws Exception {
	int subscriptionID=2;
	try {
	 FangliDAO.getInstance().Bill(subscriptionID);
		fail("Exception expected");
	//assertNull(customer);
	} catch (NewsagentException e) {
		assertEquals("No bills have paid in database", e.getMessage());
}
}
// CustomerDetails Test
// Test No: 001
// Objective: Test of possibility to get customer details from database 
// Inputs: customerID=1
// Expected Output: not null details with customer details
public void testCustomerDetails001() throws Exception {
	Fangli_Customer customer = null;
	customer = FangliDAO.getInstance().CustomerDetails(1);
	assertNotNull(customer);
}

// CustomerDetails Test
// Test No: 002
// Objective: Test of possibility to get customer details from database 
// Inputs: customerID=2
// Expected Output: null details with customer details
public void testCustomerDetails002() throws Exception {
	int customerID=2;
	try {
	 FangliDAO.getInstance().CustomerDetails(customerID);
		fail("Exception expected");
	//assertNull(customer);
	} catch (NewsagentException e) {
		assertEquals("No customer details in database", e.getMessage());
}
}
// PublicationDetails Test
// Test No: 001
// Objective: Test of possibility to get Publication Details from database 
// Inputs: publicationID=1
// Expected Output: not null list with publication details
public void testPublicationDetails001() throws Exception {
	Fangli_Publication publication = null;
	publication = FangliDAO.getInstance().PublicationDetails(1);
	assertNotNull(publication);
}

// PublicationDetails Test
// Test No: 001
// Objective: Test of possibility to get Publication Details from database 
// Inputs: publicationID=2
// Expected Output: null list with publication details
public void testPublicationDetails002() throws Exception {
    int publicationID = 2;
	try{
       FangliDAO.getInstance().PublicationDetails(publicationID);
   	   fail("Exception expected");
	    }
       catch (NewsagentException e) {
			assertEquals("No publication details in database", e.getMessage());
	}
	
}

//getAllBills Test
// Test No: 001
// Objective: Test of possibility to get  all bills which is payed from database 
// Inputs: none
// Expected Output: not null list with bills
public void testgetAllBills001() throws Exception {
	List<Fangli_Subscription> subscriptions = null;
	subscriptions = FangliDAO.getInstance().getAllBills();
	assertNotNull(subscriptions);
}

//getAllBills Test
//Test No: 002
//Objective: Test of possibility to get  all bills which is payed from database (If there are no any paid bills in database)
//Inputs: none
//Expected Output: null list with bills
//public void testgetAllBills002() throws Exception {
//	List<Subscription> subscriptions = null;
//	try{
//		subscriptions=FangliDAO.getInstance().getAllBills();	
//	    fail("Exception expected");
//    }catch (NewsagentException e) {
//     
//     assertSame("No bills have paid in database",e.getMessage());
//	}
//	
//}
			
}








