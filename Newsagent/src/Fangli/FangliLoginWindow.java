package Fangli;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;


import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class FangliLoginWindow {

	private JFrame frame;
	private JTextField nametextField;
	private JTextField passwordtextField_1;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					FangliLoginWindow window = new FangliLoginWindow();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public FangliLoginWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblName = new JLabel("Name:");
		lblName.setBounds(115, 89, 67, 15);
		frame.getContentPane().add(lblName);
		
		JLabel lblNewLabel = new JLabel("Password:");
		lblNewLabel.setBounds(115, 134, 67, 15);
		frame.getContentPane().add(lblNewLabel);
		
		nametextField = new JTextField();
		nametextField.setBounds(192, 86, 124, 21);
		frame.getContentPane().add(nametextField);
		nametextField.setColumns(10);
		
		passwordtextField_1 = new JTextField();
		passwordtextField_1.setBounds(197, 131, 119, 21);
		frame.getContentPane().add(passwordtextField_1);
		passwordtextField_1.setColumns(10);
		
		JButton btnLogin = new JButton("Login");
		btnLogin.setBounds(103, 181, 93, 23);
		frame.getContentPane().add(btnLogin);
		btnLogin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				
				String name=nametextField.getText();
				String password=passwordtextField_1.getText();
				Fangli logint=new Fangli();
				try {
					boolean l= logint.Login(name, password);
					if(l){
						
						FangliMainWindow window = new FangliMainWindow();
						 // System.exit(0);
						frame.setVisible(false);
						
					}
					else{
						nametextField.setText("Please make sure the name is right");
						passwordtextField_1.setText("Please make sure the password is right");
					}
				} catch (NewsagentException e1) {
					
					nametextField.setText("Please make sure the name is right");
					passwordtextField_1.setText("Please make sure the password is right");
				}
				
			}
		});
		
		JButton btnRegister = new JButton("Register");
		btnRegister.setBounds(223, 181, 93, 23);
		frame.getContentPane().add(btnRegister);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "Newsagent Management System", TitledBorder.LEADING, TitledBorder.TOP, null, SystemColor.textHighlight));
		panel.setBounds(30, 53, 381, 171);
		frame.getContentPane().add(panel);
	}
}
