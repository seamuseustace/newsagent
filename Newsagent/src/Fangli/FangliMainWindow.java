package Fangli;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextArea;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

import DatabaseTables.Fangli_Customer;
import DatabaseTables.Fangli_Subscription;

import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
public class FangliMainWindow {

	private JFrame frame;
	private JTextField customerIdtextField;
	private JTextArea textArea;

	/**
	 * Launch the application.
	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					FangliMainWindow window = new FangliMainWindow();
//					window.frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}

	/**
	 * Create the application.
	 */
	public FangliMainWindow() {
		initialize();
		this.frame.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 609, 414);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		textArea = new JTextArea();
		textArea.setBounds(202, 22, 401, 344);
		frame.getContentPane().add(textArea);
		
		JLabel lblCustomerid = new JLabel("CustomerID:");
		lblCustomerid.setBounds(20, 47, 75, 15);
		frame.getContentPane().add(lblCustomerid);
		
		customerIdtextField = new JTextField();
		customerIdtextField.setBounds(92, 44, 89, 21);
		frame.getContentPane().add(customerIdtextField);
		customerIdtextField.setColumns(10);
		
		JButton btnNewButton = new JButton("View Oders");
		btnNewButton.setForeground(SystemColor.textHighlight);
		btnNewButton.setBounds(20, 85, 161, 23);
		frame.getContentPane().add(btnNewButton);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				String t=customerIdtextField.getText();
			//	System.out.println("start1");
				if(t.equals("")){
					textArea.setText("Plaese input a correct customer id in the Customer id text area");
				}
				else{
					int id=0;
					try{
			          	id=Integer.parseInt(t);
					}catch(Exception e2){
						textArea.setText("Plaese input a correct customer id in the Customer id text area");
					}
				    if(id>=0){
				      Fangli fangli=new Fangli();
				  try {
					   String str=fangli.viewOrder(id);
					   textArea.setText(str);
					
				} catch (NewsagentException e1) {
					
					textArea.setText("The customer has no subscription in this system");
				}
				
				}
				else textArea.setText("Plaese input a correct customer id in the Customer id text area");
			}
				
			}
		});
		
		
		JLabel label = new JLabel("");
		label.setBounds(20, 200, 54, 15);
		frame.getContentPane().add(label);
		
		JButton btnNewButton_1 = new JButton("Customer Details");
		btnNewButton_1.setForeground(SystemColor.textHighlight);
		btnNewButton_1.setBounds(20, 126, 161, 23);
		frame.getContentPane().add(btnNewButton_1);
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				FangliDAO fd=new FangliDAO();
				try {
					List<Fangli_Customer> customers=(ArrayList) fd.getAllCustomers();
					String strH="All customer details"+"\n";
					String H="--------------------"+"\n";
					String str=strH+H;
					for(Fangli_Customer customer:customers){
						String strO="Customer ID: "+customer.getCustomerID()+"\n"+
					"Customer Name: "+customer.getFirstName()+" "+customer.getLastName()+"\n"+
								"Customer Address: "+customer.getHouseAddress()+" "+customer.getStreetAddress()+"\n"+
					"Customer Region: "+customer.getRegion()+"\n"+
								"Customer Email; "+customer.getEmail()+"\n"+
					"Customer Holiday: "+customer.getHolidayDate()+"\n"+
								"Customer Telephone: "+customer.getPhoneNumber()+"\n";
						str=str+H+strO;
					}
					textArea.setText(str);
				} catch (NewsagentException e1) {
					textArea.setText("No customer details in this system");
				}
				
				
				
				
			}
		});
		
		JPanel panel = new JPanel();
		panel.setForeground(SystemColor.activeCaption);
		panel.setBorder(new TitledBorder(null, "View Customer Oders", TitledBorder.LEADING, TitledBorder.TOP, null, SystemColor.textHighlight));
		panel.setBounds(10, 22, 182, 140);
		frame.getContentPane().add(panel);
		
		JButton btnNewButton_2 = new JButton("All Paid Bills");
		btnNewButton_2.setForeground(SystemColor.textHighlight);
		btnNewButton_2.setBounds(20, 223, 161, 23);
		frame.getContentPane().add(btnNewButton_2);
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {	
				
			Fangli fangli=new Fangli();
			try {
				String str1=fangli.getallpaidbills();
				if(str1.equals("")){
					textArea.setText("No paid bills in this system");
				}
				else{
					textArea.setText(str1);
				}
				
			} catch (NewsagentException e1) {
				textArea.setText("No paid bills in this system");
			}
				
				
			}
		});
	}
}
