package Fangli;
import java.util.List;

import DatabaseTables.Fangli_Customer;
import DatabaseTables.Fangli_Subscription;
import junit.framework.TestCase;
public class FangliTest extends TestCase {

	 // Login Test
	 // Test No: 001
	 // Objective: Test length of login and password
	 // Inputs: login = "yefangli", password = "111111"
	 // Expected Output: true
   public void testLogin001() throws Exception {
	 Fangli fangli=new Fangli();
	 String login = "yefangli";
	 String password = "111111";
	 assertEquals(true,fangli.Login(login, password));
   }
     // Login Test
	 // Test No: 002
	 // Objective: Test length of login and password
	 // Inputs: login = "pio", password = "123"
	 // Expected Output: exception
	public void testLogin002() throws Exception {
		Fangli fangli=new Fangli();
		String login = "pio";
		String password = "123";
		try {
			fangli.Login(login, password);
			fail("Exception expected");
		} catch (NewsagentException e) {
			//assertSame("Login length is too short", e.getMessage());
			assertSame("Login length is too short", e.getMessage());
		}
	  }
	  // Login Test
	  // Test No: 003
	  // Objective: Test length of login and password
	  // Inputs: login = "yefangli", password = "123"
	  // Expected Output: exception
	  public void testLogin003() throws Exception {
		Fangli fangli=new Fangli();
		String login = "yefangli";
		String password = "123";
		try {
			fangli.Login(login, password);
			fail("Exception expected");
		} catch (NewsagentException e) {
			assertSame("Password length is too short", e.getMessage());
		}
	  }
	  // Login Test
	  // Test No: 004
	  // Objective: Test length of login and password
	  // Inputs: login = "pioeeeeeeeeeeeeeeeeee", password = "123"
	  // Expected Output: exception
	public void testLogin004() throws Exception {
		Fangli fangli=new Fangli();
		String login = "pioeeeeeeeeeeeeeeeeee";
		String password = "123";
		try {
			fangli.Login(login, password);
			fail("Exception expected");
		} catch (NewsagentException e) {
			//assertSame("Login length is too long", e.getMessage());
			assertSame("Login length is too long", e.getMessage());
		}
	  }
	        // Login Test
			// Test No: 005
			// Objective: Test length of login and password
			// Inputs: login = "yefangli", password = "123123123123123123123"
			// Expected Output: exception
			public void testLogin005() throws Exception {
				Fangli fangli=new Fangli();
				String login = "yefangli";
				String password = "12312312312312312312312";
				try {	
					fangli.Login(login, password);
					fail("Exception expected");
				} catch (NewsagentException e) {
					//assertSame("Password length is too long", e.getMessage());
					assertSame("Password length is too long", e.getMessage());
				}
        }
			 // Login Test
			// Test No: 006
			// Objective: Test the user is not a valid user
			// Inputs: login = "Three", password = "123456"
			// Expected Output: false
			public void testLogin006() throws Exception {
				    Fangli fangli=new Fangli();
					String login = "Three";
					String password = "123456";
					assertEquals(false,fangli.Login(login, password));
				}
			
			//View order test
		    // Test No: 001
			// Object: Test of possibility to get  orders
			// Inputs: customerID=1
			// Expected Output: return string not equals null

	        public void testViewOrder001() throws Exception {
	     	// Create  object
	    	 Fangli fangli=new Fangli();
	    	 int customerid=1;
	    	 String str=fangli.viewOrder(customerid);
	    	// Test the method
	    	 assertNotNull(str);
           }
	    // View order test
        // Test No: 002
	    // Object: Test of possibility to get  orders
	    // Inputs: customerID=2
	    // Expected Output: return false

    public void testViewOrder002() throws Exception {
    	        // Create  object
    	    int customerID=2;
    		Fangli fangli=new Fangli();
    			// Test the method
    		try{
    		      fangli.viewOrder(customerID);
    		  	  fail("Exception expected");
    		}catch (NewsagentException e) {
				assertSame("No subscriptions in database",e.getMessage());			
			}
    }
    
	//View all paid bills test
    // Test No: 001
	// Object: Test of possibility to get all paid bills
	// Inputs: null
	// Expected Output: return String not null

     public void testViewPaidBills001() throws Exception {
     // Create  object
     Fangli fangli=new Fangli();
     // Test the method
     String str=fangli.getallpaidbills();
     assertNotNull(str);
     }
    //View all paid bills test
    // Test No: 002
    // Object: Test of possibility to get all paid bills(If there are no any paid bills in database)
    // Inputs: null
    // Expected Output: exception

//    public void testViewPaidBills002() throws Exception {
//        // Create  object
//	    Fangli fangli=new Fangli();
//		// Test the method
//	    try{
//	    fangli.getallpaidbills();
//		fail("Exception expected");
//	    }catch (NewsagentException e) {	
//		assertSame("No bills have paid in database",e.getMessage());	
//     }
//   }
}