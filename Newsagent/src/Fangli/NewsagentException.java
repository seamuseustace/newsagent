
package Fangli;


public class NewsagentException extends Exception {

	private String errorMessage;
	
	public NewsagentException() {
		super();
		errorMessage = "";
	}
	
	public NewsagentException(String errorMessage) {
		this.errorMessage = errorMessage;
	}
	
	public String getMessage() {
		return errorMessage;
	}
}
