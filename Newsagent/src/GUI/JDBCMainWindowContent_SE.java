package GUI;

import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;

@SuppressWarnings("serial")
public class JDBCMainWindowContent_SE extends JInternalFrame implements ActionListener {
	String cmd = null;

	// DB Connectivity Attributes
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	private Container content;

	private JPanel customerPanel;
	private JPanel exportButtonPanel;
	private JPanel exportConceptDataPanel;
	private JScrollPane publicationContentsPanel;
	private JScrollPane subscriptionContentsPanel;
	private JScrollPane billContentsPanel;
	private JScrollPane customerContentsPanel;

	private Border lineBorder;

	// Label variables
	private JLabel customerIDLabel = new JLabel("Customer ID: ");
	private JLabel receivedLabel = new JLabel("Received: ");

	// Test Field variables
	private JTextField customerIDTF = new JTextField(5);
	private JTextField receivedTF = new JTextField(10);

	private static QueryTableModel_SE PublicationTable = new QueryTableModel_SE();
	private static QueryTableModel_SE SubscriptionTable = new QueryTableModel_SE();
	private static QueryTableModel_SE BillTable = new QueryTableModel_SE();
	private static QueryTableModel_SE CustomerTable = new QueryTableModel_SE();

	// Add the models to JTabels
	private JTable PublicationContents = new JTable(PublicationTable);
	private JTable SubscriptionContents = new JTable(SubscriptionTable);
	private JTable BillContents = new JTable(BillTable);
	private JTable CustomerContents = new JTable(CustomerTable);

	// Buttons for inserting, and updating members
	// also a clear button to clear patients panel
	private JButton viewButton = new JButton("View");
	private JButton exportButton = new JButton("Export");
	private JButton clearButton = new JButton("Clear");

	public JDBCMainWindowContent_SE(String aTitle) {
		// setting up the GUI
		super(aTitle, false, false, false, false);
		setEnabled(true);

		initiate_db_conn();
		// add the 'main' panel to the Internal Frame
		content = getContentPane();
		content.setLayout(null);
		content.setBackground(Color.lightGray);
		lineBorder = BorderFactory.createEtchedBorder(15, Color.red, Color.black);

		// setup patients panel and add the components to it
		customerPanel = new JPanel();
		customerPanel.setLayout(new GridLayout(12, 2));
		customerPanel.setBackground(Color.lightGray);
		customerPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "CRUD Actions"));

		customerPanel.add(customerIDLabel);
		customerPanel.add(customerIDTF);
		customerPanel.add(receivedLabel);
		customerPanel.add(receivedTF);

		// setup patients panel and add the components to it
		exportButtonPanel = new JPanel();
		exportButtonPanel.setLayout(new GridLayout(3, 2));
		exportButtonPanel.setBackground(Color.lightGray);
		exportButtonPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Export Data"));

		exportButtonPanel.setSize(500, 200);
		exportButtonPanel.setLocation(3, 300);
		content.add(exportButtonPanel);

		viewButton.setSize(100, 25);
		exportButton.setSize(100, 25);
		clearButton.setSize(100, 25);

		viewButton.setLocation(370, 110);
		exportButton.setLocation(370, 160);
		clearButton.setLocation(370, 210);

		viewButton.addActionListener(this);
		exportButton.addActionListener(this);
		clearButton.addActionListener(this);

		content.add(viewButton);
		content.add(exportButton);
		content.add(clearButton);

		PublicationContents.setPreferredScrollableViewportSize(new Dimension(900, 200));
		SubscriptionContents.setPreferredScrollableViewportSize(new Dimension(900, 200));
		BillContents.setPreferredScrollableViewportSize(new Dimension(900, 200));
		CustomerContents.setPreferredScrollableViewportSize(new Dimension(900, 200));

		publicationContentsPanel = new JScrollPane(PublicationContents, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		publicationContentsPanel.setBackground(Color.lightGray);
		publicationContentsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Publication Database"));

		subscriptionContentsPanel = new JScrollPane(SubscriptionContents, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		subscriptionContentsPanel.setBackground(Color.lightGray);
		subscriptionContentsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Subscription Database"));

		billContentsPanel = new JScrollPane(BillContents, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		billContentsPanel.setBackground(Color.lightGray);
		billContentsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Bill Database"));

		customerContentsPanel = new JScrollPane(CustomerContents, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED,
				JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		customerContentsPanel.setBackground(Color.lightGray);
		customerContentsPanel.setBorder(BorderFactory.createTitledBorder(lineBorder, "Customer Database"));

		JScrollPane scroll1 = new JScrollPane(publicationContentsPanel);
		JScrollPane scroll2 = new JScrollPane(subscriptionContentsPanel);
		JScrollPane scroll3 = new JScrollPane(billContentsPanel);
		JScrollPane scroll4 = new JScrollPane(customerContentsPanel);

		customerPanel.setSize(360, 300);
		customerPanel.setLocation(3, 0);

		publicationContentsPanel.setSize(900, 100);
		publicationContentsPanel.setLocation(500, 0);
		scroll1.setSize(750, 100);
		scroll1.setLocation(477, 0);

		subscriptionContentsPanel.setSize(900, 150);
		subscriptionContentsPanel.setLocation(480, 110);
		scroll2.setSize(750, 150);
		scroll2.setLocation(477, 110);

		billContentsPanel.setSize(900, 100);
		billContentsPanel.setLocation(480, 270);
		scroll3.setSize(750, 100);
		scroll3.setLocation(477, 270);

		customerContentsPanel.setSize(900, 150);
		customerContentsPanel.setLocation(480, 380);
		scroll4.setSize(750, 150);
		scroll4.setLocation(477, 380);

		content.add(customerPanel);
		content.add(scroll1);
		content.add(scroll2);
		content.add(scroll3);
		content.add(scroll4);

		exportButtonPanel.setSize(470, 200);
		exportButtonPanel.setLocation(3, 320);

		setSize(990, 665);
		setVisible(true);

		PublicationTable.refreshPublicationDB(stmt);
		SubscriptionTable.refreshSubscriptionDB(stmt);
		BillTable.refreshBillDB(stmt);
		CustomerTable.refreshCustomerDB(stmt);
	}

	public void initiate_db_conn() {
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/newsagentdb";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "");
			// Create a generic statement which is passed to the
			// TestInternalFrame1
			stmt = con.createStatement();
			JOptionPane.showMessageDialog(null, "Connected to database successfully.", "Successful",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n" + e.getMessage());
			JOptionPane.showMessageDialog(null, "Failed to connect to the database.", "Error Connecting to Database",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	// event handling
	public void actionPerformed(ActionEvent e) {
		Object target = e.getSource();
		if (target == clearButton) {

			customerIDTF.setText("");
			receivedTF.setText("");

			JOptionPane.showMessageDialog(null, "All field has been successfully cleared.", "Successful",
					JOptionPane.INFORMATION_MESSAGE);
		}

		if (target == viewButton) {
			try {

				stmt.addBatch("SELECT * FROM SUBSCRIPTION WHERE customerID = '" + customerIDTF + "' && received = "
						+ receivedTF + ";");

				stmt.addBatch("");

				int[] results = stmt.executeBatch();

				String cid = customerIDTF.getText();
				String rec = receivedTF.getText();

				JOptionPane.showMessageDialog(null, "Customer ID: " + cid + " has been successfully opened.",
						"Successful", JOptionPane.INFORMATION_MESSAGE);

			} catch (SQLException sqle) {
				System.err.println("Error with insert:\n" + sqle.toString());
				JOptionPane.showMessageDialog(null, "Please complete all patient information.", "Error Updating",
						JOptionPane.ERROR_MESSAGE);
			} finally {
				PublicationTable.refreshPublicationDB(stmt);
				SubscriptionTable.refreshSubscriptionDB(stmt);
			}
		}

		if (target == this.exportButton) {

			String customerName = "SELECT newsagentdb.CUSTOMER.firstName, newsagentdb.CUSTOMER.lastName"
					+ " FROM newsagentdb.CUSTOMER"
					+ " INNER JOIN newsagentdb.SUBSCRIPTION on newsagentdb.CUSTOMER.customerID = newsagentdb.SUBSCRIPTION.customerID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerIDTF.getText() + "'"
					+ " and newsagentdb.SUBSCRIPTION.received = '" + receivedTF.getText() + "';";

			String billDate = "SELECT newsagentdb.BILL.billDate"
					+ " FROM newsagentdb.BILL"
					+ " WHERE newsagentdb.BILL.customerID = '" + customerIDTF.getText() + "';";

			String customerAddress = "SELECT newsagentdb.CUSTOMER.houseAddress, newsagentdb.CUSTOMER.streetAddress"
					+ " FROM newsagentdb.CUSTOMER"
					+ " WHERE newsagentdb.CUSTOMER.customerID = '" + customerIDTF.getText() + "';";

			String customerRegion = "SELECT newsagentdb.CUSTOMER.region"
					+ " FROM newsagentdb.CUSTOMER"
					+ " WHERE"
					+ " newsagentdb.CUSTOMER.customerID = '" + customerIDTF.getText() + "';";

			String customerEmail = "SELECT newsagentdb.CUSTOMER.email"
					+ " FROM newsagentdb.CUSTOMER"
					+ " WHERE"
					+ " newsagentdb.CUSTOMER.customerID = '" + customerIDTF.getText() + "';";

			String customerPhone = "SELECT newsagentdb.CUSTOMER.phoneNumber"
					+ " FROM newsagentdb.CUSTOMER"
					+ " WHERE"
					+ " newsagentdb.CUSTOMER.customerID = '" + customerIDTF.getText() + "';";

			String deliverPerson = "SELECT newsagentdb.DELIVERYPERSON.firstName, newsagentdb.DELIVERYPERSON.lastName"
					+ " FROM newsagentdb.SUBSCRIPTION"
					+ " INNER JOIN newsagentdb.DELIVERYPERSON"
					+ " ON newsagentdb.DELIVERYPERSON.deliveryPersonID = newsagentdb.SUBSCRIPTION.deliveryPersonID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerIDTF.getText() + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + receivedTF.getText() + "';";

			String deliverPersonNo = "SELECT newsagentdb.DELIVERYPERSON.deliveryPersonID"
					+ " FROM newsagentdb.SUBSCRIPTION"
					+ " INNER JOIN newsagentdb.DELIVERYPERSON"
					+ " ON newsagentdb.DELIVERYPERSON.deliveryPersonID = newsagentdb.SUBSCRIPTION.deliveryPersonID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerIDTF.getText() + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + receivedTF.getText() + "';";

			String pubName = "SELECT newsagentdb.PUBLICATION.publicationName"
					+ " FROM newsagentdb.PUBLICATION"
					+ " INNER JOIN newsagentdb.SUBSCRIPTION"
					+ " ON newsagentdb.SUBSCRIPTION.publicationID = newsagentdb.PUBLICATION.publicationID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerIDTF.getText() + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + receivedTF.getText() + "';";

			String pubType = "SELECT newsagentdb.PUBLICATION.publicationType"
					+ " FROM newsagentdb.PUBLICATION"
					+ " INNER JOIN newsagentdb.SUBSCRIPTION"
					+ " ON newsagentdb.SUBSCRIPTION.publicationID = newsagentdb.PUBLICATION.publicationID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerIDTF.getText() + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + receivedTF.getText() + "';";

			String pubGenre = "SELECT newsagentdb.PUBLICATION.publicationGenre"
					+ " FROM newsagentdb.PUBLICATION"
					+ " INNER JOIN newsagentdb.SUBSCRIPTION"
					+ " ON newsagentdb.SUBSCRIPTION.publicationID = newsagentdb.PUBLICATION.publicationID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerIDTF.getText() + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + receivedTF.getText() + "';";

			String pubPrice = "SELECT newsagentdb.PUBLICATION.price"
					+ " FROM newsagentdb.PUBLICATION"
					+ " INNER JOIN newsagentdb.SUBSCRIPTION"
					+ " ON newsagentdb.SUBSCRIPTION.publicationID = newsagentdb.PUBLICATION.publicationID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerIDTF.getText() + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + receivedTF.getText() + "';";

			String deliverDate = "SELECT newsagentdb.SUBSCRIPTION.deliveryDate"
					+ " FROM newsagentdb.SUBSCRIPTION"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerIDTF.getText() + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + receivedTF.getText() + "';";

			String deliverFee = "SELECT newsagentdb.BILL.deliveryFee"
					+ " FROM newsagentdb.BILL"
					+ " INNER JOIN newsagentdb.SUBSCRIPTION"
					+ " ON newsagentdb.BILL.customerID = newsagentdb.SUBSCRIPTION.customerID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerIDTF.getText() + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + receivedTF.getText() + "';";

			String totPrice = "SELECT newsagentdb.BILL.totalPrice"
					+ " FROM newsagentdb.BILL"
					+ " INNER JOIN newsagentdb.SUBSCRIPTION"
					+ " ON newsagentdb.BILL.customerID = newsagentdb.SUBSCRIPTION.customerID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerIDTF.getText() + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + receivedTF.getText() + "';";

			try {
				rs = stmt.executeQuery(customerName);
				rs = stmt.executeQuery(billDate);
				rs = stmt.executeQuery(customerAddress);
				rs = stmt.executeQuery(customerRegion);
				rs = stmt.executeQuery(customerEmail);
				rs = stmt.executeQuery(customerPhone);
				rs = stmt.executeQuery(deliverPerson);
				rs = stmt.executeQuery(deliverPersonNo);
				rs = stmt.executeQuery(pubName);
				rs = stmt.executeQuery(pubType);
				rs = stmt.executeQuery(pubGenre);
				rs = stmt.executeQuery(pubPrice);
				rs = stmt.executeQuery(deliverDate);
				rs = stmt.executeQuery(deliverFee);
				rs = stmt.executeQuery(totPrice);
				
				writeToFile(rs);
				JOptionPane.showMessageDialog(null, "Customer information has been successfully exported.", "Successful",
						JOptionPane.INFORMATION_MESSAGE);
			} catch (Exception e1) {
				e1.printStackTrace();
				JOptionPane.showMessageDialog(null, "An error occured while exporting patient information.",
						"Error Exporting", JOptionPane.ERROR_MESSAGE);
			}
		}

	}

	private void writeToFile(ResultSet rs) {
		try {
			FileWriter outputFile = new FileWriter("SeamusEustace_A00167350.csv");
			PrintWriter printWriter = new PrintWriter(outputFile);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			for (int i = 0; i < numColumns; i++) {
				printWriter.print(rsmd.getColumnLabel(i + 1) + ",");
			}
			printWriter.print("\n");
			while (rs.next()) {
				for (int i = 0; i < numColumns; i++) {
					printWriter.print(rs.getString(i + 1) + ",");
				}
				printWriter.print("\n");
				printWriter.flush();
			}
			printWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "An error occured while exporting patient information.",
					"Error Exporting", JOptionPane.ERROR_MESSAGE);
		}
	}
}