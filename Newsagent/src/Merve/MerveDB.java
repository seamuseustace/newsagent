package Merve;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MerveDB {

	Connection con;
	Statement stat = null;
	ResultSet rs = null;

	
	// connect the database
		

	public boolean connect() throws MerveNewsagentExceptionHandler {
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/newsagent";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "123456");

			stat = con.createStatement();
			return true;
			
		} catch (Exception e) {
			throw new MerveNewsagentExceptionHandler("failed to connect");
		}

	}

	// *******************************************************//
	// ------------NEW 5---------------//
	
		//As a Newsagent
		// I want to 
		//create a bill account for each customer
		//So I can classify the bills for delivery every month.

	// *******************************************************//
	
	

	// Requirement - To add valid customer firstname,customer lastname , valid
	// delivery date,delivery fee and total amount and firstly check if there is
	// this customer in newsagent or no and then check the is there given delivery date for valid customer or no.
	//and then create bill(insert) for valid customer
	// 
	
	// For invalid input values, generate an appropriate error message.

	public boolean CreateBill(String cfname, String clname, Double deliveryfee, String deliverydate, Integer totalamount)
			throws MerveNewsagentExceptionHandler {
		try {
			//connect the database 
			connect();
			// take the current date for bill date
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
			Date date = new Date();

			String billdate = (dateFormat.format(date));
			if (CheckCustomer(cfname, clname)) {
				if (CheckDeliveryDate(deliverydate)) {

					String insert = "INSERT INTO bill SELECT null,subscription.subscriptionID,subscription.customerID,1,"
							+ deliveryfee + ",'" + billdate + "'," + totalamount
							+ " FROM customer,subscription where customer.firstName='" + cfname
							+ "'and customer.lastName='" + clname + "' and subscription.deliveryDate='" + deliverydate
							+ "' and customer.customerID=subscription.customerID ;";

					stat.executeUpdate(insert);
					return true;
				}
			} else
				throw new MerveNewsagentExceptionHandler("Please provide valid input");

		} catch (SQLException sqle) {
			
			throw new MerveNewsagentExceptionHandler("Fee can not be null");
			
		}
		return false;

	}

	// Requirement - To check customer firstname,customer lastname and if there
	// is customer return true.
	
	// For invalid input values, generate an appropriate error message.

	public boolean CheckCustomer(String fname, String lname) throws MerveNewsagentExceptionHandler {

		try {
			//connect the database 
			connect();
			String updateTemp = "select customerID from customer " + "where firstName = '" + fname + "' and lastName= '"
					+ lname + "'";

			rs = stat.executeQuery(updateTemp);

			if (rs.next()) {

				return true;

			}

			else
				throw new MerveNewsagentExceptionHandler("Please provide valid customername");

		}

		catch (SQLException sqle) {
			System.err.println("Error with searching:\n" + sqle.toString());
			return false;
		}

	}

	// Requirement - To check deliverydate and if there is deliverydate return
	// true.
	
	// For invalid input values, generate an appropriate error message.

	public boolean CheckDeliveryDate(String deliverydate) throws MerveNewsagentExceptionHandler {

		try {
			//connect the database 
			connect();
			String updateTemp = "select deliveryDate from subscription " + "where deliveryDate='" + deliverydate + "';";

			rs = stat.executeQuery(updateTemp);

			if (rs.next()) {

				return true;

			}

			else
				throw new MerveNewsagentExceptionHandler("Please provide valid deliverydate");

		}

		catch (SQLException sqle) {
			System.err.println("Error with searching:\n" + sqle.toString());
			return false;
		}

	}
	// --------------NEW11--------------//
		
		//As a
		//Newsagent
		//I want to 
		//search what kind of publication the customer have received on that given date
		//So I can 
		//know more about the customers interest.

	// ------------------------------//
	// ------------------------------//

		// Requirement -Firstly to check customer firstname,lastname and if there is
		// customer ,check deliverydate month and then search publication in the
		//  given  date
		
		// For invalid input values, generate an appropriate error message.

		public boolean CheckPublicationGenre(String cfname, String clname, String date1)
				throws MerveNewsagentExceptionHandler {

			try {
				//connect the database 
				connect();
				if (CheckCustomer(cfname, clname)) {
					if(CheckDeliveryDateRange(date1)) {
					
						String genre = "SELECT subscription.publicationID,deliveryDate,publicationName,publicationGenre,publicationType from subscription,publication,customer"
								+ " WHERE subscription.publicationID=publication.publicationID and MONTH(deliveryDate) = '"
								+ date1 + "' and firstName='" + cfname + "' and lastName='" + clname
								+ "';";

						rs = stat.executeQuery(genre);

						if (rs.next()) {
								return true;
						}
					}
				
				}
				else
					throw new MerveNewsagentExceptionHandler("Please provide valid date ");

			}

			catch (SQLException sqle) {
				System.err.println("Error with searching:\n" + sqle.toString());

			}
			return false;

		}

		// Requirement - To check deliverydate month and if there is deliverydate
		// return true.
		
		// For invalid input values, generate an appropriate error message.
		public boolean CheckDeliveryDateRange(String date1) throws MerveNewsagentExceptionHandler {

			try {
				//connect the database 
				connect();
				String updateTemp = "select deliveryDate from subscription " + "where MONTH(deliveryDate)=  '" + date1
						+ "' ;";

				rs = stat.executeQuery(updateTemp);

				if (rs.next()) {

					return true;

				}

				else
					throw new MerveNewsagentExceptionHandler("Please provide valid date ");

			}

			catch (SQLException sqle) {
				System.err.println("Error with searching:\n" + sqle.toString());
				return false;
			}

		}



	

}