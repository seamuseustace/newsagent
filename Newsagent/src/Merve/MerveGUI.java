package Merve;

import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;


public class MerveGUI {
	private JFrame frame;
	private JTextField txtFirstName;
	private JTextField txtLastName;
	private JTextField txtDeliveryFee;
	private JTextField txtTotalAmount;
	// DB Connectivity Attributes
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	String cmd = null;
	private JTextField txtDeliveryDate;
	private static MerveQueryTableModel TableModel = new MerveQueryTableModel();
	private JTable table = new JTable(TableModel);

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MerveGUI window = new MerveGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MerveGUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		initiate_db_conn();
		frame = new JFrame();
		frame.setBounds(100, 100, 746, 301);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblFirstName = new JLabel("First Name:");
		lblFirstName.setBounds(10, 39, 71, 14);
		frame.getContentPane().add(lblFirstName);
		
		JLabel lblLastName = new JLabel("Last Name:");
		lblLastName.setBounds(10, 64, 71, 14);
		frame.getContentPane().add(lblLastName);
		
		JLabel lblDeliveryFee = new JLabel("Delivery Fee:");
		lblDeliveryFee.setBounds(10, 89, 82, 14);
		frame.getContentPane().add(lblDeliveryFee);
		
		JLabel lblTotalAmount = new JLabel("Total Amount:");
		lblTotalAmount.setBounds(10, 158, 91, 14);
		frame.getContentPane().add(lblTotalAmount);
		
		txtFirstName = new JTextField();
		txtFirstName.setBounds(111, 36, 86, 20);
		frame.getContentPane().add(txtFirstName);
		txtFirstName.setColumns(10);
		
		txtLastName = new JTextField();
		txtLastName.setColumns(10);
		txtLastName.setBounds(111, 61, 86, 20);
		frame.getContentPane().add(txtLastName);
		
		txtDeliveryFee = new JTextField();
		txtDeliveryFee.setColumns(10);
		txtDeliveryFee.setBounds(111, 89, 86, 20);
		frame.getContentPane().add(txtDeliveryFee);
		
		txtTotalAmount = new JTextField();
		txtTotalAmount.setColumns(10);
		txtTotalAmount.setBounds(111, 155, 86, 20);
		frame.getContentPane().add(txtTotalAmount);
		
		txtDeliveryDate = new JTextField();
		txtDeliveryDate.setBounds(111, 120, 86, 20);
		frame.getContentPane().add(txtDeliveryDate);
		txtDeliveryDate.setColumns(10);
		
		
		final JComboBox comboBox = new JComboBox();
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Month", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}));
		comboBox.setBounds(187, 229, 91, 20);
		frame.getContentPane().add(comboBox);
		
		JScrollPane scrollPane = new JScrollPane(table,JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(236, 12, 484, 213);
		frame.getContentPane().add(scrollPane);
	
		table.setPreferredScrollableViewportSize(new Dimension(900, 300));
		
		
		JButton btnCreateBill = new JButton("Create Bill");
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		final String billdate = (dateFormat.format(date));
		btnCreateBill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					if(CheckCustomer(txtFirstName.getText(), txtLastName.getText())){
						if (CheckDeliveryDate(txtDeliveryDate.getText())) {
					String insert = "INSERT INTO bill SELECT null,subscription.subscriptionID,subscription.customerID,1,"
							+ txtDeliveryFee.getText() + ",'" + billdate + "'," + txtTotalAmount.getText()
							+ " FROM customer,subscription where customer.firstName='" + txtFirstName.getText()
							+ "'and customer.lastName='" + txtLastName.getText() + "' and subscription.deliveryDate='" + txtDeliveryDate.getText()
							+ "' and customer.customerID=subscription.customerID ;";

					stmt.executeUpdate(insert);
					JOptionPane.showMessageDialog(null, "Successfully created.", "Successful",
							JOptionPane.INFORMATION_MESSAGE);
				
					}
						
					}
					

				} catch (SQLException sqle) {
					System.err.println("Error with  insert:\n" + sqle.toString());
				} 
				
			}
		});
		btnCreateBill.setBounds(10, 194, 167, 23);
		frame.getContentPane().add(btnCreateBill);
		
		
		
		JLabel lblDeliveryDate = new JLabel("Delivery Date:");
		lblDeliveryDate.setBounds(10, 123, 92, 14);
		frame.getContentPane().add(lblDeliveryDate);
		
		JLabel lblBll = new JLabel("BILL");
		lblBll.setFont(new Font("Comic Sans MS", Font.BOLD, 13));
		lblBll.setBounds(10, 11, 71, 14);
		frame.getContentPane().add(lblBll);
		
		JButton btnShowCustomerInterest = new JButton("Customer Publication");
		btnShowCustomerInterest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(CheckCustomer(txtFirstName.getText(), txtLastName.getText())){
					
				String month=comboBox.getSelectedItem().toString();
				
				 if(month.equals("January")){
					month="1";
				}
				
				
				else if(month.equals("February")){
					month="2";
				}
				
				
				else if(month.equals("March")){
					month="3";
				}
				else if(month.equals("April")){
					month="4";
				}
				else if(month.equals("May")){
					month="5";
				}
				else if(month.equals("June")){
					month="6";
				}
				else if(month.equals("July")){
					month="7";
				}
				else if(month.equals("August")){
					month="8";
				}
				else if(month.equals("September")){
					month="9";
				}
				else if(month.equals("October")){
					month="10";
				}
				else if(month.equals("November")){
					month="11";
				}
				else if(month.equals("December")){
					month="12";
				}
				 if(CheckDeliveryDateRange(month)){
				TableModel.refreshFromDB(stmt,txtFirstName.getText(),txtLastName.getText(),month);
				
					}
				}
			}
		});
		btnShowCustomerInterest.setBounds(10, 228, 167, 23);
		frame.getContentPane().add(btnShowCustomerInterest);
		
	
		
		
		
	}
	public boolean CheckCustomer(String fname, String lname)  {

		try {
			String updateTemp = "select customerID from customer " + "where firstName = '" + fname + "' and lastName= '"
					+ lname + "'";

			rs = stmt.executeQuery(updateTemp);

			if (rs.next()) {

				return true;

			}else
				JOptionPane.showMessageDialog(null, "Invalid customer.", "Error",
						JOptionPane.ERROR_MESSAGE);


			
				
		}

		catch (SQLException sqle) {
			System.err.println("Error with searching:\n" + sqle.toString());
			return false;
		}
		return false;

	}
	public boolean CheckDeliveryDate(String deliverydate)  {

		try {
			String updateTemp = "select deliveryDate from subscription " + "where deliveryDate='" + deliverydate + "';";

			rs = stmt.executeQuery(updateTemp);

			if (rs.next()) {

				return true;

			}
			else
				JOptionPane.showMessageDialog(null, "Invalid date.", "Error",
						JOptionPane.ERROR_MESSAGE);
			

		}

		catch (SQLException sqle) {
			System.err.println("Error with searching:\n" + sqle.toString());
			return false;
		}
		return false;

	}

	public boolean CheckDeliveryDateRange(String date1)  {

		try {
			String updateTemp = "select deliveryDate from subscription " + "where MONTH(deliveryDate)=  '" + date1
					+ "' ;";

			rs = stmt.executeQuery(updateTemp);

			if (rs.next()) {

				return true;

			}else
				JOptionPane.showMessageDialog(null, "Invalid date.", "Error",
						JOptionPane.ERROR_MESSAGE);

			

		}

		catch (SQLException sqle) {
			System.err.println("Error with searching:\n" + sqle.toString());
			return false;
		}
		return false;

	}
	public void initiate_db_conn() {
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/newsagent";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "123456");
			// Create a generic statement which is passed to the
			// TestInternalFrame1
			stmt = con.createStatement();
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n" + e.getMessage());
		}
	}
}
