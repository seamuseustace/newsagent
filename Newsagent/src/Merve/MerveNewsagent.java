package Merve;

import java.sql.SQLException;

public class MerveNewsagent {
	MerveDB database = new MerveDB();

	public MerveNewsagent() {

	}

	// *******************************************************//
	// ------------NEW 5---------------//
		
		//As a Newsagent
		// I want to 
		//create a bill account for each customer
		//So I can classify the bills for delivery every month.
	
	// *******************************************************//

	// Requirement - Connect with database. To add valid customer
	// firstname,customer lastname , valid
	// delivery date,delivery fee and total amount and firstly check if there is
	// this customer or no  and then check the is there given delivery date for valid customer or no.
	//and then create bill for valid customer
	//
	// For invalid input values, generate an appropriate error message.

	public boolean createbill(String cfname, String clname, Double deliveryfee, String deliverydate, Integer totalamount)
			throws MerveNewsagentExceptionHandler {

		database.connect();
		if (database.CheckCustomer(cfname, clname)) {
			if (database.CheckDeliveryDate(deliverydate)) {
				database.CreateBill(cfname, clname, deliveryfee, deliverydate, totalamount);
				return true;
			}
		}

		else
			throw new MerveNewsagentExceptionHandler("Please provide valid input");

		return false;

	}

	// Requirement - Connect with database and to check customer
	// firstname,customer lastname and if there is customer return true.
	// For invalid input values, generate an appropriate error message.

	public boolean CheckCustomer(String fname, String lname) throws MerveNewsagentExceptionHandler {
		database.connect();
		if (database.CheckCustomer(fname, lname))
			return true;
		else
			throw new MerveNewsagentExceptionHandler("Please provide valid customername");

	}

	// Requirement - Connect with database to check deliverydate and if there is
	// deliverydate return true.
	// For invalid input values, generate an appropriate error message.

	public boolean CheckDeliveryDate(String deliverydate) throws MerveNewsagentExceptionHandler {

		database.connect();
		if (database.CheckDeliveryDate(deliverydate))
			return true;
		else
			throw new MerveNewsagentExceptionHandler("Please provide valid deliverydate");

	}
	
	//---------------NEW 11-----------------//
			//As a
			//Newsagent
			//I want to 
			//search what kind of publication the customer have received on that given date
			//So I can 
			//know more about the customers interest.
	
		//-------------------------------------//

		// Requirement - Connect with database to check customername and if there is
		// customer and then search the publication genre in given month date
		// For invalid input values, generate an appropriate error message.
		public boolean CheckPublicationGenre(String cfname, String clname, String date1)
				throws MerveNewsagentExceptionHandler {

			database.connect();
			if (database.CheckCustomer(cfname, clname)) {

				database.CheckPublicationGenre(cfname, clname, date1);
				return true;

			}

			else
				throw new MerveNewsagentExceptionHandler("Please provide valid input");

		}

		// Requirement - Connect with database to check deliverydate month and if there is
		// deliverydate return true.
		// For invalid input values, generate an appropriate error message.

		public boolean CheckDeliveryDateRange(String deliverydate1) throws MerveNewsagentExceptionHandler {

			database.connect();
			if (database.CheckDeliveryDateRange(deliverydate1))
				return true;
			else
				throw new MerveNewsagentExceptionHandler("Please provide valid date ");

		}

	

}
