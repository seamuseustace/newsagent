import junit.framework.TestCase;

public class MerveDBTest extends TestCase {

	// Test No:1
	// Test Objective: test connection with MySql
	// excepted output:True

	public void testDB() {
		MerveDB db = new MerveDB();
		try {

			assertEquals(db.connect(), true);

		} catch (MerveNewsagentExceptionHandler e) {

			assertSame("failed to connect", e.getMessage());
		}

	}

	// Test No: 2
	// Obj: Provide valid customer firstname and customer
	// lastname,deliveryfee,deliverydate and total amount
	// Inputs: customer firstname=merve ,customer lastname=sayar,delivery
	// fee=0.5,delivery date=2016-03-01 ,total amount=2
	// Expected Output: true

	public void testCreateBill001() {
		// Create DB object
		MerveDB db = new MerveDB();

		// Test the method
		try {

			assertEquals(db.CreateBill("merve", "sayar", 0.5, "2016-03-01", 2), true);

		} catch (MerveNewsagentExceptionHandler e1) {
			fail("Should not get here ...");
		}
	}

	// Test No: 3
	// Obj: Provide invalid customer firstname,valid customer
	// lastname,deliveryfee,deliverydate and total amount
	// Inputs: customer firstname=m ,customer lastname=sayar,delivery
	// fee=0.5,delivery date=2016-03-01 ,total amount=2
	// Expected Output: Exception thrown with message "Please provide valid
	// customername"
	public void testCreateBill002() {
		// Create DB object
		MerveDB db = new MerveDB();

		// Test the method
		try {

			db.CreateBill("m", "sayar", 0.5, "2016-03-01", 2);
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			
			assertSame("Please provide valid customername", e.getMessage());
		}
		
			
		}
	

	// Test No: 4
	// Obj: Provide valid customer firstname,invalid customer lastname,valid
	// deliveryfee,deliverydate and total amount
	// Inputs: customer firstname=merve ,customer lastname=s,delivery
	// fee=0.5,delivery date=2016-03-01 ,total amount=2
	// Expected Output: Exception thrown with message "Please provide valid
	// customername"
	public void testCreateBill003() {
		// Create DB object
		MerveDB db = new MerveDB();

		// Test the method
		try {

			db.CreateBill("merve", "s", 0.5, "2016-03-01", 2);
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid customername", e.getMessage());
		}
	}

	// Test No: 5
	// Obj: Provide valid customer firstname,valid customer lastname,valid
	// deliveryfee,invalid deliverydate and total amount
	// Inputs: customer firstname=merve ,customer lastname=sayar,delivery
	// fee=0.5,delivery date=1994-03-01 ,total amount=2
	// Expected Output: Exception thrown with message "Please provide valid
	// deliverydate"
	public void testCreateBill004() {
		// Create DB object
		MerveDB db = new MerveDB();

		// Test the method
		try {

			db.CreateBill("merve", "sayar", 0.5, "1994-03-01", 2);
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid deliverydate", e.getMessage());
		}
	}

	// Test No: 6
	// Obj: Provide invalid customer firstname,invalid customer lastname,valid
	// deliveryfee,deliverydate and total amount
	// Inputs: customer firstname=m ,customer lastname=s,delivery
	// fee=0.5, delivery date=2016-03-01 ,total amount=2
	// Expected Output: Exception thrown with message "Please provide valid
	// customername"
	public void testCreateBill005() {
		// Create DB object
		MerveDB db = new MerveDB();

		// Test the method
		try {

			db.CreateBill("m", "s", 0.5, "2016-03-01", 2);
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid customername", e.getMessage());
		}
	}
	
	// Test No: 7
	// Obj: Provide valid customer firstname,valid customer lastname,valid
	// deliveryfee,deliverydate and invalid total amount
	// Inputs: customer firstname=merve ,customer lastname=sayar,delivery
	// fee=0.5, delivery date=2016-03-01 ,total amount=null
	// Expected Output: Exception thrown with message "Fee can not be null"
	
	
	
		public void testCreateBill006() {
			// Create DB object
			MerveDB db = new MerveDB();

			// Test the method
			try {

				db.CreateBill("merve", "sayar", 0.5, "2016-03-01",null );
				fail("Exception expected .....");

			} catch (MerveNewsagentExceptionHandler e) {
				assertSame("Fee can not be null", e.getMessage());
			}
		}
	

	
	
		// Test No: 8
		// Obj: Provide valid customer firstname,valid customer lastname,invalid
		// deliveryfee,valid deliverydate and valid total amount
		// Inputs: customer firstname=merve ,customer lastname=sayar,delivery
		// fee=null, delivery date=2016-03-01 ,total amount=1
		// Expected Output: Exception thrown with message "Fee can not be null"
		
		
		
			public void testCreateBill007() {
				// Create DB object
				MerveDB db = new MerveDB();

				// Test the method
				try {

					db.CreateBill("merve", "sayar", null, "2016-03-01",1 );
					fail("Exception expected .....");

				} catch (MerveNewsagentExceptionHandler e) {
					assertSame("Fee can not be null", e.getMessage());
				}
			}
			// Test No: 9
			// Obj: Provide valid customer firstname,valid customer lastname,invalid
			// deliveryfee,valid deliverydate and invalid total amount
			// Inputs: customer firstname=merve ,customer lastname=sayar,delivery
			// fee=null, delivery date=2016-03-01 ,total amount=null
			// Expected Output: Exception thrown with message "Fee can not be null"
			
			
			
				public void testCreateBill008() {
					// Create DB object
					MerveDB db = new MerveDB();

					// Test the method
					try {

						db.CreateBill("merve", "sayar", null, "2016-03-01",null );
						fail("Exception expected .....");

					} catch (MerveNewsagentExceptionHandler e) {
						assertSame("Fee can not be null", e.getMessage());
					}
				}

	// Test No: 10
	// Obj: Provide valid customer firstname,valid customer lastname
	// Inputs: customer firstname=merve ,customer lastname=sayar
	// Expected Output: true
	public void testCheckCustomer001() {
		try {
			// Create DB object
			MerveDB db = new MerveDB();

			assertEquals(db.CheckCustomer("merve", "sayar"), true);

		} catch (MerveNewsagentExceptionHandler e1) {
			fail("Should not get here ...");
		}
	}

	// Test No: 11
	// Obj: Provide valid customer firstname,invalid customer lastname
	// Inputs: customer firstname=merve ,customer lastname=s
	// Expected Output: Exception thrown with message "Please provide valid
	// customername"
	public void testCheckCustomer002() {
		try {
			// Create DB object
			MerveDB db = new MerveDB();

			db.CheckCustomer("merve", "s");
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid customername", e.getMessage());
		}
	}

	// Test No: 12
	// Obj: Provide invalid customer firstname,valid customer lastname
	// Inputs: customer firstname=m ,customer lastname=sayar
	// Expected Output: Exception thrown with message "Please provide valid
	// customername"
	public void testCheckCustomer003() {
		try {
			// Create DB object
			MerveDB db = new MerveDB();

			db.CheckCustomer("m", "sayar");
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid customername", e.getMessage());
		}
	}

	// Test No: 13
	// Obj: Provide invalid customer firstname,invalid customer lastname
	// Inputs: customer firstname=m ,customer lastname=s
	// Expected Output: Exception thrown with message "Please provide valid
	// customername"
	public void testCheckCustomer004() {
		try {
			// Create DB object
			MerveDB db = new MerveDB();

			db.CheckCustomer("m", "s");
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid customername", e.getMessage());
		}
	}

	// Test No: 14
	// Obj: Provide valid deliverydate
	// Inputs: deliverydate=2016-03-01
	// Expected Output: true
	public void testCheckDeliveryDate001() {
		try {
			// Create DB object
			MerveDB db = new MerveDB();

			assertEquals(db.CheckDeliveryDate("2016-03-01"), true);

		} catch (MerveNewsagentExceptionHandler e1) {
			fail("Should not get here ...");
		}
	}

	// Test No: 15
	// Obj: Provide invalid deliverydate
	// Inputs: deliverydate=1994-03-01
	// Expected Output: Exception thrown with message "Please provide valid
	// deliverydate"
	public void testCheckDeliveryDate002() {
		try {
			// Create DB object
			MerveDB db = new MerveDB();

			db.CheckDeliveryDate("1994-03-01");
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid deliverydate", e.getMessage());
		}
	}
	





	//---------NEW 11----------//
	//*************************//
	
	

	// Test No: 16
	// Obj: Provide valid customer firstname, lastname and valid month date
	// Inputs: firstname=merve lastname=sayar date=3
	// Expected Output:true
	public void testCheckPublicationGenre001() {
		try {
			// Create DB object
			MerveDB db = new MerveDB();

			assertEquals(db.CheckPublicationGenre("merve", "sayar", "3"), true);

		} catch (MerveNewsagentExceptionHandler e1) {
			fail("Should not get here ...");
		}
	}

	// Test No: 17
	// Obj: Provide invalid customer firstname,valid customer lastname valid
	// month date 
	// Inputs: customer firstname=m ,customer lastname=sayar date1="3"
	// Expected Output: Exception thrown with message "Please provide valid
	// customername"
	public void testCheckPublicationGenre002() {
		try {
			// Create DB object
			MerveDB db = new MerveDB();

			db.CheckPublicationGenre("m", "sayar", "3");
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid customername", e.getMessage());
		}
	}

	// Test No: 18
	// Obj: Provide valid customer firstname,invalid customer lastname , valid
	// month date 
	// Inputs: customer firstname=merve ,customer lastname=s date="3"
	// Expected Output: Exception thrown with message "Please provide valid
	// customername"
	public void testCheckPublicationGenre003() {
		try {
			// Create DB object
			MerveDB db = new MerveDB();

			db.CheckPublicationGenre("merve", "s", "3");
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid customername", e.getMessage());
		}
	}

	// Test No: 19
	// Obj: Provide invalid customer firstname,invalid customer lastname,valid
	// month date 
	// Inputs: customer firstname=m ,customer lastname=s date1="3"
	// Expected Output: Exception thrown with message "Please provide valid
	// customername"
	public void testCheckPublicationGenre004() {
		try {
			// Create DB object
			MerveDB db = new MerveDB();

			db.CheckPublicationGenre("m", "s", "3");
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid customername", e.getMessage());
		}
	}

	// Test No: 20
	// Obj: Provide valid customer firstname,valid customer lastname,invalid
	// month date 
	// Inputs: customer firstname=merve ,customer lastname=sayar
	// date="11" 
	// Expected Output: Exception thrown with message "Please provide valid
	// date "
	public void testCheckPublicationGenre005() {
		try {
			// Create DB object
			MerveDB db = new MerveDB();

			db.CheckPublicationGenre("merve", "sayar","11");
			fail("Exception expected .."
					+ "...");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid date ", e.getMessage());
		}
	}

	// Test No: 21
	// Obj: Provide valid deliverydate monthly
	// Inputs: deliverydate1="3"
	// Expected Output: true
	public void testCheckDeliveryDateRange001() {
		try {
			// Create DB object
			MerveDB db = new MerveDB();

			assertEquals(db.CheckDeliveryDateRange("3"), true);

		} catch (MerveNewsagentExceptionHandler e1) {
			fail("Should not get here ...");
		}
	}

	// Test No: 22
	// Obj: Provide invalid deliverydate monthly
	// Inputs: deliverydate month =11
	// Expected Output: Exception thrown with message "Please provide valid
	// date"
	public void testCheckDeliveryDateRange002() {
		try {
			// Create DB object
			MerveDB db = new MerveDB();

			db.CheckDeliveryDateRange("11");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid date ", e.getMessage());
		}
	}

}
