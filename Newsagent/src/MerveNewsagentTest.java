import junit.framework.TestCase;

public class MerveNewsagentTest extends TestCase {

	// Test No: 1
	// Obj: Provide valid customer firstname and customer
	// lastname,deliveryfee,deliverydate and total amount
	// Inputs: customer firstname=merve ,customer lastname=sayar,delivery
	// fee=0.5,delivery date=2016-03-01 ,total amount=2
	// Expected Output: true

	public void testCreateBill001() {
		// Create Newsagent object
		MerveNewsagent newsagent = new MerveNewsagent();

		// Test the method
		try {

			assertEquals(newsagent.createbill("merve", "sayar", 0.5, "2016-03-01", 2), true);

		} catch (MerveNewsagentExceptionHandler e1) {
			fail("Should not get here ...");
		}
	}

	// Test No: 2
	// Obj: Provide invalid customer firstname ,valid customer
	// lastname,deliveryfee,deliverydate and total amount
	// Inputs: customer firstname=m ,customer lastname=sayar,delivery
	// fee=0.5,delivery date=2016-03-01 ,total amount=2
	// Expected Output: Exception thrown with message "Please provide valid
	// customername"

	public void testCreateBill002() {
		// Create Newsagent object
		MerveNewsagent newsagent = new MerveNewsagent();

		// Test the method
		try {

			newsagent.createbill("m", "sayar", 0.5, "2016-03-01", 2);
			fail("Exception expected .....");
		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid customername", e.getMessage());
		}
	}

	// Test No: 3
	// Obj: Provide valid customer firstname,invalid customer lastname,valid
	// deliveryfee,deliverydate and total am
	// Inputs: customer firstname=merve ,customer lastname=s,delivery
	// fee=0.5,delivery date=2016-03-01 ,total amount=2
	// Expected Output: Exception thrown with message "Please provide valid
	// customername"
	public void testCreateBill003() {
		// Create Newsagent object
		MerveNewsagent newsagent = new MerveNewsagent();

		// Test the method
		try {

			newsagent.createbill("merve", "s", 0.5, "2016-03-01", 2);
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid customername", e.getMessage());
		}
	}

	// Test No: 4
	// Obj: Provide valid customer firstname,valid customer lastname,valid
	// deliveryfee,invalid deliverydate and total amount
	// Inputs: customer firstname=merve ,customer lastname=sayar,delivery
	// fee=0.5,delivery date=1994-03-01 ,total amount=2
	// Expected Output: Exception thrown with message "Please provide valid
	// deliverydate"
	public void testCreateBill004() {
		// Create Newsagent object
		MerveNewsagent newsagent = new MerveNewsagent();

		// Test the method
		try {

			newsagent.createbill("merve", "sayar", 0.5, "1994-03-01", 2);
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid deliverydate", e.getMessage());
		}
	}

	// Test No: 5
	// Obj: Provide invalid customer firstname,invalid customer lastname,valid
	// deliveryfee,deliverydate and total amount
	// Inputs: customer firstname=merve ,customer lastname=sayar,delivery
	// fee=0.5, delivery date=2016-03-01 ,total amount=2
	// Expected Output: Exception thrown with message "Please provide valid
	// customername"
	public void testCreateBill005() {
		// Create Newsagent object
		MerveNewsagent newsagent = new MerveNewsagent();

		// Test the method
		try {

			newsagent.createbill("m", "s", 0.5, "2016-03-01", 2);
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid customername", e.getMessage());
		}
	}
	
	// Test No: 6
		// Obj: Provide valid customer firstname,valid customer lastname,valid
		// deliveryfee,deliverydate and invalid total amount
		// Inputs: customer firstname=merve ,customer lastname=sayar,delivery
		// fee=0.5, delivery date=2016-03-01 ,total amount=null
		// Expected Output: Exception thrown with message "Fee can not be null"
		
		
		
			public void testCreateBill006() {
				// Create Newsagent object
				MerveNewsagent newsagent = new MerveNewsagent();

				// Test the method
				try {

					newsagent.createbill("merve", "sayar", 0.5, "2016-03-01",null );
					fail("Exception expected .....");

				} catch (MerveNewsagentExceptionHandler e) {
					assertSame("Fee can not be null", e.getMessage());
				}
			}
		

		
		
			// Test No: 7
			// Obj: Provide valid customer firstname,valid customer lastname,invalid
			// deliveryfee,valid deliverydate and valid total amount
			// Inputs: customer firstname=merve ,customer lastname=sayar,delivery
			// fee=null, delivery date=2016-03-01 ,total amount=1
			// Expected Output: Exception thrown with message "Fee can not be null"
			
			
			
				public void testCreateBill007() {
					// Create Newsagent object
					MerveNewsagent newsagent = new MerveNewsagent();

					// Test the method
					try {

						newsagent.createbill("merve", "sayar", null, "2016-03-01",1 );
						fail("Exception expected .....");

					} catch (MerveNewsagentExceptionHandler e) {
						assertSame("Fee can not be null", e.getMessage());
					}
				}
				// Test No: 8
				// Obj: Provide valid customer firstname,valid customer lastname,invalid
				// deliveryfee,valid deliverydate and invalid total amount
				// Inputs: customer firstname=merve ,customer lastname=sayar,delivery
				// fee=null, delivery date=2016-03-01 ,total amount=null
				// Expected Output: Exception thrown with message "Fee can not be null"
				
				
				
					public void testCreateBill008() {
						// Create Newsagent object
						MerveNewsagent newsagent = new MerveNewsagent();
						// Test the method
						try {

							newsagent.createbill("merve", "sayar", null, "2016-03-01",null );
							fail("Exception expected .....");

						} catch (MerveNewsagentExceptionHandler e) {
							assertSame("Fee can not be null", e.getMessage());
						}
					}


	// Test No: 9
	// Obj: Provide valid customer firstname,valid customer lastname
	// Inputs: customer firstname=merve ,customer lastname=sayar
	// Expected Output: true
	public void testCheckCustomer001() {
		try {
			// Create Newsagent object
			MerveNewsagent newsagent = new MerveNewsagent();

			assertEquals(newsagent.CheckCustomer("merve", "sayar"), true);

		} catch (MerveNewsagentExceptionHandler e1) {
			fail("Should not get here ...");
		}
	}

	// Test No: 10
	// Obj: Provide valid customer firstname,invalid customer lastname
	// Inputs: customer firstname=merve ,customer lastname=s
	// Expected Output: Exception thrown with message "Please provide valid
	// customername"
	public void testCheckCustomer002() {
		try {
			// Create Newsagent object
			MerveNewsagent newsagent = new MerveNewsagent();

			newsagent.CheckCustomer("merve", "s");
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid customername", e.getMessage());
		}
	}

	// Test No: 11
	// Obj: Provide invalid customer firstname,valid customer lastname
	// Inputs: customer firstname=m ,customer lastname=sayar
	// Expected Output: Exception thrown with message "Please provide valid
	// customername"
	public void testCheckCustomer003() {
		try {
			// Create Newsagent object
			MerveNewsagent newsagent = new MerveNewsagent();

			newsagent.CheckCustomer("m", "sayar");
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid customername", e.getMessage());
		}
	}

	// Test No: 12
	// Obj: Provide invalid customer firstname,invalid customer lastname
	// Inputs: customer firstname=m ,customer lastname=s
	// Expected Output: Exception thrown with message "Please provide valid
	// customername"
	public void testCheckCustomer004() {
		try {
			// Create Newsagent object
			MerveNewsagent newsagent = new MerveNewsagent();

			newsagent.CheckCustomer("m", "s");
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid customername", e.getMessage());
		}
	}

	// Test No: 13
	// Obj: Provide valid deliverydate
	// Inputs: deliverydate=2016-03-01
	// Expected Output: true
	public void testCheckDeliveryDate001() {
		try {
			// Create Newsagent object
			MerveNewsagent newsagent = new MerveNewsagent();

			assertEquals(newsagent.CheckDeliveryDate("2016-03-01"), true);

		} catch (MerveNewsagentExceptionHandler e1) {
			fail("Should not get here ...");
		}
	}

	// Test No: 14
	// Obj: Provide invalid deliverydate
	// Inputs: deliverydate=1994-03-01
	// Expected Output: Exception thrown with message "Please provide valid
	// deliverydate"
	public void testCheckDeliveryDate002() {
		try {
			// Create Newsagent object
			MerveNewsagent newsagent = new MerveNewsagent();

			newsagent.CheckDeliveryDate("1994-03-01");
			fail("Exception expected .....");

		} catch (MerveNewsagentExceptionHandler e) {
			assertSame("Please provide valid deliverydate", e.getMessage());
		}
	}
	//---------NEW 11----------//
	   //*************************//

		// Test No: 15
		// Obj: Provide valid customer firstname, lastname and valid month date 
		// Inputs: firstname=merve lastname=sayar date1=3
		// Expected Output:true
		public void testCheckPublicationGenre001() {
			try {
				// Create Newsagent object
				MerveNewsagent newsagent = new MerveNewsagent();

				assertEquals(newsagent.CheckPublicationGenre("merve", "sayar", "3"), true);

			} catch (MerveNewsagentExceptionHandler e1) {
				fail("Should not get here ...");
			}
		}

		// Test No: 16
		// Obj: Provide invalid customer firstname,valid customer lastname valid
		// month date 
		// Inputs: customer firstname=m ,customer lastname=sayar date1="3"
		// Expected Output: Exception thrown with message "Please provide valid
		// customername"
		public void testCheckPublicationGenre002() {
			try {
				// Create Newsagent object
				MerveNewsagent newsagent = new MerveNewsagent();

				newsagent.CheckPublicationGenre("m", "sayar", "3");
				fail("Exception expected .....");

			} catch (MerveNewsagentExceptionHandler e) {
				assertSame("Please provide valid customername", e.getMessage());
			}
		}

		// Test No: 17
		// Obj: Provide valid customer firstname,invalid customer lastname , valid
		// month date 
		// Inputs: customer firstname=merve ,customer lastname=s date1="3"
		// Expected Output: Exception thrown with message "Please provide valid
		// customername"
		public void testCheckPublicationGenre003() {
			try {
				// Create Newsagent object
				MerveNewsagent newsagent = new MerveNewsagent();

				newsagent.CheckPublicationGenre("merve", "s","3");
				fail("Exception expected .....");

			} catch (MerveNewsagentExceptionHandler e) {
				assertSame("Please provide valid customername", e.getMessage());
			}
		}

		// Test No: 18
		// Obj: Provide invalid customer firstname,invalid customer lastname,valid
		// month date 
		// Inputs: customer firstname=m ,customer lastname=s date1="3"
		// Expected Output: Exception thrown with message "Please provide valid
		// customername"
		public void testCheckPublicationGenre004() {
			try {
				// Create Newsagent object
				MerveNewsagent newsagent = new MerveNewsagent();

				newsagent.CheckPublicationGenre("m", "s", "3");
				fail("Exception expected .....");

			} catch (MerveNewsagentExceptionHandler e) {
				assertSame("Please provide valid customername", e.getMessage());
			}
		}

		// Test No: 19
		// Obj: Provide valid deliverydate month
		// Inputs: deliverydate1=3
		// Expected Output: true
		public void testCheckDeliveryDateRange001() {
			try {
				// Create Newsagent object
				MerveNewsagent newsagent = new MerveNewsagent();

				assertEquals(newsagent.CheckDeliveryDateRange("3"), true);

			} catch (MerveNewsagentExceptionHandler e) {
				fail("Should not get here ...");
			}
		}

		// Test No: 20
		// Obj: Provide invalid deliverydate month
		// Inputs: deliverydate1="3"
		// Expected Output: Exception thrown with message "Please provide valid
		// date"
		public void testCheckDeliveryDateRange002() {
			try {
				// Create Newsagent object
				MerveNewsagent newsagent = new MerveNewsagent();

				newsagent.CheckDeliveryDateRange("11");

			} catch (MerveNewsagentExceptionHandler e) {
				assertSame("Please provide valid date ", e.getMessage());
			}
		}


	
	
}
