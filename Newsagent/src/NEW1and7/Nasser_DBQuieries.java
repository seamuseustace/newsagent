package NEW1and7;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Nasser_DBQuieries {

	/*
	 * Database Connectivity Attributes
	 */
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	String cmd = null;
	String insertCustomer = "";
	String insertPublication = "";
	String insertDelivery = "";
	String insertSubscription = "";
	String allPublications = "";
	String allDeliveryPersons = "";
	String allSubscriptions = "";
	String printSummary = "";
	String customerHolidays = "";

	/*
	 * initiate database connection
	 */

	public String initiate_db_connection() throws Nasser_NewsagentExceptionHandler {
		String connected = "";
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/Newsagent";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "password");
			// Create a generic statement which is passed to the
			// TestInternalFrame1
			stmt = con.createStatement();
			connected = "Connected to database successfully.";
		} catch (Exception e) {

			throw new Nasser_NewsagentExceptionHandler(
					"Failed to connect to the database.");
		}
		return connected;
	}

	/*
	 * User story NEW 1 
	 * 
	 * Objective: Insert Customers Into Database
	 * 
	 * Requirements: publicationID, Customer firstName, Customer lastName, Customer
	 * houseAddress, Customer streetAddress, Customer region, Customer email not
	 * mandatory, Customer holidayDate, Customer phoneNumber
	 * 
	 * Implementation: 
	 * 1- Call initiate_db_connection() method to connect to database
	 * 2- Check for Mandatory fields as they can not be null or empty 
	 * 3- Insert into database 
	 * 4- Exception thrown once there is an invalid value
	 */
	public String insertCustomerIntoDB(Integer publicationID, String firstName,
			String lastName, String houseAddress, String streetAddress,
			String region, String email, String holidayDate, String phoneNumber)
			throws Nasser_NewsagentExceptionHandler {

		try {
			// Call initiate_db_connection() method to connect to database
			initiate_db_connection();

			// Check for these Mandatory fields as they can not be null or empty
			if (publicationID != null && firstName != null && firstName != null
					&& lastName != null && houseAddress != null
					&& streetAddress != null && region != null
					&& holidayDate != null && phoneNumber != null) {
				if (firstName != "" && firstName != "" && lastName != ""
						&& houseAddress != "" && streetAddress != ""
						&& region != "" && holidayDate != ""
						&& phoneNumber != "") {

					// Insert CUSTOMER into database
					String insert = "INSERT INTO CUSTOMER VALUES(null,"
							+ publicationID + ",'" + firstName + "','"
							+ lastName + "','" + houseAddress + "','"
							+ streetAddress + "','" + region + "','" + email
							+ "','" + holidayDate + "','" + phoneNumber
							+ " ');";

					stmt.executeUpdate(insert);
					insertCustomer = "Successfully saved.";

				} else
					throw new Nasser_NewsagentExceptionHandler(
							"Please complete all information with valid inputs");
			} else
				throw new Nasser_NewsagentExceptionHandler("Can not be null");

		} catch (SQLException e) {

			throw new Nasser_NewsagentExceptionHandler("Can not be null");
		}
		return insertCustomer;
	}

	/* 
	 * Objective :Insert Publication Into Database
	 * 
	 * Requirements: publicationID, publicationName, publicationType, publicationGenre, price,
	 * publicationDate
	 * 
	 * Implementation: 
	 * 1- Call initiate_db_connection() method to connect to database
	 * 2- Check for Mandatory fields as they can not be null or empty 
	 * 3- Insert into database 
	 * 4- Exception thrown once there is an invalid value
	 */

	public String insertPublicationIntoDB(String publicationName,
			String publicationType, String publicationGenre, Double price,
			String publicationDate) throws Nasser_NewsagentExceptionHandler {

		try {
			// Call initiate_db_connection() method to connect to database
			initiate_db_connection();

			// Check for these Mandatory fields as they can not be null or empty
			if (publicationName != null && publicationType != null
					&& publicationGenre != null && publicationDate != null) {
				if (publicationName != "" && publicationType != ""
						&& publicationGenre != "" && publicationDate != "") {
					
					// Insert PUBLICATION into database
					String insert = "INSERT INTO PUBLICATION VALUES( null,'"
							+ publicationName + "','" + publicationType + "','"
							+ publicationGenre + "'," + price + ",'"
							+ publicationDate + " ');";

					stmt.executeUpdate(insert);
					insertPublication = "Successfully saved.";

				} else
					throw new Nasser_NewsagentExceptionHandler(
							"Please complete all information with valid inputs");
			} else
				throw new Nasser_NewsagentExceptionHandler("Can not be null");
		} catch (SQLException sqle) {
			throw new Nasser_NewsagentExceptionHandler("Can not be null");
		}
		return insertPublication;
	}

	/*
	 * Objective: Insert Delivery Person Into Database 
	 *
	 * Requirements: deliveryPersonID, firstName, lastName, phoneNumber,
	 * region,
	 * 
	 * Implementation: 
	 * 1- Call initiate_db_connection() method to connect to database
	 * 2- Check for Mandatory fields as they can not be null or empty 
	 * 3- Insert into database 
	 * 4- Exception thrown once there is an invalid value
	 */

	public String insertDeliveryIntoDB(String firstName, String lastName,
			String phoneNumber, String region) throws Nasser_NewsagentExceptionHandler {

		try {
			// Call initiate_db_connection() method to connect to database
			initiate_db_connection();

			// Check for these Mandatory fields as they can not be null or empty
			if (firstName != null && lastName != null && phoneNumber != null
					&& region != null) {
				if (firstName != "" && lastName != "" && phoneNumber != ""
						&& region != "") {

					// Insert into DELIVERYPERSON table
					String insert = "INSERT INTO DELIVERYPERSON VALUES( null, +'"
							+ firstName
							+ "','"
							+ lastName
							+ "','"
							+ phoneNumber + "','" + region + " ');";

					stmt.executeUpdate(insert);
					insertDelivery = "Successfully saved.";

				} else
					throw new Nasser_NewsagentExceptionHandler(
							"Please complete all information with valid inputs");
			} else
				throw new Nasser_NewsagentExceptionHandler("Can not be null");
		} catch (SQLException sqle) {
			throw new Nasser_NewsagentExceptionHandler("Can not be null");
		}
		return insertDelivery;
	}

	/*
	 * Objective: Insert Subscription Into Database 
	 *
	 * Requirements: subscriptionID, publicationID, deliveryPersonID,
	 * customerID,amount, lastPaymentDate, received, deliveryDate
	 * 
	 * Implementation: 
	 * 1- Call initiate_db_connection() method to connect to database
	 * 2- Check for Mandatory fields as they can not be null or empty 
	 * 3- Insert into database 
	 * 4- Exception thrown once there is an invalid value
	 */

	public String insertSubscriptionIntoDB(Integer publicationID,
			Integer deliveryPersonID, Integer customerID, Integer amount,
			String lastPaymentDate, String received, String deliveryDate)
			throws Nasser_NewsagentExceptionHandler {

		try {
			// Call initiate_db_connection() method to connect to database
			initiate_db_connection();

			// Check for these Mandatory fields as they can not be null or empty
			if (publicationID != null && deliveryPersonID != null
					&& customerID != null && amount != null
					&& lastPaymentDate != null && received != null
					&& deliveryDate != null) {
				if (lastPaymentDate != "" && deliveryDate != ""
						&& received != "") {
					
					String insert = "";
					// Check if is received is true
					if (received.equals("true")) {

						// Insert into SUBSCRIPTION table with true value for received
						insert = "INSERT INTO SUBSCRIPTION VALUES( null,"
								+ publicationID + "," + deliveryPersonID + ","
								+ customerID + "," + amount + ",'"
								+ lastPaymentDate + "'," + true + ",'"
								+ deliveryDate + " ');";
					}

					// Else check if received is false
					else if (received.equals("false")) {
						// Insert into SUBSCRIPTION table with false value for received
						insert = "INSERT INTO SUBSCRIPTION VALUES( null,"
								+ publicationID + "," + deliveryPersonID + ","
								+ customerID + "," + amount + ",'"
								+ lastPaymentDate + "'," + false + ",'"
								+ deliveryDate + " ');";

					}
					stmt.executeUpdate(insert);
					insertSubscription = "Successfully saved.";

				} else
					throw new Nasser_NewsagentExceptionHandler(
							"Please complete all information with valid inputs");
			} else
				throw new Nasser_NewsagentExceptionHandler("Can not be null");

		} catch (SQLException sqle) {
			throw new Nasser_NewsagentExceptionHandler("Can not be null");
		}
		return insertSubscription;
	}

	/*
	 * Objective: Viewing Customer Holiday 
	 *  
	 * Requirements: Customer firstName, Customer lastName
	 * 
	 * Implementation: 
	 * 1- Call initiate_db_connection() method to connect to database
	 * 2- Check for Mandatory fields as they can not be null or empty 
	 * 3- Read from database 
	 * 4- Exception thrown once Failed to connect to the database.
	 */
	public boolean viewCustomerHoliday(String firstName, String lastName)
			throws Nasser_NewsagentExceptionHandler {

		try {
			// Call initiate_db_connection() method to connect to database
			initiate_db_connection();

			// Check for these Mandatory fields as they can not be null or empty
			if (firstName != null && lastName != null) {
				if (firstName != "" && lastName != "") {

					// Read customer holiday date form CUSTOMER table
					cmd = "SELECT holidayDate FROM CUSTOMER WHERE firstName  = '"
							+ firstName
							+ " '  and lastName = '"
							+ lastName
							+ "'; ";

					rs = stmt.executeQuery(cmd);

					// Check if there is a holiday value for the given first and
					// last name
					if (rs.next()) {
						String customerholidayDate = rs
								.getString("holidayDate");
						customerHolidays = "Holidate Days: "
								+ customerholidayDate;
						
						System.out.println(customerHolidays);

					} else
						throw new Nasser_NewsagentExceptionHandler(
								"Please enter valid first and last names");
				} else
					throw new Nasser_NewsagentExceptionHandler(
							"Please enter valid first and last names");
			} else
				throw new Nasser_NewsagentExceptionHandler("Can not be null");

		} catch (SQLException sqle) {

			throw new Nasser_NewsagentExceptionHandler(
					"Failed to connect to the database.");
		}
		return true;

	}

	/*
	 * Objective: Viewing Publication 
	 * 
	 * Requirement: Execute Query 
	 * 
	 * Implementation: 
	 * 1- Call initiate_db_connection() method to connect to database
	 * 2- Execute Query SELECT * FROM PUBLICATION ; 
	 * 3- Check while there is one or more publications
	 * 4- Read from database 
	 * 5- Exception thrown once Failed to connect to the database.
	 */
	public boolean viewPublication() throws Nasser_NewsagentExceptionHandler {

		try {
			// Call initiate_db_connection() method to connect to database
			initiate_db_connection();
			// Read Publication from PUBLICATION table
			cmd = "SELECT * FROM PUBLICATION ; ";
			rs = stmt.executeQuery(cmd);

			// Check while there is one or more publications
			while (rs.next()) {

				// Read publicationID, publicationName, publicationType,
				// publicationGenre, price and publicationDate values
				int id = rs.getInt("publicationID");
				String puplicationName = rs.getString("publicationName");
				String publicationType = rs.getString("publicationType");
				String publicationGenre = rs.getString("publicationGenre");
				double price = rs.getDouble("price");
				String publicationDate = rs.getString("publicationDate");

				allPublications = "ID:	" + id + "\nName:	" + puplicationName
						+ "\nType:	" + publicationType + "\nGener:	"
						+ publicationGenre + "\nPrice:	" + price + "\nDate:	"
						+ publicationDate + "\n---------------------\n";

				System.out.println(allPublications);
			}

		} catch (SQLException sqle) {

			throw new Nasser_NewsagentExceptionHandler(
					"Failed to connect to the database.");
		}
		return true;
	}

	/*
	 * Objective: Viewing Deliver Persons 
	 * 
	 * Requirement: Execute Query 
	 * 
	 * Implementation: 
	 * 1- Call initiate_db_connection() method to connect to database
	 * 2- Execute Query SELECT * FROM DELIVERYPERSON ; 
	 * 3- Check while there is one or more Deliver Persons 
	 * 4- Read from database 
	 * 5- Exception thrown once Failed to connect to the database.
	 */
	public boolean viewDeliverPersons() throws Nasser_NewsagentExceptionHandler {

		try {
			// Call initiate_db_connection() method to connect to database
			initiate_db_connection();
			
			// Read Deliver Persons from DELIVERYPERSON table
			cmd = "SELECT * FROM DELIVERYPERSON ; ";
			rs = stmt.executeQuery(cmd);

			// Check while there is one or more Deliver Persons
			while (rs.next()) {

				// Read deliveryPersonID, firstName, lastName, phoneNumber and
				// region values
				int id = rs.getInt("deliveryPersonID");
				String deliverFirstName = rs.getString("firstName");
				String deliverLastName = rs.getString("lastName");
				String deliveryPhoneNumber = rs.getString("phoneNumber");
				String deliveryRrgion = rs.getString("region");

				allDeliveryPersons = "ID: " + id + "\nFirstName: "
						+ deliverFirstName + "\nLastName: " + deliverLastName
						+ "\nPhone No.: " + deliveryPhoneNumber + "\nRegion: "
						+ deliveryRrgion + "\n---------------------\n";

				System.out.println(allDeliveryPersons);
			}

		} catch (SQLException sqle) {
			throw new Nasser_NewsagentExceptionHandler(
					"Failed to connect to the database.");
		}
		return true;
	}

	/*
	 /*
	 * Objective: Viewing Subscription 
	 * 
	 * Requirement: Execute Query 
	 * 
	 * Implementation: 
	 * 1- Call initiate_db_connection() method to connect to database
	 * 2- Execute Query SELECT * FROM SUBSCRIPTION ; 
	 * 3- Check while there is one or more Subscription
	 * 4- Read from database 
	 * 5- Exception thrown once Failed to connect to the database.
	 */
	public boolean viewSubscription() throws Nasser_NewsagentExceptionHandler {

		try {
			// Call initiate_db_connection() method to connect to database
			initiate_db_connection();
			cmd = "SELECT * FROM SUBSCRIPTION ; ";
			rs = stmt.executeQuery(cmd);

			// Check while there is one or more Subscriptions
			while (rs.next()) {

				// Read subscriptionID, publicationID, deliveryPersonID,
				// customerID, amount, lastPaymentDate, received and
				// deliveryDate values
				int subscriptionID = rs.getInt("subscriptionID");
				int publicationID = rs.getInt("publicationID");
				int deliveryPersonID = rs.getInt("deliveryPersonID");
				int customerID = rs.getInt("customerID");
				String amount = rs.getString("amount");
				String lastPaymentDate = rs.getString("lastPaymentDate");
				boolean received = rs.getBoolean("received");
				String deliveryDate = rs.getString("deliveryDate");

				allSubscriptions = "subscription ID: " + subscriptionID
						+ "\nPublication ID: " + publicationID
						+ "\nDelivery Person ID: " + deliveryPersonID
						+ "\nCustomer ID: " + customerID + "\nAmount: "
						+ amount + "\nLast Payment Date: " + lastPaymentDate
						+ "\nReceived: " + received + "\nDelivery Date.: "
						+ deliveryDate + "\n---------------------\n";

				System.out.println(allSubscriptions);
			}

		} catch (SQLException sqle) {
			throw new Nasser_NewsagentExceptionHandler(
					"Failed to connect to the database.");
		}
		return true;
	}

	/* User story NEW 7
	 * Objective: Printing Customer Summary 
	 * 
	 * Requirements: CustomerID and PublicationID 
	 * 
	 * Implementation: 
	 * 1- Call initiate_db_connection() method to connect to database
	 * 2- Execute Query with the assigned CustomerID and PublicationID; 
	 * 3- Check while there is one or more summary details
	 * 4- Read from database 
	 * 5- Exception thrown once Failed to connect to the database.
	 */
	public boolean printSummary(Integer myCustomerID, Integer myPublicationID)
			throws Nasser_NewsagentExceptionHandler {

		try {
			// Call initiate_db_connection() method to connect to database
			initiate_db_connection();
			
			// Execute query with assigned myCustomerID and myPublicationID to
			// print customer summary
			cmd = " SELECT customer.firstName,customer.lastName,publication.publicationName,publication.publicationType,publication.publicationGenre,subscription.deliveryDate from customer,subscription,publication where subscription.customerID= "
					+ myCustomerID
					+ " and subscription.publicationID= "
					+ myPublicationID + " ; ";

			rs = stmt.executeQuery(cmd);

			// Check while there is one or more details to print
			while (rs.next()) {
				// Read customerFirstName, customerLastName, name, type, genre
				// and deliveryDate values

				String customerFirstName = rs.getString("FirstName");
				String customerLastName = rs.getString("LastName");
				String name = rs.getString("publicationName");
				String type = rs.getString("publicationType");
				String genre = rs.getString("publicationGenre");
				String deliveryDate = rs.getString("deliveryDate");

				printSummary = "Custmoer First Name: " + customerFirstName
						+ "\nCustmoerLastName: " + customerLastName
						+ "\nPublicationName: " + name + "\nPublication Type: "
						+ type + "\nPublication Gener: " + genre
						+ "\nDelivery Date: " + deliveryDate
						+ "\n---------------------\n";

				System.out.println(printSummary);
			}

		} catch (SQLException sqle) {
			throw new Nasser_NewsagentExceptionHandler(
					"Failed to connect to the database.");
		}
		return true;
	}

}
