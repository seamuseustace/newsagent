package NEW1and7;

import junit.framework.TestCase;

public class Nasser_DBQuieriesTest extends TestCase {

	// Test No:1
	// Objective: test database Connection
	// Excepted output: Connected to database successfully

	public void testDBQuieriesDBcoonection() {
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();
		try {

			assertEquals(myDBQuieries.initiate_db_connection(),
					"Connected to database successfully.");

		} catch (Nasser_NewsagentExceptionHandler e) {

			fail("Should not reach here ...");
		}

	}

	// Test No: 2
	// Objective: Verify valid publicationID, valid firstName, valid lastName,
	// valid houseAddress, valid streetAddress, valid region, valid email, valid
	// holidayDate, valid phoneNumber
	// Inputs: 1,"Nasser","Alselami", "3 Dalys Apartments",
	// "Dublin Road","South","alselami@gmail.com", "2016-3-31", "0873998098"
	// Expected Output: Successfully saved.

	public void testInsertCustomerIntoDB001() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			assertEquals(myDBQuieries.insertCustomerIntoDB(1, "Nasser",
					"Alselami", "3 Dalys Apartments", "Dublin Road", "South",
					"alselami@gmail.com", "2016-3-31", "0873998098"),
					"Successfully saved.");

		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			fail("Should not reach here ...");
		}
	}

	// Test No: 3
	// Objective: Verify invalid publicationID, valid firstName, valid lastName,
	// valid houseAddress, valid streetAddress, valid region, valid email, valid
	// holidayDate, valid phoneNumber
	// Inputs: null, "Nasser" ,"Alselami", "3 Dalys Apartments", "Dublin Road",
	// "South","alselami@gmail.com", "2016-3-31", "0873998098"
	// Expected Output: Can not be null

	public void testInsertCustomerIntoDB002() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertCustomerIntoDB(null, "Nasser", "Alselami",
					"3 Dalys Apartments", "Dublin Road", "South",
					"alselami@gmail.com", "2016-3-31", "0873998098");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());

		}
	}

	// Test No: 4
	// Objective: Verify valid publicationID, invalid firstName, valid lastName,
	// valid houseAddress, valid streetAddress, valid region, valid email, valid
	// holidayDate, valid phoneNumber
	// Inputs: 1, "" , "Alselami", "3 Dalys Apartments",
	// "Dublin Road","South","alselami@gmail.com", "2016-3-31", "0873998098"
	// Expected Output: Please complete all information with valid inputs

	public void testInsertCustomerIntoDB003() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertCustomerIntoDB(1, "", "Alselami",
					"3 Dalys Apartments", "Dublin Road", "South",
					"alselami@gmail.com", "2016-3-31", "0873998098");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Please complete all information with valid inputs",
					myExcp.getMessage());

		}
	}

	// Test No: 5
	// Objective: Verify valid publicationID, valid firstName, invalid lastName,
	// valid houseAddress, valid streetAddress, valid region, valid email, valid
	// holidayDate, valid phoneNumber
	// Inputs: 1,"Nasser","", "3 Dalys Apartments",
	// "Dublin Road","South","alselami@gmail.com", "2016-3-31", "0873998098"
	// Expected Output: Please complete all information with valid inputs

	public void testInsertCustomerIntoDB004() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertCustomerIntoDB(1, "Nasser", "",
					"300 Dalys Apartments", "Dublin Road", "South",
					"alselami@gmail.com", "2016-3-31", "0873998098");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Please complete all information with valid inputs",
					myExcp.getMessage());

		}
	}

	// Test No: 6
	// Objective: Verify valid publicationID, valid firstName, valid lastName,
	// invalid houseAddress,valid streetAddress,valid region,valid email, valid
	// holidayDate,valid phoneNumber
	// Inputs: 1,"Nasser","Alselami", null,
	// "Dublin Road","South","alselami@gmail.com", "2016-3-31", "0873998098"
	// Expected Output: Can not be null

	public void testInsertCustomerIntoDB005() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertCustomerIntoDB(1, "Nasser", "Alselami", null,
					"Dublin Road", "South", "alselami@gmail.com", "2016-3-31",
					"0873998098");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());

		}
	}

	// Test No: 7
	// Objective: Verify valid publicationID, valid firstName, valid lastName,
	// valid houseAddress,invalid streetAddress,valid region,valid email, valid
	// holidayDate,valid phoneNumber,
	// Inputs: 1,"Nasser","Alselami", "3 Dalys Apartments",
	// "","South","alselami@gmail.com", "2016-3-31", "0873998098"
	// Expected Output: Please complete all information with valid inputs

	public void testInsertCustomerIntoDB006() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertCustomerIntoDB(1, "Nasser", "Alselami",
					"3 Dalys Apartments", "", "South", "alselami@gmail.com",
					"2016-3-31", "0873998098");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Please complete all information with valid inputs",
					myExcp.getMessage());

		}
	}

	// Test No: 8
	// Objective: Verify valid publicationID, valid firstName, valid lastName,
	// valid houseAddress,valid streetAddress, invalid region,valid email, valid
	// holidayDate, valid phoneNumber,
	// Inputs: 1,"Nasser","Alselami", "3 Dalys Apartments", "Dublin Road",
	// "","alselami@gmail.com", "2016-3-31", "0873998098"
	// Expected Output: Please complete all information with valid inputs

	public void testInsertCustomerIntoDB007() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertCustomerIntoDB(1, "Nasser", "Alselami",
					"3 Dalys Apartments", "Dublin Road", "",
					"alselami@gmail.com", "2016-3-31", "0873998098");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Please complete all information with valid inputs",
					myExcp.getMessage());

		}
	}

	// Test No: 9
	// Objective: Verify valid publicationID, valid firstName, valid lastName,
	// valid houseAddress,valid streetAddress, valid region, invalid
	// email,validholidayDate,valid phoneNumber,
	// Inputs: 1,"Nasser","Alselami", "3 Dalys Apartments",
	// "Dublin Road","South",null, "2016-3-31", "0873998098"
	// Expected Output: Successfully saved.

	public void testInsertCustomerIntoDB008() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			assertEquals(myDBQuieries.insertCustomerIntoDB(1, "Nasser",
					"Alselami", "3 Dalys Apartments", "Dublin Road", "South",
					null, "2016-3-31", "0873998098"), "Successfully saved.");

		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			fail("Should not reach here ...");
		}
	}

	// Test No: 10
	// Objective: Verify valid publicationID, valid firstName, valid lastName,
	// valid houseAddress,valid streetAddress,valid region,valid email, invalid
	// holidayDate,valid phoneNumber,
	// Inputs: 1,"Nasser","Alselami", "3 Dalys Apartments",
	// "Dublin Road","South","alselami@gmail.com", "", "0873998098"
	// Expected Output: Please complete all information with valid inputs

	public void testInsertCustomerIntoDB009() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertCustomerIntoDB(1, "Nasser", "Alselami",
					"3 Dalys Apartments", "Dublin Road", "South",
					"alselami@gmail.com", "", "0873998098");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Please complete all information with valid inputs",
					myExcp.getMessage());

		}
	}

	// Test No: 11
	// Objective: Verify valid publicationID, valid firstName, valid lastName,
	// valid houseAddress,valid streetAddress,valid region,valid email,
	// validholidayDate,invalid phoneNumber,
	// Inputs: 1,"Nasser","Alselami", "3 Dalys Apartments",
	// "Dublin Road","South","alselami@gmail.com", "2016-3-31", null
	// Expected Output: Can not be null

	public void testInsertCustomerIntoDB010() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertCustomerIntoDB(1, "Nasser", "Alselami",
					"3 Dalys Apartments", "Dublin Road", "South",
					"alselami@gmail.com", "2016-3-31", null);
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());

		}
	}

	// Test No: 12
	// Objective: Verify valid publicationName, valid publicationType, valid
	// publicationGener,
	// valid price,valid publicationDate
	// Inputs:"Agile","Magazine", "Education", 10.99, "2016-03-30"
	// Expected Output: Successfully saved.

	public void testInsertPublicationIntoDB001() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			assertEquals(myDBQuieries.insertPublicationIntoDB("Agile",
					"Magazine", "Education", 10.99, "2016-03-30"),
					"Successfully saved.");

		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			fail("Should not reach here ...");
		}
	}

	// Test No: 13
	// Objective: Verify invalid publicationName, valid publicationType,
	// validpublicationGener,valid price,valid publicationDate
	// Inputs: "","Magazine", "Education", 10.99, "2016-03-30"
	// Expected Output: Please complete all information with valid inputs

	public void testInsertPublicationIntoDB002() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertPublicationIntoDB("", "Magazine", "Education",
					10.99, "2016-03-30");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Please complete all information with valid inputs",
					myExcp.getMessage());

		}
	}

	// Test No: 14
	// Objective: Verify valid publicationName, invalid publicationType, valid
	// publicationGener,valid price,valid publicationDate
	// Inputs: "Agile",null, "Education", 10.99, "2016-03-30"
	// Expected Output: Can not be null

	public void testInsertPublicationIntoDB003() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertPublicationIntoDB("Agile", null, "Education",
					10.99, "2016-03-30");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());

		}
	}

	// Test No: 15
	// Objective: Verify valid publicationName, valid publicationType, invalid
	// publicationGener,valid price,valid publicationDate
	// Inputs: "Agile","Magazine", "", 10.99, "2016-03-30"
	// Expected Output: Please complete all information with valid inputs

	public void testInsertPublicationIntoDB004() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertPublicationIntoDB("Agile", "Magazine", "",
					10.99, "2016-03-30");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Please complete all information with valid inputs",
					myExcp.getMessage());

		}
	}

	// Test No: 16
	// Objective: Verify valid publicationName, valid publicationType,
	// validpublicationGener,invalid price,valid publicationDate
	// Inputs: "Agile","Magazine", "Education", null, "2016-03-30"
	// Expected Output: Can not be null

	public void testInsertPublicationIntoDB005() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertPublicationIntoDB("Agile", "Magazine",
					"Education", null, "2016-03-30");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());

		}
	}

	// Test No: 17
	// Objective: Verify valid publicationName, valid publicationType,
	// validpublicationGener,valid price,invalid publicationDate
	// Inputs: "Agile","Magazine", "Education", 10.99, ""
	// Expected Output: Please complete all information with valid inputs

	public void testInsertPublicationIntoDB006() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertPublicationIntoDB("Agile", "Magazine",
					"Education", 10.99, "");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Please complete all information with valid inputs",
					myExcp.getMessage());

		}
	}

	// Test No: 18
	// Objective: Verify valid firstName, valid lastName, valid phone number,
	// valid region
	// Inputs:"John","Smith", "0863888899", "South"
	// Expected Output: Successfully saved.

	public void testInsertDeliveryPersonIntoDB001() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			assertEquals(myDBQuieries.insertDeliveryIntoDB("John", "Smith",
					"0863888899", "South"), "Successfully saved.");

		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			fail("Should not reach here ...");
		}
	}

	// Test No: 19
	// Objective: Verify invalid firstName, valid lastName, valid phone number,
	// valid region
	// Inputs:null,"Smith", "0863888899", "South"
	// Expected Output: Can not be null.

	public void testInsertDeliveryPersonIntoDB002() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertDeliveryIntoDB(null, "Smith", "0863888899",
					"South");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());
		}
	}

	// Test No: 20
	// Objective: Verify valid firstName, invalid lastName, valid phone number,
	// valid region
	// Inputs:"John","", "0863888899", "South"
	// Expected Output: Please complete all information with valid inputs

	public void testInsertDeliveryPersonIntoDB003() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries
					.insertDeliveryIntoDB("John", "", "0863888899", "South");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Please complete all information with valid inputs",
					myExcp.getMessage());
		}
	}

	// Test No: 21
	// Objective: Verify valid firstName, valid lastName, invalid phone number,
	// valid region
	// Inputs:"John","Smith", null, "South"
	// Expected Output: Can not be null

	public void testInsertDeliveryPersonIntoDB04() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertDeliveryIntoDB("John", "Smith", null, "South");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());
		}
	}

	// Test No: 22
	// Objective: Verify valid firstName, valid lastName, valid phone number,
	// valid region
	// Inputs:"John","Smith", "0863888899", ""
	// Expected Output: Please complete all information with valid inputs

	public void testInsertDeliveryPersonIntoDB005() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries
					.insertDeliveryIntoDB("John", "Smith", "0863888899", "");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Please complete all information with valid inputs",
					myExcp.getMessage());
		}
	}

	// Test No: 23
	// Objective: Verify valid publicationID, valid deliveryPersonID, valid
	// customerID, valid amount,valid lastPaymentDate , valid received, valid
	// deliveryDate
	// Inputs:1, 1, 1, 1, "2016-3-31" , "true" , "2016-3-31"
	// Expected Output: Successfully saved.

	public void testInsertSubscriptionIntoDB001() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			assertEquals(myDBQuieries.insertSubscriptionIntoDB(1, 1, 1, 1,
					"2016-3-31", "true", "2016-3-31"), "Successfully saved.");

		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			fail("Should not reach here ...");
		}
	}

	// Test No: 24
	// Objective: Verify valid publicationID, valid deliveryPersonID, valid
	// customerID, valid amount,valid lastPaymentDate , valid received, valid
	// deliveryDate
	// Inputs:1, 1, 1, 1, "2016-3-31" , "false" , "2016-3-31"
	// Expected Output: Successfully saved.

	public void testInsertSubscriptionIntoDB002() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			assertEquals(myDBQuieries.insertSubscriptionIntoDB(1, 1, 1, 1,
					"2016-3-31", "false", "2016-3-31"), "Successfully saved.");

		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			fail("Should not reach here ...");
		}
	}

	// Test No: 25
	// Objective: Verify invalid publicationID, valid deliveryPersonID, valid
	// customerID, valid amount,valid lastPaymentDate , valid received, valid
	// deliveryDate
	// Inputs:null, 1, 1, 1, "2016-3-31" , "true" , "2016-3-31"
	// Expected Output: Can not be null

	public void testInsertSubscriptionIntoDB003() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertSubscriptionIntoDB(null, 1, 1, 1, "2016-3-31",
					"true", "2016-3-31");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());
		}
	}

	// Test No: 26
	// Objective: Verify valid publicationID, invalid deliveryPersonID, valid
	// customerID, valid amount,valid lastPaymentDate , valid received, valid
	// deliveryDate
	// Inputs: 1, null, 1, 1, "2016-3-31" , "true" , "2016-3-31"
	// Expected Output: Can not be null

	public void testInsertSubscriptionIntoDB004() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertSubscriptionIntoDB(1, null, 1, 1, "2016-3-31",
					"true", "2016-3-31");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());
		}
	}

	// Test No: 27
	// Objective: Verify valid publicationID, valid deliveryPersonID, invalid
	// customerID, valid amount,valid lastPaymentDate , valid received, valid
	// deliveryDate
	// Inputs: 1, 1, null, 1, "2016-3-31" , "true" , "2016-3-31"
	// Expected Output: Can not be null

	public void testInsertSubscriptionIntoDB005() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertSubscriptionIntoDB(1, 1, null, 1, "2016-3-31",
					"true", "2016-3-31");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());
		}
	}

	// Test No: 28
	// Objective: Verify valid publicationID, valid deliveryPersonID, valid
	// customerID, invalid amount,valid lastPaymentDate , valid received, valid
	// deliveryDate
	// Inputs: 1, 1, 1, null, "2016-3-31" , "true" , "2016-3-31"
	// Expected Output: Can not be null

	public void testInsertSubscriptionIntoDB006() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertSubscriptionIntoDB(1, 1, 1, null, "2016-3-31",
					"true", "2016-3-31");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());
		}
	}

	// Test No: 29
	// Objective: Verify valid publicationID, valid deliveryPersonID, valid
	// customerID, valid amount, invalid lastPaymentDate , valid received, valid
	// deliveryDate
	// Inputs: 1, 1, 1, 1, "" , "true" , "2016-3-31"
	// Expected Output: Please complete all information with valid inputs

	public void testInsertSubscriptionIntoDB007() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertSubscriptionIntoDB(1, 1, 1, 1, "", "true",
					"2016-3-31");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Please complete all information with valid inputs",
					myExcp.getMessage());
		}
	}

	// Test No: 30
	// Objective: Verify valid publicationID, valid deliveryPersonID, valid
	// customerID, valid amount, valid lastPaymentDate , invalid received, valid
	// deliveryDate
	// Inputs: 1, 1, 1, 1, "2016-3-31" , null , "2016-3-31"
	// Expected Output: Can not be null

	public void testInsertSubscriptionIntoDB008() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertSubscriptionIntoDB(1, 1, 1, 1, "2016-3-31",
					null, "2016-3-31");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());
		}
	}

	// Test No: 31
	// Objective: Verify valid publicationID, valid deliveryPersonID, valid
	// customerID, valid amount, valid lastPaymentDate , valid received, invalid
	// deliveryDate
	// Inputs: 1, 1, 1, 1, "2016-3-31" , "true" , null
	// Expected Output: Can not be null

	public void testInsertSubscriptionIntoDB009() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.insertSubscriptionIntoDB(1, 1, 1, 1, "2016-3-31",
					"true", null);
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());
		}
	}

	// Test No: 32
	// Objective: Verify valid firstName, valid lastName,
	// Inputs: "Nasser", "Alselami"
	// Expected Output: Holidate Days: 2016-3-31

	public void testViewCustomerHoliday001() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			assertEquals(
					myDBQuieries.viewCustomerHoliday("Nasser", "Alselami"),
					true);

		} catch (Nasser_NewsagentExceptionHandler myExcp) {

			// assertSame("Failed to connect to the database.",
			// myExcp.getMessage());
			fail("Should not reach here ...");
		}
	}

	// Test No: 33
	// Objective: Verify invalid firstName, valid lastName,
	// Inputs: "Majed", "Alselami"
	// Expected Output: Please enter valid first and last names

	public void testViewCustomerHoliday002() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.viewCustomerHoliday("Majed", "Alselami");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Please enter valid first and last names",
					myExcp.getMessage());
		}
	}

	// Test No: 34
	// Objective: Verify valid firstName, invalid lastName,
	// Inputs: "Nasser", "Ali"
	// Expected Output: Please enter valid first and last names

	public void testViewCustomerHoliday003() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.viewCustomerHoliday("Nasser", "Ali");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Please enter valid first and last names",
					myExcp.getMessage());
		}
	}

	// Test No: 35
	// Objective: Verify invalid firstName, invalid lastName,
	// Inputs: "", ""
	// Expected Output: Please enter valid first and last names

	public void testViewCustomerHoliday004() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.viewCustomerHoliday("", "");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Please enter valid first and last names",
					myExcp.getMessage());
		}
	}

	// Test No: 36
	// Objective: Verify invalid firstName, invalid lastName,
	// Inputs: null, ""
	// Expected Output: Can not be null

	public void testViewCustomerHoliday005() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.viewCustomerHoliday(null, "");
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());
		}
	}

	// Test No: 37
	// Objective: Verify invalid firstName, invalid lastName,
	// Inputs: "", null
	// Expected Output: Can not be null

	public void testViewCustomerHoliday006() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.viewCustomerHoliday("", null);
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());
		}
	}

	// Test No: 38
	// Objective: Verify invalid firstName, invalid lastName,
	// Inputs: null, null
	// Expected Output: Can not be null

	public void testViewCustomerHoliday007() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			myDBQuieries.viewCustomerHoliday(null, null);
			fail("Should not reach here ...");
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Can not be null", myExcp.getMessage());
		}
	}

	// Test No: 39
	// Objective: Read Publication from database
	// Inputs: Execute SELECT * FROM PUBLICATION ; query from viewPublication()
	// Expected "Agile","Magazine", "Education", 10.99, "2016-03-30"

	public void testViewPublication001() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			assertEquals(myDBQuieries.viewPublication(), true);
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Failed to connect to the database.",
					myExcp.getMessage());

		}
	}

	// Test No: 40
	// Objective: Read DeliverPerson from database
	// Inputs: Execute SELECT * FROM DELIVERYPERSON ; query from
	// viewDeliverPersons()
	// Expected Output:
	// ID: 1
	// FirstName: John
	// LastName: Smith
	// Phone No.: 0863888899
	// Region: South

	public void testViewDeliverPersons001() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			assertEquals(myDBQuieries.viewDeliverPersons(), true);
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Failed to connect to the database.",
					myExcp.getMessage());

		}
	}

	// Test No: 41
	// Objective: Read Subscription from database
	// Inputs: Execute SELECT * FROM SUBSCRIPTION ; query from
	// viewSubscription()
	// Expected Output:
	// subscription ID: 1
	// Publication ID: 1
	// Delivery Person ID: 1
	// Customer ID: 1
	// Amount: 1
	// Last Payment Date: 2016-3-31
	// Received: true
	// Delivery Date.: 2016-3-31
	// ---------------------

	// subscription ID: 2
	// Publication ID: 1
	// Delivery Person ID: 1
	// Customer ID: 1
	// Amount: 1
	// Last Payment Date: 2016-3-31
	// Received: false
	// Delivery Date.: 2016-3-31
	// ---------------------

	public void testViewSubscription001() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			assertEquals(myDBQuieries.viewSubscription(), true);
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			assertSame("Failed to connect to the database.",
					myExcp.getMessage());

		}
	}

	// Test No: 42
	// Objective: Print Summary from database
	// Inputs: Execute SELECT
	// customer.firstName,customer.lastName,publication.publicationName,publication.publicationType,publication.publicationGenre,subscription.deliveryDate
	// from customer,subscription,publication
	// where subscription.customerID=customer.customerID and
	// subscription.publicationID=publication.publicationID; query from
	// viewSubscription()
	// Expected Output:
	// Customer First Name: Nasser
	// CustomerLastName: Alselami
	// PublicationName: Agile
	// Publication Type: Magazine
	// Publication Gener: Education
	// Delivery Date: 2016-3-31

	public void testPrintSummary001() {
		// Create DBQuieries object
		Nasser_DBQuieries myDBQuieries = new Nasser_DBQuieries();

		// Test the method
		try {

			assertEquals(myDBQuieries.printSummary(1, 1), true);
		} catch (Nasser_NewsagentExceptionHandler myExcp) {
			fail("Should not reach here ...");

		}
	}

}
