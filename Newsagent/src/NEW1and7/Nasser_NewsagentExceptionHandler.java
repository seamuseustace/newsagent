package NEW1and7;

public class Nasser_NewsagentExceptionHandler extends Exception {

	String myExceptionMessage;

	public Nasser_NewsagentExceptionHandler(String error) {
		myExceptionMessage = error;
	}

	public String getMessage() {
		return myExceptionMessage;
	}

}
