package Seamus;
//Seamus Eustace

/*
 * NEW2
 * As a Newsagent
 * I want to view each customers invoice in a month
 * So I can bill each customer appropriately.
 
 * Bills are prepared by adding up all publications received by a customer in a month.
 * Keeping all financial transactions of customer accounts.
 * Verify correct customer id, subscription id, total amount & fee received dates.
 
 
 * NEW16
 * As a Newsagent
 * I want to Print the delivery docket
 * So I can give it to the delivery person.
 
 * Receive customer details,beside each customer to confirm delivery.
 * The delivery will be in the order of closest delivery first and furthest delivery last.
 * The docket will be specific to each delivery persons designated region (North, South, East, West). 
 */

import junit.framework.TestCase;

public class NewsagentTest_SE extends TestCase {

	// ******************************************************************************
	// *																			*
	// * Test No: 1 - NEW2															*
	// * Object: View a customers invoice that have received publications in order	*
	//			 to create a bill.													*
	// * Inputs: Customer Number - 36222											*
	// * 		 Received - True													*
	// * Expected Output: True														*
	// * 																			*
	// ******************************************************************************
	public void test001() {
		Newsagent_SE DB = new Newsagent_SE();

		try {
			assertEquals(true, DB.viewValidCustInvoice("36222", "true"));
		} catch (Exception E) {
			E.getStackTrace();
		}
	}
	

	// ******************************************************************************
	// *																			*
	// * Test No: 2 - NEW2															*
	// * Object: View a customers invoice that have no publications delivered so	*
	//			 that no bill is created.											*
	// * Inputs: Customer Number - 36222											*
	// * 		 Received - False													*
	// * Expected Output: True														*
	// * 																			*
	// ******************************************************************************
	public void test002() {
		Newsagent_SE DB = new Newsagent_SE();

		try {
			assertEquals(false, DB.viewInvalidCustInvoice("36222", "false"));
		} catch (Exception E) {
			E.getStackTrace();
		}
	}
	
	
	// ******************************************************************************
	// *																			*
	// * Test No: 3 - NEW2 															*
	// * Object: View a customers invoice that the customer does not exist.			*
	// * Inputs: Customer Number - 12345											*
	// * 		 Received - True													*
	// * Expected Output: False														*
	// * 																			*
	// ******************************************************************************
	public void test003() {
		Newsagent_SE DB = new Newsagent_SE();

		try {
			assertEquals(false, DB.customerNotExist("12345", "true"));
		} catch (Exception E) {
			E.getStackTrace();
		}
	}


// ******************************************************************************
// *																			*
// * Test No: 4 - NEW16 														*
// * Object: Print customers bill with received publications.					*
// * Inputs: Customer Number - 12345											*
// * 		 Received - True													*
// * Expected Output: true														*
// * 																			*
// ******************************************************************************
	public void test004() {
		Newsagent_SE DB = new Newsagent_SE();

		try {
			assertEquals(true, DB.printCustomer("36222", "true"));
		} catch (Exception E) {
			E.getStackTrace();
		}
	}
}