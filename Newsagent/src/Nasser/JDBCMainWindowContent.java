package Nasser;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JTextArea;
import javax.swing.JScrollPane;

import java.awt.Font;

import javax.swing.JCheckBox;

@SuppressWarnings("serial")
public class JDBCMainWindowContent extends JInternalFrame {
	String cmd = null;

	// DB Connectivity Attributes
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	private Container content;

	String insertCustomer = "";
	String insertPublication = "";
	String insertDelivery = "";
	String insertSubscription = "";
	String allPublications = "";
	String allDeliveryPersons = "";
	String allSubscriptions = "";
	String printSummary = "";
	String customerHolidays = "";

	private JButton insertCustomerButton = new JButton("Insert Customer");
	private JButton clearButton = new JButton("Clear Text");
	private JButton printSummaryButton = new JButton("Print Customer Summary");
	private JButton viewCustmersHolidaysButton = new JButton(
			"View Custmer's Holidays");
	private JButton viewPublicationsButton = new JButton("View Publications");
	private JButton insertPublicationButton = new JButton("Insert Publication");
	private JButton insertDeliveryPersonButton = new JButton(
			"Insert Delivery Person");
	private JButton InsertSubscriptionButton = new JButton(
			"Insert Subscription");
	private JButton viewDeliveryButton = new JButton("View Delivery Person");
	private JButton viewSubscriptionButton = new JButton("View Subscription");

	private JCheckBox mySubscriptionReceivedYes = new JCheckBox("Yes");
	private JCheckBox mySubscriptionReceivedNo = new JCheckBox("No");

	private JTextField myCustomerFirstNameTextField = new JTextField();
	private JTextField myCustomerLastNameTextField = new JTextField();
	private JTextField myCustomerHouseAddressTextField = new JTextField();
	private JTextField myCustomerStreetAddressTextField = new JTextField();
	private JTextField myCustomerRegoinTextField = new JTextField();
	private JTextField myCustomerEmailTextField = new JTextField();
	private JTextField myCustomerHolidaysTextField = new JTextField();
	private JTextField myCustomerPhoneNumberTextField = new JTextField();
	private JTextField myPuplicationIDTextField = new JTextField();
	private JTextField myPublicationNameTextField = new JTextField();
	private JTextField myPublicationTypeTextField = new JTextField();
	private JTextField myPublicationGenerTextField = new JTextField();
	private JTextField myPublicationPriceTextField = new JTextField();
	private JTextField myPublicationDateTextField = new JTextField();
	private JTextField myDeliverFirstNameTextField = new JTextField();
	private JTextField myDeliverLastNameTextField = new JTextField();
	private JTextField myDeliveryPhoneNumberTextField = new JTextField();
	private JTextField myDeliveryRegoinTextField = new JTextField();
	private JTextField myPuplicationIDForSubscriptionTextField = new JTextField();
	private JTextField myDeliveryIDTextField = new JTextField();
	private JTextField myCustomerIDTextField = new JTextField();
	private JTextField mySubscriptionAmountTextField = new JTextField();
	private JTextField mySubscriptionLastPaymentTextField = new JTextField();
	private JTextField mySubscriptionDeliveryDateTextField = new JTextField();

	private final JTextArea textArea = new JTextArea(15, 30);
	private final JScrollPane scrollPane = new JScrollPane();

	private JLabel lblCustomerFirstName = new JLabel("Customer First Name");
	private JLabel lblCustomerLastName = new JLabel("Customer Last Name");
	private JLabel lblPublicationId = new JLabel("Publication Id");
	private JLabel lblCustomerAddress = new JLabel("House Address ");
	private JLabel lblCustomerHolidays = new JLabel("Street Address");
	private JLabel lblPuplicationName = new JLabel("Region");
	private JLabel lblPuplicationType = new JLabel("Email");
	private JLabel lblPublicationGener = new JLabel("Customer Holidays");
	private JLabel lblDeliveryDates = new JLabel("Phone Number");
	private JLabel lblCustomersInformation = new JLabel("Customer Information");
	private JLabel lblPublicationsInformation = new JLabel(
			"Publication Information");
	private JLabel lblName = new JLabel("Name");
	private JLabel lblType = new JLabel("Type");
	private JLabel lblGener = new JLabel("Gener");
	private JLabel lblPrice = new JLabel("Price");
	private JLabel lblDate = new JLabel("Date");
	private JLabel lblDeliveryInformation = new JLabel("Delivery Information");
	private JLabel lblNewLabel = new JLabel("First Name");
	private JLabel lblNewLabel_1 = new JLabel("Last Name");
	private JLabel lblNewLabel_2 = new JLabel("Phone No.");
	private JLabel lblNewLabel_3 = new JLabel("Region");
	private JLabel lblSubscriptionInformation = new JLabel(
			"Subscription Information");
	private JLabel lblNewLabel_4 = new JLabel("Publication ID");
	private JLabel lblNewLabel_5 = new JLabel("Delivery ID");
	private JLabel lblNewLabel_6 = new JLabel("Customer ID");
	private JLabel lblNewLabel_7 = new JLabel("Amount");
	private JLabel lblNewLabel_8 = new JLabel("Last Payment");
	private JLabel lblNewLabel_9 = new JLabel("Received");
	private JLabel lblNewLabel_10 = new JLabel("Delivery Date");

	public JDBCMainWindowContent() {

		initiate_db_connection();
		insertCustomerIntoDB();
		insertPublicationIntoDB();
		insertDeliveryIntoDB();
		insertSubscriptionIntoDB();
		viewCustomerHoliday();
		viewPublication();
		viewDeliverPersons();
		viewSubscription();
		printSummary();
		clearTextFields();

		content = getContentPane();
		content.setLayout(null);

		insertCustomerButton.setSize(136, 25);
		clearButton.setSize(100, 25);

		insertCustomerButton.setLocation(135, 507);
		clearButton.setLocation(1043, 601);

		printSummaryButton.setBounds(979, 643, 183, 29);
		viewCustmersHolidaysButton.setBounds(710, 599, 189, 29);
		viewPublicationsButton.setBounds(896, 599, 147, 29);
		insertPublicationButton.setBounds(460, 245, 166, 29);
		insertDeliveryPersonButton.setBounds(453, 541, 173, 29);
		InsertSubscriptionButton.setBounds(861, 489, 147, 29);
		viewDeliveryButton.setBounds(680, 643, 156, 29);
		viewSubscriptionButton.setBounds(837, 643, 139, 29);

		myCustomerFirstNameTextField.setBounds(200, 45, 200, 25);
		myCustomerLastNameTextField.setBounds(200, 95, 200, 25);
		myCustomerHouseAddressTextField.setBounds(200, 195, 200, 25);
		myCustomerStreetAddressTextField.setBounds(200, 245, 200, 25);
		myCustomerRegoinTextField.setBounds(200, 295, 200, 25);
		myCustomerEmailTextField.setBounds(200, 345, 200, 25);
		myCustomerHolidaysTextField.setBounds(200, 395, 200, 25);
		myCustomerPhoneNumberTextField.setBounds(200, 451, 200, 25);
		myPuplicationIDTextField.setBounds(200, 145, 200, 28);
		myPublicationNameTextField.setBounds(508, 44, 173, 28);
		myPublicationTypeTextField.setBounds(508, 84, 173, 28);
		myPublicationGenerTextField.setBounds(508, 124, 173, 28);
		myPublicationPriceTextField.setBounds(508, 164, 173, 28);
		myPublicationDateTextField.setBounds(508, 204, 173, 28);
		myDeliverFirstNameTextField.setBounds(542, 354, 134, 28);
		myDeliverLastNameTextField.setBounds(542, 404, 134, 28);
		myDeliveryPhoneNumberTextField.setBounds(542, 457, 134, 28);
		myDeliveryRegoinTextField.setBounds(542, 504, 134, 28);
		myPuplicationIDForSubscriptionTextField.setBounds(913, 145, 134, 28);
		myDeliveryIDTextField.setBounds(913, 194, 134, 28);
		myCustomerIDTextField.setBounds(913, 244, 134, 28);
		mySubscriptionAmountTextField.setBounds(913, 294, 134, 28);
		mySubscriptionLastPaymentTextField.setBounds(913, 344, 134, 28);
		mySubscriptionDeliveryDateTextField.setBounds(913, 444, 134, 28);
		mySubscriptionReceivedYes.setBounds(913, 393, 54, 29);
		mySubscriptionReceivedNo.setBounds(979, 393, 61, 29);

		lblCustomerFirstName.setBounds(50, 50, 139, 16);
		lblCustomerLastName.setBounds(50, 100, 139, 16);
		lblPublicationId.setBounds(50, 150, 136, 16);
		lblCustomerAddress.setBounds(50, 200, 139, 16);
		lblCustomerHolidays.setBounds(50, 250, 139, 16);
		lblPuplicationName.setBounds(50, 300, 139, 16);
		lblPuplicationType.setBounds(50, 350, 139, 16);
		lblPublicationGener.setBounds(50, 400, 139, 16);
		lblDeliveryDates.setBounds(50, 450, 139, 16);
		lblCustomersInformation.setFont(new Font("Lucida Grande", Font.BOLD
				| Font.ITALIC, 13));
		lblCustomersInformation.setBounds(198, 17, 166, 16);
		lblPublicationsInformation.setFont(new Font("Lucida Grande", Font.BOLD
				| Font.ITALIC, 13));
		lblPublicationsInformation.setBounds(525, 17, 173, 16);
		lblName.setBounds(450, 50, 46, 16);
		lblType.setBounds(450, 90, 46, 16);
		lblGener.setBounds(450, 130, 46, 16);
		lblPrice.setBounds(450, 170, 46, 16);
		lblDate.setBounds(450, 210, 46, 16);
		lblDeliveryInformation.setFont(new Font("Lucida Grande", Font.BOLD
				| Font.ITALIC, 13));
		lblDeliveryInformation.setBounds(472, 326, 139, 16);
		lblNewLabel.setBounds(450, 360, 71, 16);
		lblNewLabel_1.setBounds(450, 410, 71, 16);
		lblNewLabel_2.setBounds(450, 460, 71, 22);
		lblNewLabel_3.setBounds(450, 510, 61, 16);
		lblSubscriptionInformation.setFont(new Font("Lucida Grande", Font.BOLD
				| Font.ITALIC, 13));
		lblSubscriptionInformation.setBounds(837, 100, 182, 16);
		lblNewLabel_4.setBounds(800, 150, 101, 16);
		lblNewLabel_5.setBounds(800, 200, 101, 16);
		lblNewLabel_6.setBounds(800, 250, 79, 16);
		lblNewLabel_7.setBounds(800, 300, 61, 16);
		lblNewLabel_8.setBounds(800, 350, 101, 16);
		lblNewLabel_9.setBounds(800, 400, 61, 16);
		lblNewLabel_10.setBounds(800, 450, 92, 16);

		scrollPane.setBounds(466, 45, 385, 425);
		scrollPane.setViewportView(textArea);

		content.add(insertCustomerButton);
		content.add(clearButton);
		content.add(printSummaryButton);
		content.add(viewCustmersHolidaysButton);
		content.add(viewPublicationsButton);
		content.add(insertDeliveryPersonButton);
		content.add(InsertSubscriptionButton);
		content.add(viewDeliveryButton);
		content.add(viewSubscriptionButton);
		content.add(insertPublicationButton);
		content.add(myCustomerFirstNameTextField);
		content.add(myCustomerLastNameTextField);
		content.add(myCustomerHouseAddressTextField);
		content.add(myCustomerStreetAddressTextField);
		content.add(myCustomerRegoinTextField);
		content.add(myCustomerEmailTextField);
		content.add(myCustomerHolidaysTextField);
		content.add(myCustomerPhoneNumberTextField);
		content.add(myPuplicationIDTextField);
		content.add(myPublicationNameTextField);
		content.add(myPublicationTypeTextField);
		content.add(myPublicationGenerTextField);
		content.add(myPublicationPriceTextField);
		content.add(myPublicationDateTextField);
		content.add(lblCustomerFirstName);
		content.add(lblCustomerLastName);
		content.add(lblPublicationId);
		content.add(lblCustomerAddress);
		content.add(lblCustomerHolidays);
		content.add(lblPuplicationName);
		content.add(lblPuplicationType);
		content.add(lblPublicationGener);
		content.add(lblDeliveryDates);
		content.add(lblCustomersInformation);
		content.add(lblPublicationsInformation);
		content.add(lblName);
		content.add(lblType);
		content.add(lblGener);
		content.add(lblPrice);
		content.add(lblDate);
		content.add(lblDeliveryInformation);
		content.add(lblNewLabel);
		content.add(lblNewLabel_1);
		content.add(lblNewLabel_2);
		content.add(lblNewLabel_3);
		content.add(myDeliverFirstNameTextField);
		content.add(myDeliverLastNameTextField);
		content.add(myDeliveryPhoneNumberTextField);
		content.add(myDeliveryRegoinTextField);
		content.add(lblSubscriptionInformation);
		content.add(lblNewLabel_4);
		content.add(lblNewLabel_5);
		content.add(lblNewLabel_6);
		content.add(lblNewLabel_7);
		content.add(lblNewLabel_8);
		content.add(lblNewLabel_9);
		content.add(myPuplicationIDForSubscriptionTextField);
		content.add(myDeliveryIDTextField);
		content.add(myCustomerIDTextField);
		content.add(mySubscriptionAmountTextField);
		content.add(mySubscriptionLastPaymentTextField);
		content.add(lblNewLabel_10);
		content.add(mySubscriptionDeliveryDateTextField);
		content.add(InsertSubscriptionButton);
		content.add(mySubscriptionReceivedYes);
		content.add(mySubscriptionReceivedNo);
		content.add(viewDeliveryButton);
		content.add(viewSubscriptionButton);

		setSize(1250, 750);
		setVisible(true);

	}

	public String initiate_db_connection() {
		String connected = "";
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/Newsagent";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "Nasser88");
			// Create a generic statement which is passed to the
			// TestInternalFrame1
			stmt = con.createStatement();
			connected = "Connected to database successfully.";
			JOptionPane.showMessageDialog(null, connected, "Successful",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null,
					"Failed to connect to the database.",
					"Error Connecting to Database", JOptionPane.ERROR_MESSAGE);

		}
		System.out.println(connected);
		return connected;
	}

	// insert into CUSTOMER table
	public String insertCustomerIntoDB() {

		insertCustomerButton.addActionListener(new ActionListener() {

			
			public void actionPerformed(ActionEvent e) {

				try {

					// customerID, publicationID, firstName, lastName,
					// houseAddress, streetAddress, region, email, holidayDate,
					// phoneNumber,

					if (!myPuplicationIDTextField.getText().equals("")
							&& !myCustomerFirstNameTextField.getText().equals(
									"")
							&& !myCustomerLastNameTextField.getText()
									.equals("")
							&& !myCustomerHouseAddressTextField.getText()
									.equals("")
							&& !myCustomerStreetAddressTextField.getText()
									.equals("")
							&& !myCustomerRegoinTextField.getText().equals("")
							&& !myCustomerHolidaysTextField.getText()
									.equals("")
							&& !myCustomerPhoneNumberTextField.getText()
									.equals("")) {
						String insert = "INSERT INTO CUSTOMER VALUES(null,"
								+ myPuplicationIDTextField.getText() + ",'"
								+ myCustomerFirstNameTextField.getText()
								+ "','" + myCustomerLastNameTextField.getText()
								+ "','"
								+ myCustomerHouseAddressTextField.getText()
								+ "','"
								+ myCustomerStreetAddressTextField.getText()
								+ "','" + myCustomerRegoinTextField.getText()
								+ "','" + myCustomerEmailTextField.getText()
								+ "','" + myCustomerHolidaysTextField.getText()
								+ "','"
								+ myCustomerPhoneNumberTextField.getText()
								+ " ');";

						stmt.executeUpdate(insert);

						insertCustomer = "Successfully saved.";
						JOptionPane.showMessageDialog(null, insertCustomer,

						"Successful", JOptionPane.INFORMATION_MESSAGE);

					} else
						JOptionPane.showMessageDialog(null,
								"Please complete all information.", "Error",
								JOptionPane.ERROR_MESSAGE);
				} catch (SQLException sqle) {

					JOptionPane.showMessageDialog(null,
							"Please complete all information.", "Error",
							JOptionPane.ERROR_MESSAGE);

				}

			}
		});
		return insertCustomer;

	}

	// insert into PUBLICATION table
	public String insertPublicationIntoDB() {

		insertPublicationButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {

					// customerID, publicationID, firstName, lastName,
					// houseAddress, streetAddress, region, email, holidayDate,
					// phoneNumber

					if (!myPublicationNameTextField.getText().equals("")
							&& !myPublicationTypeTextField.getText().equals("")
							&& !myPublicationGenerTextField.getText()
									.equals("")
							&& !myPublicationPriceTextField.getText()
									.equals("")
							&& !myPublicationDateTextField.getText().equals("")) {

						String insert = "INSERT INTO PUBLICATION VALUES(null,'"
								+ myPublicationNameTextField.getText() + "','"
								+ myPublicationTypeTextField.getText() + "','"
								+ myPublicationGenerTextField.getText() + "',"
								+ myPublicationPriceTextField.getText() + ",'"
								+ myPublicationDateTextField.getText() + " ');";

						stmt.executeUpdate(insert);

						insertPublication = "Successfully saved.";
						JOptionPane.showMessageDialog(null,
								"Successfully saved.", "Successful",
								JOptionPane.INFORMATION_MESSAGE);
					} else
						JOptionPane.showMessageDialog(null,
								"Please complete all information.", "Error",
								JOptionPane.ERROR_MESSAGE);

				} catch (SQLException sqle) {
					JOptionPane.showMessageDialog(null,
							"Please complete all information.", "Error ",
							JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		return insertPublication;
	}

	// insert into DELIVERYPERSON table
	public String insertDeliveryIntoDB() {

		insertDeliveryPersonButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					if (!myDeliverFirstNameTextField.getText().equals("")
							&& !myDeliverLastNameTextField.getText().equals("")
							&& !myDeliveryPhoneNumberTextField.getText()
									.equals("")
							&& !myDeliveryRegoinTextField.getText().equals("")) {

						String insert = "INSERT INTO DELIVERYPERSON VALUES(null,'"
								+ myDeliverFirstNameTextField.getText()
								+ "','"
								+ myDeliverLastNameTextField.getText()
								+ "','"
								+ myDeliveryPhoneNumberTextField.getText()
								+ "','"
								+ myDeliveryRegoinTextField.getText()
								+ " ');";

						stmt.executeUpdate(insert);

						insertDelivery = "Successfully saved.";
						JOptionPane.showMessageDialog(null,
								"Successfully saved.", "Successful",
								JOptionPane.INFORMATION_MESSAGE);
					} else
						JOptionPane.showMessageDialog(null,
								"Please complete all information.", "Error",
								JOptionPane.ERROR_MESSAGE);
				} catch (SQLException sqle) {
					JOptionPane.showMessageDialog(null,
							"Please complete all information.", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}

		});
		return insertDelivery;
	}

	// insert into SUBSCRIPTION table
	public String insertSubscriptionIntoDB() {

		InsertSubscriptionButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				try {

					// subscriptionID, publicationID, deliveryPersonID,
					// customerID, amount, lastPaymentDate, received,
					// deliveryDate,

					if (!myPuplicationIDForSubscriptionTextField.getText()
							.equals("")
							&& !myDeliveryIDTextField.getText().equals("")
							&& !myCustomerIDTextField.getText().equals("")
							&& !mySubscriptionAmountTextField.getText().equals(
									"")
							&& !mySubscriptionLastPaymentTextField.getText()
									.equals("")
							&& !mySubscriptionDeliveryDateTextField.getText()
									.equals("")) {
						String insert = "";
						if (mySubscriptionReceivedYes.isSelected()) {

							insert = "INSERT INTO SUBSCRIPTION VALUES(null,"
									+ myPuplicationIDForSubscriptionTextField
											.getText()
									+ ","
									+ myDeliveryIDTextField.getText()
									+ ","
									+ myCustomerIDTextField.getText()
									+ ","
									+ mySubscriptionAmountTextField.getText()
									+ ",'"
									+ mySubscriptionLastPaymentTextField
											.getText()
									+ "',"
									+ true
									+ ",'"
									+ mySubscriptionDeliveryDateTextField
											.getText() + " ');";
						}

						else if (mySubscriptionReceivedNo.isSelected()) {

							insert = "INSERT INTO SUBSCRIPTION VALUES("
									+ null
									+ ","
									+ myPuplicationIDForSubscriptionTextField
											.getText()
									+ ","
									+ myDeliveryIDTextField.getText()
									+ ","
									+ myCustomerIDTextField.getText()
									+ ","
									+ mySubscriptionAmountTextField.getText()
									+ ",'"
									+ mySubscriptionLastPaymentTextField
											.getText()
									+ "',"
									+ false
									+ ",'"
									+ mySubscriptionDeliveryDateTextField
											.getText() + " ');";

						}
						stmt.executeUpdate(insert);

						insertSubscription = "Successfully saved.";
						JOptionPane.showMessageDialog(null, insertSubscription,
								"Successful", JOptionPane.INFORMATION_MESSAGE);
					}else
					JOptionPane.showMessageDialog(null,
							"Please complete all information.", "Error",
							JOptionPane.ERROR_MESSAGE);
				} catch (SQLException sqle) {
					JOptionPane.showMessageDialog(null,
							"Please complete all information.", "Error",
							JOptionPane.ERROR_MESSAGE);
				}
			}

		});

		return insertSubscription;

	}

	// View Customer Holiday
	public String viewCustomerHoliday() {
		viewCustmersHolidaysButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				try {

					if (!myCustomerFirstNameTextField.getText().equals("")
							&& !myCustomerLastNameTextField.getText()
									.equals("")) {

						cmd = "SELECT holidayDate FROM CUSTOMER WHERE firstName  = '"
								+ myCustomerFirstNameTextField.getText()
								+ " '  and lastName = '"
								+ myCustomerLastNameTextField.getText() + "'; ";

						rs = stmt.executeQuery(cmd);

						if (rs.next()) {
							customerHolidays = "Holidate Days: "
									+ rs.getString(1);

							JOptionPane.showMessageDialog(null,
									customerHolidays, "Successful",
									JOptionPane.INFORMATION_MESSAGE);

						} else
							JOptionPane.showMessageDialog(null,
									"Please enter valid first and last name.",
									"Error",
									JOptionPane.ERROR_MESSAGE);
					}else
					JOptionPane.showMessageDialog(null,
							"Please enter valid first and last name.", "Error",
							JOptionPane.ERROR_MESSAGE);
				} catch (SQLException sqle) {

					JOptionPane.showMessageDialog(null,
							"Please complete all information.", "Error",
							JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		return customerHolidays;

	}

	// view all publications
	public String viewPublication() {
		viewPublicationsButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				try {
					textArea.setText("");
					cmd = "SELECT * FROM PUBLICATION ; ";
					rs = stmt.executeQuery(cmd);

					while (rs.next()) {

						// publicationID, publicationName, publicationType,
						// publicationGenre, price, publicationDate
						int id = rs.getInt("publicationID");
						String puplicationName = rs
								.getString("publicationName");
						String publicationType = rs
								.getString("publicationType");
						String publicationGenre = rs
								.getString("publicationGenre");
						double price = rs.getDouble("price");
						String publicationDate = rs
								.getString("publicationDate");

						allPublications = "ID:	" + id + "\nName:	"
								+ puplicationName + "\nType:	"
								+ publicationType + "\nGener:	"
								+ publicationGenre + "\nPrice:	" + price
								+ "\nDate:	" + publicationDate
								+ "\n---------------------\n";

						textArea.append(allPublications);

					}
					if (textArea.getText().equals("")) {
						JOptionPane.showMessageDialog(null,
								"No Publication Found", "Error",
								JOptionPane.ERROR_MESSAGE);
					} else
						JOptionPane.showMessageDialog(null, scrollPane,
								"Successful", JOptionPane.INFORMATION_MESSAGE);

				} catch (SQLException sqle) {
					System.err.println("Error with insert:\n" + sqle.toString());
					JOptionPane.showMessageDialog(null,
							"Please complete all information.",
							"Error Updating", JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		return allPublications;
	}

	// view all Deliver Persons
	public String viewDeliverPersons() {
		viewDeliveryButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					textArea.setText("");
					cmd = "SELECT * FROM DELIVERYPERSON ; ";
					rs = stmt.executeQuery(cmd);

					while (rs.next()) {

						// deliveryPersonID, firstName, lastName, phoneNumber,
						// region
						int id = rs.getInt("deliveryPersonID");
						String deliverFirstName = rs.getString("firstName");
						String deliverLastName = rs.getString("lastName");
						String deliveryPhoneNumber = rs
								.getString("phoneNumber");
						String deliveryRrgion = rs.getString("region");

						allDeliveryPersons = "ID:	" + id + "\nFirstName:	"
								+ deliverFirstName + "\nLastName:	"
								+ deliverLastName + "\nPhone No.:	"
								+ deliveryPhoneNumber + "\nRegion:	"
								+ deliveryRrgion + "\n---------------------\n";

						textArea.append(allDeliveryPersons);

					}
					if (textArea.getText().equals("")) {
						JOptionPane.showMessageDialog(null,
								"No Delivery Persons Found", "Error",
								JOptionPane.ERROR_MESSAGE);
					} else
						JOptionPane.showMessageDialog(null, scrollPane,
								"Successful", JOptionPane.INFORMATION_MESSAGE);

				} catch (SQLException sqle) {
					System.err.println("Error with insert:\n" + sqle.toString());
					JOptionPane.showMessageDialog(null,
							"Please complete all information.",
							"Error Updating", JOptionPane.ERROR_MESSAGE);
				}

			}
		});

		return allDeliveryPersons;

	}

	// view all Subscriptions
	public String viewSubscription() {
		viewSubscriptionButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				try {
					textArea.setText("");
					cmd = "SELECT * FROM SUBSCRIPTION ; ";

					rs = stmt.executeQuery(cmd);
					allSubscriptions = "";
					while (rs.next()) {

						// subscriptionID, publicationID, deliveryPersonID,
						// customerID, amount, lastPaymentDate, received,
						// deliveryDate,
						int subscriptionID = rs.getInt("subscriptionID");
						int publicationID = rs.getInt("publicationID");
						int deliveryPersonID = rs.getInt("deliveryPersonID");
						int customerID = rs.getInt("customerID");
						String amount = rs.getString("amount");
						String lastPaymentDate = rs
								.getString("lastPaymentDate");
						boolean received = rs.getBoolean("received");
						String deliveryDate = rs.getString("deliveryDate");

						allSubscriptions = "subscription ID: " + subscriptionID
								+ "\nPublication ID: " + publicationID
								+ "\nDelivery Person ID: " + deliveryPersonID
								+ "\nCustomer ID: " + customerID + "\nAmount: "
								+ amount + "\nLast Payment Date: "
								+ lastPaymentDate + "\nReceived: " + received
								+ "\nDelivery Date.: " + deliveryDate
								+ "\n---------------------\n";

						textArea.append(allSubscriptions);

					}
					if (textArea.getText().equals("")) {
						JOptionPane.showMessageDialog(null,
								"No Subscriptions Found", "Error",
								JOptionPane.ERROR_MESSAGE);
					} else
						JOptionPane.showMessageDialog(null, scrollPane,
								"Successful", JOptionPane.INFORMATION_MESSAGE);

				} catch (SQLException sqle) {
					JOptionPane.showMessageDialog(null,
							"Please complete all information.",
							"Error Updating", JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		return allSubscriptions;

	}

	// print customer publication Summary
	public String printSummary() {
		printSummaryButton.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {

				try {
					textArea.setText("");

					cmd = " select customer.firstName,customer.lastName,publication.publicationName,publication.publicationType,publication.publicationGenre,subscription.deliveryDate from customer,subscription,publication where subscription.customerID=customer.customerID and subscription.publicationID=publication.publicationID; ";

					rs = stmt.executeQuery(cmd);

					while (rs.next()) {
						String customerFirstName = rs.getString("FirstName");
						String customerLastName = rs.getString("LastName");
						String name = rs.getString("publicationName");
						String type = rs.getString("publicationType");
						String gener = rs.getString("publicationGenre");
						String deliveryDate = rs.getString("deliveryDate");

						printSummary = "Custmoer First Name: "
								+ customerFirstName + "\nCustmoerLastName: "
								+ customerLastName + "\nPublicationName: "
								+ name + "\nPublication Type: " + type
								+ "\nPublication Gener: " + gener
								+ "\nDelivery Date: " + deliveryDate
								+ "\n---------------------\n";
						textArea.append(printSummary);

					}
					if (textArea.getText().equals("")) {
						JOptionPane.showMessageDialog(null, "No Summary Found",
								"Error", JOptionPane.ERROR_MESSAGE);
					} else
						JOptionPane.showMessageDialog(null, scrollPane,
								"Customer's publications Summary",
								JOptionPane.INFORMATION_MESSAGE);

				} catch (SQLException sqle) {
					JOptionPane
							.showMessageDialog(
									null,
									"Please enter valid Customer's First and Last Names",
									"Error Searching",
									JOptionPane.ERROR_MESSAGE);
				}

			}
		});
		return printSummary;

	}

	// Clear Text Field
	public void clearTextFields() {

		clearButton.addActionListener(new ActionListener() {

		
			public void actionPerformed(ActionEvent e) {

				myCustomerFirstNameTextField.setText("");
				myCustomerLastNameTextField.setText("");
				myCustomerHouseAddressTextField.setText("");
				myCustomerStreetAddressTextField.setText("");
				myCustomerRegoinTextField.setText("");
				myCustomerEmailTextField.setText("");
				myCustomerHolidaysTextField.setText("");
				myCustomerPhoneNumberTextField.setText("");
				myPuplicationIDTextField.setText("");
				myPublicationNameTextField.setText("");
				myPublicationTypeTextField.setText("");
				myPublicationGenerTextField.setText("");
				myPublicationPriceTextField.setText("");
				myPublicationDateTextField.setText("");
				myDeliverFirstNameTextField.setText("");
				myDeliverLastNameTextField.setText("");
				myDeliveryIDTextField.setText("");
				myDeliveryPhoneNumberTextField.setText("");
				myDeliveryRegoinTextField.setText("");
				myPuplicationIDForSubscriptionTextField.setText("");
				mySubscriptionAmountTextField.setText("");
				mySubscriptionDeliveryDateTextField.setText("");
				mySubscriptionLastPaymentTextField.setText("");
				myCustomerIDTextField.setText("");
				mySubscriptionReceivedNo.setSelected(false);
				mySubscriptionReceivedYes.setSelected(false);

			}
		});

	}

}
