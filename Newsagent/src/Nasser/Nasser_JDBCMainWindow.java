import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;

public class Nasser_JDBCMainWindow extends JFrame implements ActionListener {
	private JMenuItem exitItem;

	public Nasser_JDBCMainWindow() {
		// Sets the Window Title
		super("Newsagent");

		// Setup fileMenu and its elements
		JMenuBar menuBar = new JMenuBar();
		JMenu fileMenu = new JMenu("File");
		exitItem = new JMenuItem("Exit");
		fileMenu.add(exitItem);
		menuBar.add(fileMenu);
		setJMenuBar(menuBar);

		// Add a listener to the Exit Menu Item
		exitItem.addActionListener(this);

		// Create an instance of our class JDBCMainWindowContent
		Nasser_JDBCMainWindowContent aWindowContent = new Nasser_JDBCMainWindowContent("Newsagent");
		// Add the instance to the main section of the window
		getContentPane().add(aWindowContent);

		addWindowListener(new java.awt.event.WindowAdapter() {
			@Override
			public void windowClosing(java.awt.event.WindowEvent windowEvent) {
				System.exit(DISPOSE_ON_CLOSE);
			}
		});

		setSize(1260, 615);
		setResizable(false);
		setVisible(true);
	}

	// The event handling for the main frame
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(exitItem)) {
			this.dispose();

		}
	}
}