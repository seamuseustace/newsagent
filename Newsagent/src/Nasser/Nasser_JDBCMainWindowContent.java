import java.awt.Container;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JButton;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
@SuppressWarnings("serial")
public class Nasser_JDBCMainWindowContent extends JInternalFrame implements ActionListener {
	String cmd = null;

	// DB Connectivity Attributes
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	private Container content;

	private static Nasser_QueryTableModel TableModel = new Nasser_QueryTableModel();

	// Add the models to JTabels
	private JTable TableofDBContents = new JTable(TableModel);

	// Buttons for inserting, and updating members
	// also a clear button to clear patients panel
	private JButton updateButton = new JButton("Update");
	private JButton insertButton = new JButton("Insert");
	private JButton exportButton = new JButton("Export");
	private JButton deleteButton = new JButton("Delete");
	private JButton clearButton = new JButton("Clear");

	public Nasser_JDBCMainWindowContent(String aTitle) {
		// setting up the GUI
		super(aTitle, false, false, false, false);
		setEnabled(true);

		initiate_db_conn();
		// add the 'main' panel to the Internal Frame
		content = getContentPane();
		content.setLayout(null);

		insertButton.setSize(100, 25);
		updateButton.setSize(100, 25);
		exportButton.setSize(100, 25);
		deleteButton.setSize(100, 25);
		clearButton.setSize(100, 25);

		insertButton.setLocation(370, 10);
		updateButton.setLocation(370, 110);
		exportButton.setLocation(370, 160);
		deleteButton.setLocation(370, 60);
		clearButton.setLocation(370, 210);

		insertButton.addActionListener(this);
		updateButton.addActionListener(this);
		exportButton.addActionListener(this);
		deleteButton.addActionListener(this);
		clearButton.addActionListener(this);

		content.add(insertButton);
		content.add(updateButton);
		content.add(exportButton);
		content.add(deleteButton);
		content.add(clearButton);

		TableofDBContents.setPreferredScrollableViewportSize(new Dimension(900, 300));

		// setSize(990, 665);
		setVisible(true);

		TableModel.refreshFromDB(stmt);
	}

	public void initiate_db_conn() {
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/Newsagent";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "admin");
			// Create a generic statement which is passed to the
			// TestInternalFrame1
			stmt = con.createStatement();
			JOptionPane.showMessageDialog(null, "Connected to database successfully.", "Successful",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n" + e.getMessage());
			JOptionPane.showMessageDialog(null, "Failed to connect to the database.", "Error Connecting to Database",
					JOptionPane.ERROR_MESSAGE);
		}
	}

	// event handling
	public void actionPerformed(ActionEvent e) {
		Object target = e.getSource();

		if (target == insertButton) {
			try {

				stmt.addBatch("SQL CODE HERE");													// SQL CODE HERE

				int[] results = stmt.executeBatch();

				JOptionPane.showMessageDialog(null, "Successfully saved.", "Successful",
						JOptionPane.INFORMATION_MESSAGE);

			} catch (SQLException sqle) {
				System.err.println("Error with insert:\n" + sqle.toString());
				JOptionPane.showMessageDialog(null, "Please complete all information.", "Error Updating",
						JOptionPane.ERROR_MESSAGE);
			} finally {
				TableModel.refreshFromDB(stmt);
			}
		}
		if (target == deleteButton) {

			try {
				stmt.addBatch("SQL CODE HERE");															// SQL CODE HERE

				int[] results = stmt.executeBatch();

				JOptionPane.showMessageDialog(null, "Successfully deleted.", "Successful",
						JOptionPane.INFORMATION_MESSAGE);
			} catch (SQLException sqle) {
				System.err.println("Error with delete:\n" + sqle.toString());
				JOptionPane.showMessageDialog(null, "An error occured while deleting information.", "Error Deleting",
						JOptionPane.ERROR_MESSAGE);
			} finally {
				TableModel.refreshFromDB(stmt);
			}
		}

		if (target == updateButton) {
			try {
				stmt.addBatch("JAVA CODE HERE");

				int[] results = stmt.executeBatch();

				rs = stmt.executeQuery("SQL CODE HERE");												// SQL CODE HERE
				rs.next();
				rs.close();

			} catch (SQLException sqle) {
				System.err.println("Error with update:\n" + sqle.toString());
				JOptionPane.showMessageDialog(null, "Please complete all information.", "Error Updating",
						JOptionPane.ERROR_MESSAGE);
			} finally {
				TableModel.refreshFromDB(stmt);
			}
		}

	}

	private void writeToFile(ResultSet rs) {
		try {
			FileWriter outputFile = new FileWriter("Newsagent.csv");
			PrintWriter printWriter = new PrintWriter(outputFile);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			for (int i = 0; i < numColumns; i++) {
				printWriter.print(rsmd.getColumnLabel(i + 1) + ",");
			}
			printWriter.print("\n");
			while (rs.next()) {
				for (int i = 0; i < numColumns; i++) {
					printWriter.print(rs.getString(i + 1) + ",");
				}
				printWriter.print("\n");
				printWriter.flush();
			}
			printWriter.close();
		} catch (Exception e) {
			e.printStackTrace();
			JOptionPane.showMessageDialog(null, "An error occured while exporting.", "Error Exporting",
					JOptionPane.ERROR_MESSAGE);
		}
	}

}
