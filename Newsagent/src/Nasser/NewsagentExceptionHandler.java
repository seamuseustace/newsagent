package Nasser;

public class NewsagentExceptionHandler extends Exception {

	String myExceptionMessage;

	public NewsagentExceptionHandler(String error) {
		myExceptionMessage = error;
	}

	public String getMessage() {
		return myExceptionMessage;
	}

}
