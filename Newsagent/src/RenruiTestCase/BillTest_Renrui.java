package RenruiTestCase;

import RenruiTestCase.Bill_Renrui;
import junit.framework.TestCase;


public class BillTest_Renrui extends TestCase {

	// Test No:1
	// Test Objective: customer should be existed in database  ,and date should be a right form.
	// Input: valid customer name .and month
    // excepted output:True
	public void test1Bill(){
		
		Bill_Renrui first=new Bill_Renrui();
		
		
		try {

			assertSame(true, first.list_Bill("Emma",3));	
		} catch (BillExceptionHandler_Renrui e) {

			 e.getMessage();
		}
	}
		

	// Test No:2
	// Test Objective: customer should be existed in database 
	// Input:  customer name is not existed in database .
    // excepted output:exception thrown
	public void test2Bill(){
		
		Bill_Renrui first=new Bill_Renrui();
		
		try {

			assertSame(false, first.list_Bill("Eric",3));	
		} catch (BillExceptionHandler_Renrui e) {

			 e.getMessage();
		}
	}
		
	// Test No:3
	// Test Objective: customer name can not be null 
	// Input:  customer name null .
    // excepted output:exception thrown
	public void test3Bill(){
		
		Bill_Renrui first=new Bill_Renrui();
		
		
		try {

			assertSame(false, first.list_Bill("",3));	
		} catch (BillExceptionHandler_Renrui e) {

			 e.getMessage();
		}
	
	}
	// Test No:4
		// Test Objective: date should be a valid month.
		// Input:  a month with wrong.
	    // excepted output:exception thrown
		public void test4Bill(){
			
			Bill_Renrui first=new Bill_Renrui();
			
			
			try {

				assertSame(false, first.list_Bill("Emma",8));	
			} catch (BillExceptionHandler_Renrui e) {

				 e.getMessage();
			}
		}
		// Test No:5
		// Test Objective: month can not be null
		// Input: month is null .
	    // excepted output:exception thrown
		public void test6Bill(){
			
			Bill_Renrui first=new Bill_Renrui();
			
			try {

				assertSame(false, first.list_Bill("Emma",0));	
			} catch (BillExceptionHandler_Renrui e) {

				 e.getMessage();
			}
		}
/*------------------------------------------*/		
		
		
		// Test No:11
		// Test Objective: customer should be existed in database ,and month should be right.
		// Input: valid customer name .and month
	    // excepted output:True
		public void test11Calculatebill(){
			
      Bill_Renrui first=new Bill_Renrui();
			
			try {

				assertSame(true, first.month_total("Emma",2));	
			} catch (BillExceptionHandler_Renrui e) {

				 e.getMessage();
			}
		}
		
		
		// Test No:12
		// Test Objective: customer name should not be null
		// Input: null customer name but right month
	    // excepted output:False
		public void test12Calculatebill(){
			
      Bill_Renrui first=new Bill_Renrui();
			
			try {

				assertSame(true, first.month_total("",2));	
			} catch (BillExceptionHandler_Renrui e) {

				 e.getMessage();
			}
		}
		
		
		// Test No:13
		// Test Objective: customer should be existed in database
		// Input: wrong customer name that do not exist in database, but valid month
	    // excepted output:False
		public void test13Calculatebill(){
			
      Bill_Renrui first=new Bill_Renrui();
			
			try {

				assertSame(true, first.month_total("Helen",2));	
			} catch (BillExceptionHandler_Renrui e) {

				 e.getMessage();
			}
		}
		
		// Test No:14
		// Test Objective:  month should be right that should be exist in database
		// Input: valid customer name,but month is wrong
	    // excepted output:False
		public void test14Calculatebill(){
			
      Bill_Renrui first=new Bill_Renrui();
			
			try {

				assertSame(true, first.month_total("Emma",10));	
			} catch (BillExceptionHandler_Renrui e) {

				 e.getMessage();
			}
		}
		// Test No:15
		// Test Objective:  month should be right that should not be null
		// Input: valid customer name,but month is null
	    // excepted output:False
		public void test15Calculatebill(){
			
      Bill_Renrui first=new Bill_Renrui();
			
			try {

				assertSame(true, first.month_total("Emma",0));	
			} catch (BillExceptionHandler_Renrui e) {

				 e.getMessage();
			}
		}
				
} 
