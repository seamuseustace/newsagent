package RenruiTestCase;
import junit.framework.TestCase;

public class DB_Test_Renrui extends TestCase {

	// Test No:1
	// Test Objective: test connection with Mysql
	// excepted output:True
	//
	public void testConnectOne() {

		DB_Renrui first = new DB_Renrui();
		try {

			assertSame(true,first.connect());

		} catch (DbExceptionHandler_Renrui e) {

			 e.getMessage();
		}
	}

	
	/*--------------------------------------------------------*/
	
	
	    // Test No:1
		// Test Objective: test newspaper type is exist my database and output all .
		// Input: Newspaper that include database.
	    // excepted output:True
	public void testPublicaiton1() {

		DB_Renrui first = new DB_Renrui();
		assertSame(true,first.Print_publication("Newspaper"));
	}

		
		    // Test No:2
			// Test Objective: test magazine type is not exist my database and can't output all .
			// Input: magazine whose type not exist some  database.
		    // excepted output:False
		public void testPublicaiton2(){
			DB_Renrui second = new DB_Renrui();
			assertSame(false, second.Print_publication("MAGAZINE"));
		}
		   // Test No:3
					// Test Objective: Publication type should be a string and not null.
					// Input: a null string type
				    // excepted output:False
			public void testPublicaiton3(){
				
				DB_Renrui third = new DB_Renrui();
				
				assertSame(false, third.Print_publication(""));
			}
			
			/*--------------------------------------------------------*/
			// Test No:1
			// Test Objective: customer should be existed in database  ,and date should be a right form.
			// Input: valid customer name .and month
		    // excepted output:True
			public void testBill1(){
				
	         DB_Renrui bill= new DB_Renrui();
				
				assertSame(true, bill.check_bill("Emma", 3));
 
			}
				

			// Test No:2
			// Test Objective: customer should be existed in database 
			// Input:  customer name is not existed in database .
		    // excepted output:exception thrown
			public void testBill2(){
				
				   DB_Renrui bill= new DB_Renrui();
				
		
					assertSame(false, bill.check_bill("Eric",3));	
			
			}
				
			// Test No:3
			// Test Objective: customer name can not be null 
			// Input:  customer name null .
		    // excepted output:exception thrown
			public void testBill3(){
				
				
				   DB_Renrui bill= new DB_Renrui();
				
		
					assertSame(false, bill.check_bill("",3));	
			
			}
			// Test No:4
				// Test Objective: date should be a valid month.
				// Input:  a month with wrong.
			    // excepted output:exception thrown
				public void testBill4(){
					
					 DB_Renrui bill= new DB_Renrui();
						
						
						assertSame(false, bill.check_bill("Emma",8));	
				}
				// Test No:5
				// Test Objective: month can not be null
				// Input: month is null .
			    // excepted output:exception thrown
				public void testBill6(){
					
					 DB_Renrui bill= new DB_Renrui();
						
						
						assertSame(false, bill.check_bill("Emma",0));	
				}
				
		/*------------------------------------------*/		
				
				
				// Test No:11
				// Test Objective: customer should be existed in database ,and month should be right.
				// Input: valid customer name .and month
			    // excepted output:True
				public void testCalculateBill1(){
					
					 DB_Renrui bill= new DB_Renrui();
						
						
						assertSame(true, bill.bill_month("Emma", 2));	
					
				}
				
				
				// Test No:12
				// Test Objective: customer name should not be null
				// Input: null customer name but right month
			    // excepted output:False
				public void testCalculateBill2(){
					
	
					
		      DB_Renrui bill= new DB_Renrui();
				
				
				assertSame(false, bill.bill_month("", 2));	
				
				}
				// Test No:13
				// Test Objective: customer should be existed in database
				// Input: wrong customer name that do not exist in database, but valid month
			    // excepted output:False
				public void testCalculateBill3(){
					
					 DB_Renrui bill= new DB_Renrui();
						
						
						assertSame(false, bill.bill_month("Helen", 2));	
				}
				
				// Test No:14
				// Test Objective:  month should be right that should be exist in database
				// Input: valid customer name,but month is wrong
			    // excepted output:False
				public void testCalculateBill4(){
					
					 DB_Renrui bill= new DB_Renrui();
						
						
						assertSame(false, bill.bill_month("Emma", 12));	
				}
				// Test No:15
				// Test Objective:  month should be right that should not be null
				// Input: valid customer name,but month is null
			    // excepted output:False
				public void testCalculateBill5(){
					
		      Bill_Renrui first=new Bill_Renrui();
					
		      DB_Renrui bill= new DB_Renrui();
				
				assertSame(false, bill.bill_month("Emma", 0));	
				}
			
}
