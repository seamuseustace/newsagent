//package RenruiTestCase;
//
//import java.awt.BorderLayout;
//import java.awt.EventQueue;
//import java.awt.event.ActionEvent;
//import java.awt.event.ActionListener;
//import java.sql.Connection;
//import java.sql.DriverManager;
//import java.sql.ResultSet;
//import java.sql.ResultSetMetaData;
//import java.sql.SQLException;
//import java.sql.Statement;
//import java.util.ArrayList;
//import java.util.List;
//
//import javax.swing.JFrame;
//import javax.swing.JPanel;
//import javax.swing.border.EmptyBorder;
//import javax.swing.JOptionPane;
//import javax.swing.JTextField;
//import javax.swing.JButton;
//import javax.swing.JLabel;
//import javax.swing.JComboBox;
//import javax.swing.DefaultComboBoxModel;
//import javax.swing.JTextArea;
//
//
//public class Newsagent extends JFrame  implements ActionListener{
//
//	private JPanel contentPane;
//	private JTextField text;
//	private JTextField text2;
//	private JButton button;
//	private JButton button2;
//	private JButton button3;
//	private JLabel lblPublicationList;
//	private JLabel lblMonth;
//	private JTextArea textArea1;
//	private JLabel lblName;
//	private JTextArea textArea2;
//
//	private JComboBox comboBox;
//	private JTextArea textArea;
//	private JLabel lblBillList;
//	
//	 String url="jdbc:mysql://localhost:3307";
//     
//     private String userName="root";
//     private String password="admin";
//	
//	
//     Connection con; 
//     Statement stat=null;
//     ResultSet rs= null;
//     ResultSetMetaData rsmd=null;
//     private JTextField text3;
//     private JTextField text4;
//	/**
//	 * Launch the application.
//	 */
//	public static void main(String[] args) {
//		EventQueue.invokeLater(new Runnable() {
//			public void run() {
//				try {
//					Newsagent frame = new Newsagent();
//					frame.setVisible(true);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//			}
//		});
//	}
//
//	/**
//	 * Create the frame.
//	 */
//	public Newsagent() {
//		initialize();
//		connect();
//		
//	}
//	private void initialize() {
//		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//		setBounds(100, 100, 451, 426);
//		contentPane = new JPanel();
//		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
//		setContentPane(contentPane);
//		contentPane.setLayout(null);
//		
//		button = new JButton("Submit");
//		button.setBounds(294, 22, 117, 29);
//		contentPane.add(button);
//		
//		 lblPublicationList = new JLabel("Publication list: ");
//		 lblPublicationList.setBounds(19, 27, 138, 16);
//		contentPane.add(lblPublicationList);
//		
//	    comboBox = new JComboBox();
//	    comboBox.setBounds(169, 23, 110, 27);
//		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Magazine", "Newspaper", "Weekly publication"}));
//		contentPane.add(comboBox);
//		
//		textArea = new JTextArea();
//		textArea.setBounds(29, 55, 312, 56);
//		contentPane.add(textArea);
//		textArea.setLineWrap(true);        
//		 textArea.setWrapStyleWord(true); 
//		
//		
//		lblBillList = new JLabel("Bill list:");
//		lblBillList.setBounds(19, 143, 61, 16);
//		contentPane.add(lblBillList);
//		
//		text = new JTextField();
//		text.setBounds(69, 159, 86, 26);
//		contentPane.add(text);
//		text.setColumns(10);
//		
//		 lblName = new JLabel("Name:");
//		 lblName.setBounds(19, 164, 61, 16);
//		contentPane.add(lblName);
//		
//		text2 = new JTextField();
//		text2.setBounds(87, 192, 70, 26);
//		contentPane.add(text2);
//		text2.setColumns(10);
//		
//		 lblMonth = new JLabel("Month:");
//		 lblMonth.setBounds(19, 197, 61, 16);
//		contentPane.add(lblMonth);
//		
//	    textArea1 = new JTextArea();
//	    textArea1.setBounds(220, 160, 220, 100);
//		contentPane.add(textArea1);
//		textArea1.setLineWrap(true);        
//		 textArea1.setWrapStyleWord(true); 
//		
//		button2 = new JButton("Show bill");
//		button2.setBounds(19, 225, 117, 29);
//		contentPane.add(button2);
//		
//		JLabel lblCalculateBill = new JLabel("Calculate bill:");
//		lblCalculateBill.setBounds(19, 281, 117, 16);
//		contentPane.add(lblCalculateBill);
//		
//		JLabel lblNewLabel = new JLabel("Name:");
//		lblNewLabel.setBounds(19, 319, 92, 16);
//		contentPane.add(lblNewLabel);
//		
//		JLabel lblMonth_1 = new JLabel("Month:");
//		lblMonth_1.setBounds(19, 348, 61, 16);
//		contentPane.add(lblMonth_1);
//		
//		text3 = new JTextField();
//		text3.setBounds(87, 314, 92, 26);
//		contentPane.add(text3);
//		text3.setColumns(10);
//		
//		text4 = new JTextField();
//		text4.setBounds(92, 347, 70, 26);
//		contentPane.add(text4);
//		text4.setColumns(10);
//		
//		button3 = new JButton("calculate");
//		button3.setBounds(19, 376, 117, 29);
//		contentPane.add(button3);
//		
//		textArea2 = new JTextArea();
//		textArea2.setBounds(198, 281, 213, 117);
//		contentPane.add(textArea2);
//		textArea2.setLineWrap(true);        
//		 textArea2.setWrapStyleWord(true); 
//
//		button.addActionListener((ActionListener) this);
//		button2.addActionListener((ActionListener) this);
//		button3.addActionListener((ActionListener) this);
//	}
//	
//	public void actionPerformed(ActionEvent e){
//
//	  	Object target=e.getSource();
//	 	if (target==button)
//	 		
//	 		{  	
//	 		
//	 		String publication=comboBox.getSelectedItem().toString();
//	 		print_publication(publication);
//	 		
//	 		}
//	 		
//	 	if (target==button2)
//	 		
// 		{  	
// 		
// 		String name=text.getText();
// 		int month=Integer.parseInt(text2.getText());
// 		check_bill(name,month);
// 		}
//if (target==button3)
//	 		
// 		{  	
// 		
// 		String name=text3.getText();
// 		int month=Integer.parseInt(text4.getText());
// 		bill_month(name,month);
// 		}
//
//	}		   
//	 		
//public void print_publication(String publicationType){
//	 boolean b=false;
//	 List<Publication> PublicationList = new ArrayList();
//		try
//		{
//			 String select="select * from publication "+"where publicationType= '"+publicationType+"'";
//				rs=stat.executeQuery(select);
//				rsmd = rs.getMetaData();
//				
//				
//				 while(rs.next())
//				    	 
//				     { b=true;
//			      
//				    
//				       Publication p = new Publication(rs.getInt("publicationID"), rs.getString("publicationType"), rs.getString("publicationName"), rs.getFloat("Price"),rs.getDate("publicationDate"));
//				       String t="Name:"+rs.getString("publicationName")+"  price:"+rs.getFloat("Price");
//				       textArea.append(t+"\n");
//				       
//				       System.out.println("Name:"+rs.getString("publicationName")+"  price:"+rs.getFloat("Price"));
//				      
//				      PublicationList.add(p);
//				           
//				     
//				      }
//				
//				 
//				 }
//		
//				catch (SQLException sqle)
//				{
//					System.err.println("Error with list:\n"+sqle.toString());
//					
//	
//					 
//				}
//
//	 
//	
//	
//}
//public void check_bill(String user,int month){
//	 boolean b=false;
//	 List<Bill2> bill= new ArrayList();
//		try
//		{
//			 String select="select concat(a.lastName,'',a.firstName)as customer_name ,a.phoneNumber,concat(a.houseAddress,'',a.streetAddress)as address , c.totalPrice ,b.publicationName,b.price,d.deliveryDate from publication as b,customer as a ,BILL as c, SUBSCRIPTION as d where a.customerID=c.customerID  and c.subscriptionID=d.subscriptionID and d.publicationID=b.publicationID  and month(deliveryDate)="+month+" and a.firstName='"+user+"'";
//				rs=stat.executeQuery(select);
//				rsmd = rs.getMetaData();
//				
//				
//				 while(rs.next())
//				    	 
//				     { b=true;
//			      
//				    
//				       Bill2 b1 = new Bill2( rs.getString("customer_name"), rs.getInt("phoneNumber"), rs.getString("address"),rs.getFloat("totalPrice"),rs.getString("publicationName"),rs.getFloat("price"),rs.getDate("deliveryDate"));
//				       System.out.println("Customer name:"+rs.getString("customer_name")+ "  phone number:"+rs.getInt("phoneNumber")+"  address:"+rs.getString("address")+"  totalprice:"+rs.getFloat("totalPrice")+"  publication:"+rs.getString("publicationName")+"  price:"+rs.getFloat("price")+"  date:"+rs.getDate("deliveryDate"));
//				       bill.add(b1);
//                       String t="Customer name:"+rs.getString("customer_name")+"\npublication:"+rs.getString("publicationName")+"  totalprice:"+rs.getFloat("totalPrice");
//                       
//                       textArea1.append(t+"\n");
//				     
//				      }}
//		
//				catch (SQLException sqle)
//				{
//					System.err.println("Error with list:\n"+sqle.toString());
//					
//					 
//				}
//		}
//
///*select concat(a.lastName," ",a.firstName)as customer_name ,a.phoneNumber,concat(a.houseAddress," ",a.streetAddress)as address ,
//c.totalPrice ,b.publicationName,b.price,d.deliveryDate
//from publication as b,customer as a ,BILL as c, SUBSCRIPTION as d
//where a.customerID=1=c.customerID  and c.subscriptionID=d.subscriptionID and
//d.publicationID=b.publicationID  and month(deliveryDate)=3*/
//
//public void bill_month(String customername,int month){
//	 boolean b=false;
//	 List<Bill2> bill= new ArrayList();
//		try
//		{
//			 String select=	"select concat(a.lastName,' ',a.firstName)as customer_name, sum(c.totalPrice) as monthprice from customer as a ,BILL as c  where a.customerID=c.customerID  and a.firstName='"+customername+"'and month(billDate)="+month+" group by a.customerID";
//				 
//				rs=stat.executeQuery(select);
//				rsmd = rs.getMetaData();
//				
//				
//				 while(rs.next())
//				    	 
//				     { b=true;
//			      
//				    
//				       Bill2 b1 = new Bill2( rs.getString("customer_name"),rs.getFloat("monthprice"));
//				       System.out.println("Customer name:"+rs.getString("customer_name")+ "  month total price:"+rs.getFloat("monthprice"));
//				       bill.add(b1);
//String t="Customer name:"+rs.getString("customer_name")+"\n  month totalprice:"+rs.getFloat("monthPrice");
//                       
//                      textArea2.append(t+"\n");
//				           
//				     
//				      }}
//		
//				catch (SQLException sqle)
//				{
//					System.err.println("Error with list:\n"+sqle.toString());
//					
//					 
//				}
//	}
//
//
//
//public void connect(){
//	 try
//		{
//			// Load the JConnector Driver
//			Class.forName("com.mysql.jdbc.Driver");
//			// Specify the DB Name
//			String url="jdbc:mysql://localhost:3306/Newsagent";
//			// Connect to DB using DB URL, Username and password
//			con =  DriverManager.getConnection(url, "root", "123456");
//			//Create a generic statement which is passed to the TestInternalFrame1
//			stat =  con.createStatement();
//			
//		}
//		catch(Exception e)
//		{
//			System.out.println("Error: Failed to connect to database\n" + e.getMessage());
//		}
//     
//		 
//}
//}
//	    	        
//	    	            
