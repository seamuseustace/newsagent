package RenruiTestCase;

import junit.framework.TestCase;
import RenruiTestCase.Publication_Renrui;


public class PublicationTests_Renrui extends TestCase {

	// Test No:1
	// Test Objective: test newspaper type is exist my database and output all .
	// Input: Newspaper that include database.
    // excepted output:True
	public void test1(){
		
		Publication_Renrui first=new Publication_Renrui();
		
		
		try {

			assertSame(true, first.print_publication("NEWSPAPER"));	
		} catch (PublicationExceptionHandler_Renrui e) {

		 e.getMessage();
		}
	}
	
	// Test No:2
		// Test Objective: test magazine type is not exist my database and can't output all .
		// Input: magazine whose type not exist some  database.
	    // excepted output:False
	public void test2(){
		
		Publication_Renrui second=new Publication_Renrui();
		
		
		try {

			assertSame(false, second.print_publication("MAGAZINE"));	
		} catch (PublicationExceptionHandler_Renrui e) {

			 e.getMessage();
		}
	}
	        // Test No:2
			// Test Objective: Publication type should be a string and not null.
			// Input: a null string type
		    // excepted output:False
	public void test3(){
		
		Publication_Renrui third=new Publication_Renrui();
		
		
		try {

	assertSame(false, third.print_publication(""));	
		} catch (PublicationExceptionHandler_Renrui e) {

			 e.getMessage();
		}
	}
	
}
