package SeamusTestCase;
//Seamus Eustace

public class NewsagentExceptionHandler_SE extends Exception {

	String message;

	public NewsagentExceptionHandler_SE(String errMessage) {
		message = errMessage;
	}

	public String getMessage() {
		return message;
	}

}
