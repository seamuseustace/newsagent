package SeamusTestCase;
//Seamus Eustace

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@SuppressWarnings("serial")
class Newsagent_SE {

	public ResultSet rs = null;
	public Connection con = null;
	public Statement stmt = null;

	private void initiate_db_conn() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/newsagentdb";
			con = DriverManager.getConnection(url, "root", "");
			stmt = con.createStatement();
		} catch (Exception e) {
			System.out.print("Failed to initialise DB Connection");
		}
	}

	public boolean viewValidCustInvoice(String customerID, String received) {
		try {
			initiate_db_conn();
			String findCustomer = "SELECT * FROM SUBSCRIPTION WHERE customerID = '" + customerID + "' && received = "
					+ received + ";";
			System.out.println("Customer ID: " + customerID + " has received their subscription, please bill.");

			stmt.executeQuery(findCustomer);
			rs.close();
		} catch (SQLException sqle) {
			System.err.println(sqle.toString());
			return false;
		}
		return true;
	}

	public boolean viewInvalidCustInvoice(String customerID, String received) {
		try {
			initiate_db_conn();
			String findCustomer = "SELECT * FROM SUBSCRIPTION WHERE customerID = '" + customerID + "' && received = "
					+ received + ";";
			System.out.println(
					"Customer ID: " + customerID + " has not received their subscription, no billing required.");

			stmt.executeQuery(findCustomer);
			rs.close();
		} catch (SQLException sqle) {
			System.err.println(sqle.toString());
			return false;
		}
		return true;
	}

	public boolean customerNotExist(String customerID, String received) {
		try {
			initiate_db_conn();
			String findCustomer = "SELECT * FROM SUBSCRIPTION WHERE customerID = '" + customerID + "' && received = "
					+ received + ";";
			System.out.println("No Customer exists.");
			rs = stmt.executeQuery(findCustomer);
			rs.close();

		} catch (SQLException sqle) {
			System.err.println(sqle.toString());
		}
		return false;
	}

	public boolean printCustomer(String customerID, String received) {
		try {
			initiate_db_conn();

			String customerName = "SELECT newsagentdb.CUSTOMER.firstName, newsagentdb.CUSTOMER.lastName"
					+ " FROM newsagentdb.CUSTOMER"
					+ " INNER JOIN newsagentdb.SUBSCRIPTION on newsagentdb.CUSTOMER.customerID = newsagentdb.SUBSCRIPTION.customerID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerID + "'"
					+ " and newsagentdb.SUBSCRIPTION.received = '" + received + "';";

			String billDate = "SELECT newsagentdb.BILL.billDate"
					+ " FROM newsagentdb.BILL"
					+ " WHERE newsagentdb.BILL.customerID = '" + customerID + "';";

			String customerAddress = "SELECT newsagentdb.CUSTOMER.houseAddress, newsagentdb.CUSTOMER.streetAddress"
					+ " FROM newsagentdb.CUSTOMER"
					+ " WHERE newsagentdb.CUSTOMER.customerID = '" + customerID + "';";

			String customerRegion = "SELECT newsagentdb.CUSTOMER.region"
					+ " FROM newsagentdb.CUSTOMER"
					+ " WHERE"
					+ " newsagentdb.CUSTOMER.customerID = '" + customerID + "';";

			String customerEmail = "SELECT newsagentdb.CUSTOMER.email"
					+ " FROM newsagentdb.CUSTOMER"
					+ " WHERE"
					+ " newsagentdb.CUSTOMER.customerID = '" + customerID + "';";

			String customerPhone = "SELECT newsagentdb.CUSTOMER.phoneNumber"
					+ " FROM newsagentdb.CUSTOMER"
					+ " WHERE"
					+ " newsagentdb.CUSTOMER.customerID = '" + customerID + "';";

			String deliverPerson = "SELECT newsagentdb.DELIVERYPERSON.firstName, newsagentdb.DELIVERYPERSON.lastName"
					+ " FROM newsagentdb.SUBSCRIPTION"
					+ " INNER JOIN newsagentdb.DELIVERYPERSON"
					+ " ON newsagentdb.DELIVERYPERSON.deliveryPersonID = newsagentdb.SUBSCRIPTION.deliveryPersonID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerID + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + received + "';";

			String deliverPersonNo = "SELECT newsagentdb.DELIVERYPERSON.deliveryPersonID"
					+ " FROM newsagentdb.SUBSCRIPTION"
					+ " INNER JOIN newsagentdb.DELIVERYPERSON"
					+ " ON newsagentdb.DELIVERYPERSON.deliveryPersonID = newsagentdb.SUBSCRIPTION.deliveryPersonID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerID + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + received + "';";

			String pubName = "SELECT newsagentdb.PUBLICATION.publicationName"
					+ " FROM newsagentdb.PUBLICATION"
					+ " INNER JOIN newsagentdb.SUBSCRIPTION"
					+ " ON newsagentdb.SUBSCRIPTION.publicationID = newsagentdb.PUBLICATION.publicationID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerID + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + received + "';";

			String pubType = "SELECT newsagentdb.PUBLICATION.publicationType"
					+ " FROM newsagentdb.PUBLICATION"
					+ " INNER JOIN newsagentdb.SUBSCRIPTION"
					+ " ON newsagentdb.SUBSCRIPTION.publicationID = newsagentdb.PUBLICATION.publicationID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerID + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + received + "';";

			String pubGenre = "SELECT newsagentdb.PUBLICATION.publicationGenre"
					+ " FROM newsagentdb.PUBLICATION"
					+ " INNER JOIN newsagentdb.SUBSCRIPTION"
					+ " ON newsagentdb.SUBSCRIPTION.publicationID = newsagentdb.PUBLICATION.publicationID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerID + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + received + "';";

			String pubPrice = "SELECT newsagentdb.PUBLICATION.price"
					+ " FROM newsagentdb.PUBLICATION"
					+ " INNER JOIN newsagentdb.SUBSCRIPTION"
					+ " ON newsagentdb.SUBSCRIPTION.publicationID = newsagentdb.PUBLICATION.publicationID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerID + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + received + "';";

			String deliverDate = "SELECT newsagentdb.SUBSCRIPTION.deliveryDate"
					+ " FROM newsagentdb.SUBSCRIPTION"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerID + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + received + "';";

			String deliverFee = "SELECT newsagentdb.BILL.deliveryFee"
					+ " FROM newsagentdb.BILL"
					+ " INNER JOIN newsagentdb.SUBSCRIPTION"
					+ " ON newsagentdb.BILL.customerID = newsagentdb.SUBSCRIPTION.customerID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerID + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + received + "';";

			String totPrice = "SELECT newsagentdb.BILL.totalPrice"
					+ " FROM newsagentdb.BILL"
					+ " INNER JOIN newsagentdb.SUBSCRIPTION"
					+ " ON newsagentdb.BILL.customerID = newsagentdb.SUBSCRIPTION.customerID"
					+ " WHERE"
					+ " newsagentdb.SUBSCRIPTION.customerID = '" + customerID + "'"
					+ " AND newsagentdb.SUBSCRIPTION.received = '" + received + "';";

			rs = stmt.executeQuery(customerName);
			rs = stmt.executeQuery(billDate);
			rs = stmt.executeQuery(customerAddress);
			rs = stmt.executeQuery(customerRegion);
			rs = stmt.executeQuery(customerEmail);
			rs = stmt.executeQuery(customerPhone);
			rs = stmt.executeQuery(deliverPerson);
			rs = stmt.executeQuery(deliverPersonNo);
			rs = stmt.executeQuery(pubName);
			rs = stmt.executeQuery(pubType);
			rs = stmt.executeQuery(pubGenre);
			rs = stmt.executeQuery(pubPrice);
			rs = stmt.executeQuery(deliverDate);
			rs = stmt.executeQuery(deliverFee);
			rs = stmt.executeQuery(totPrice);

			System.out.println("***** Billing Details for " + customerName + " *****");
			System.out.println("Bill Date: " + billDate);
			System.out.println("Customer ID: " + customerID);
			System.out.println("Address: " + customerAddress);
			System.out.println("Region: " + customerRegion);
			System.out.println("Email Address: " + customerEmail);
			System.out.println("Phone Number: " + customerPhone);
			System.out.println("Delivered By: " + "( " + deliverPersonNo + " )" + deliverPerson);
			System.out.println("***** Order Detils *****");
			System.out.println("Publication Name: " + pubName);
			System.out.println("Publication Type: " + pubType);
			System.out.println("Publication Genre: " + pubGenre);
			System.out.println("Publication Price: �" + pubPrice);
			System.out.println("Delivery Date: " + deliverDate);
			System.out.println("Delivery Fee: �" + deliverFee);
			System.out.println("Total Price: �" + totPrice);
			rs.close();

		} catch (SQLException sqle) {
			System.err.println(sqle.toString());
		}
		return true;
	}
}