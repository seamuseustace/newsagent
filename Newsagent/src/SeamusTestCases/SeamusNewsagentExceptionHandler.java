package SeamusTestCases;
//Seamus Eustace

public class SeamusNewsagentExceptionHandler extends Exception {

	String message;

	public SeamusNewsagentExceptionHandler(String errMessage) {
		message = errMessage;
	}

	public String getMessage() {
		return message;
	}

}
