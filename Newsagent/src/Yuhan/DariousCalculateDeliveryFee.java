package Yuhan;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class DariousCalculateDeliveryFee extends JInternalFrame implements ActionListener{

	private JFrame frame;
	private JTextField textField = new JTextField();
	private JTextField textField_1;

	/**
	 * Launch the application.
	 */
	
	public ResultSet rs = null;
	public Connection con = null;
	public Statement stmt = null;
	String cmd = null;
	
	
	//@SuppressWarnings("unused")
	private void initiate_db_conn() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/newsagentdb";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
		} catch (Exception e) {
			System.out.print("Failed to initialise DB Connection");
		}
		

	}
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DariousCalculateDeliveryFee window = new DariousCalculateDeliveryFee();
					window.getFrame().setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public DariousCalculateDeliveryFee() {
		initiate_db_conn();
		initialize();
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		setFrame(new JFrame());
		getFrame().setBounds(100, 100, 450, 300);
		getFrame().setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getFrame().getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("CustomerID");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setBounds(58, 43, 86, 45);
		getFrame().getContentPane().add(lblNewLabel);
		
		textField = new JTextField();
		textField.setFont(new Font("Tahoma", Font.PLAIN, 15));
		textField.setBounds(197, 49, 86, 34);
		getFrame().getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("DeliveryFee");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel_1.setBounds(58, 112, 86, 31);
		getFrame().getContentPane().add(lblNewLabel_1);
		
		textField_1 = new JTextField();
		textField_1.setBounds(197, 109, 86, 34);
		getFrame().getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnNewButton = new JButton("Calculate");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					int ID = Integer.parseInt(textField.getText());
					cmd = "select publicationID from customer where customerID =" +ID;
					ResultSet rs=stmt.executeQuery(cmd);
					System.out.println(rs);
					try {while(rs.next()){
						int a=rs.getInt("publicationID");
						if (a==1) 
						{
							
							textField_1.setText("0.5");
						} 
						else if(a==2) 
						{
							
							textField_1.setText("1");
						}
						else if(a>=3)
						{
							
							textField_1.setText("1.5");	
						}
						else if (a<1){
							textField_1.setText("Null");
						}
					}} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
				catch(Exception e2){
					e2.printStackTrace();
					
					
			}
			}});
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 15));
		btnNewButton.setBounds(127, 183, 159, 34);
		getFrame().getContentPane().add(btnNewButton);}
//	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
	public double calculate2(int publicationid){
		int x=publicationid;
		
		if (x==1) 
		{
			
			return 0.5;
		} 
		else if(x==2) 
		{
			
			return 1.00;
		}
		else if(x>=3)
		{
			
			return 1.50;	
		}
		else{
			return -1;
		}
	}
	public JFrame getFrame() {
		return frame;
	}
	public void setFrame(JFrame frame) {
		this.frame = frame;
	}
}
