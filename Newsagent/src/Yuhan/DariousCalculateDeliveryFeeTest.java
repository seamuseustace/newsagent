package Yuhan;

import junit.framework.TestCase;

public class DariousCalculateDeliveryFeeTest extends TestCase {
	// ******************************************************************************
	// *																			*
	// * Test No: 1 																*
	// * Object: one publication cost 0.5 euro delivery fee.         				*
	// * Inputs: 	1																*
	// * 		 					 												*
	// * Expected Output: 0.5 														*
	// * 																			*
	// ******************************************************************************
	public void test001() {
		assertEquals(0.5,new DariousCalculateDeliveryFee().calculate2(1),0.00);
	}
	
	    // ******************************************************************************
		// *																			*
		// * Test No: 2 																*
		// * Object: one publication cost 1 euro delivery fee.         				*
		// * Inputs: 	2																*
		// * 		 					 												*
		// * Expected Output: 1 														*
		// * 																			*
		// ******************************************************************************
	public void test002() {
		assertEquals(1,new DariousCalculateDeliveryFee().calculate2(2),0.00);
	}
	
    // ******************************************************************************
	// *																			*
	// * Test No: 3 																*
	// * Object: one publication cost 1.5 euro delivery fee.         				*
	// * Inputs: 	3																*
	// * 		 					 												*
	// * Expected Output: 1.5 														*
	// * 																			*
	// ******************************************************************************
	public void test003() {
		assertEquals(1.5,new DariousCalculateDeliveryFee().calculate2(3),0.00);
	}
	
    // ******************************************************************************
	// *																			*
	// * Test No: 4 																*
	// * Object: no publication have no delivery fee.         			        	*
	// * Inputs: 	0																*
	// * 		 					 												*
	// * Expected Output: -1 (return -1 means input wrong number)					*
	// * 																			*
	// ******************************************************************************
	public void test004() {
		assertEquals(-1,new DariousCalculateDeliveryFee().calculate2(0),0.00);
	}
    // ******************************************************************************
	// *																			*
	// * Test No: 5  																*
	// * Object: if the publication quantity more than 3.         			    	*
	// * Inputs: 	4 or more than 3												*
	// * 		 					 												*
	// * Expected Output: 1.5														*
	// * 																			*
	// ******************************************************************************
	public void test005() {
		assertEquals(1.5,new DariousCalculateDeliveryFee().calculate2(4),0.00);
	}
    // ******************************************************************************
	// *																			*
	// * Test No: 6 																*
	// * Object: input wrong number of publication .         				*
	// * Inputs: 	-4																*
	// * 		 					 												*
	// * Expected Output: -1 														*
	// * 																			*
	// ******************************************************************************
	public void test006() {
		assertEquals(-1,new DariousCalculateDeliveryFee().calculate2(-4),0.00);
	}
}
