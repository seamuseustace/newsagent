package Yuhan;

import java.awt.Container;
import java.awt.EventQueue;
import java.awt.Window;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.awt.event.ActionEvent;

public class DariousGUI1 implements ActionListener {

	private JFrame frame;
	private Connection con = null;
	private Statement stmt = null;

	private Container content;
	private JButton logoutButton = new JButton("Logout");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DariousGUI1 window = new DariousGUI1();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public void initiate_db_conn() {
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/Newsagent";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "admin");
			// Create a generic statement which is passed to the
			// TestInternalFrame1
			stmt = con.createStatement();
			JOptionPane.showMessageDialog(null, "Connected to database successfully.", "Successful",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n" + e.getMessage());
			JOptionPane.showMessageDialog(null, "Failed to connect to the database.", "Error Connecting to Database",
					JOptionPane.ERROR_MESSAGE);
		}
	}
	/**
	 * Create the application.
	 */
	public DariousGUI1() {
		initialize();
		
		
		
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JButton btnButton = new JButton("logout");
		btnButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				logout();
			}
		});
		btnButton.setBounds(166, 118, 89, 23);
		frame.getContentPane().add(btnButton);
	}

	public boolean logout() {
		frame.dispose();
		return true;
	}

//	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		
	}
}
