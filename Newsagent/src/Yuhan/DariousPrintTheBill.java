package Yuhan;

import java.awt.EventQueue;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JFrame;
import javax.swing.AbstractButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.awt.event.ActionEvent;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class DariousPrintTheBill {

	private JFrame frame;
	public ResultSet rs = null;
	public Connection con = null;
	public Statement stmt = null;
	String cmd = null;
	private JTextField txtCustomerid;
	private JLabel lblNewLabel;
	private JButton btnNewButton;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					DariousPrintTheBill window = new DariousPrintTheBill();
					window.frame.setVisible(true);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	private void initiate_db_conn() {
		try {
			Class.forName("com.mysql.jdbc.Driver");
			String url = "jdbc:mysql://localhost:3306/newsagentdb";
			con = DriverManager.getConnection(url, "root", "admin");
			stmt = con.createStatement();
		} catch (Exception e) {
			System.out.print("Failed to initialise DB Connection");
		}
		

	}
	/**
	 * Create the application.
	 */
	public DariousPrintTheBill() {
		initiate_db_conn();
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		
		
		txtCustomerid = new JTextField();
		txtCustomerid.setBounds(216, 72, 117, 37);
		frame.getContentPane().add(txtCustomerid);
		txtCustomerid.setColumns(10);
		
		lblNewLabel = new JLabel("CustomerID:");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblNewLabel.setBounds(91, 70, 117, 37);
		frame.getContentPane().add(lblNewLabel);
		btnNewButton=new JButton("Print the bell");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try{
					int ID = Integer.parseInt(txtCustomerid.getText());
					System.out.println(ID);
					cmd = "select * from bill where customerID =" +ID;
					rs=stmt.executeQuery(cmd);
					System.out.println(rs);
					writeToFile(rs);}
				catch (SQLException e1) {
						e1.printStackTrace();
					}
			}
		});
		
		
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 14));
		btnNewButton.setBounds(216, 135, 117, 37);
		frame.getContentPane().add(btnNewButton);
	}

	protected void writeToFile(ResultSet rs) {
		// TODO Auto-generated method stub
		try{
			System.out.println("In writeToFile");
			FileWriter outputFile = new FileWriter("CustomerBill.csv");
			PrintWriter printWriter = new PrintWriter(outputFile);
			ResultSetMetaData rsmd = rs.getMetaData();
			int numColumns = rsmd.getColumnCount();

			for(int i=0;i<numColumns;i++){
				printWriter.print(rsmd.getColumnLabel(i+1)+",");
			}
			printWriter.print("\n");
			while(rs.next()){
				for(int i=0;i<numColumns;i++){
					printWriter.print(rs.getString(i+1)+",");
				}
				printWriter.print("\n");
				printWriter.flush();
			}
			printWriter.close();
		}
		catch(Exception e){e.printStackTrace();}
		
	}

}
