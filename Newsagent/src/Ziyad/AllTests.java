package Ziyad;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CustomerJunitTest.class, CustomerTest.class,
		DeliveryPersonJunitTest.class, DeliveryTest.class })
public class AllTests {

}
