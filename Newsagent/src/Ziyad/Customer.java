package Ziyad;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Customer {
	private static int customerID;
	private int publicationID;
	private String firstName;
	private String lastName;
	private String houseAddress;
	private String streetAddress;
	private String region;
	private String holidayDate;
	private String email;
	private String phoneNumber;
	private String status;

	public void customerID(int CustomerID) {
		this.setCustomerID(++CustomerID); // this.id +=1；may be better
		System.out.println(this.getCustomerID());

	}

	public int getpublicationID() {
		return publicationID;
	}

	public void setStatus(String s) {
		status = s;
	}

	public String getStatus() {
		return status;
	}

	public void setpublicationID(int PublicationID) {
		this.publicationID = PublicationID;
	}

	public String getfirstName() {
		return firstName;
	}

	public void setfirstName(String FirstName) {
		this.firstName = FirstName;
	}

	public String getlastName() {
		return lastName;
	}

	public void setlastName(String LastName) {
		this.lastName = LastName;
	}

	public String gethouseAddress() {
		return houseAddress;
	}

	public void sethouseAddress(String HouseAddress) {
		this.houseAddress = HouseAddress;
	}

	public String getstreetAddress() {
		return streetAddress;
	}

	public void setstreetAddress(String StreetAddress) {
		this.streetAddress = StreetAddress;
	}

	public String getregion() {
		return region;
	}

	public void setregion(String Region) {
		this.region = Region;
	}

	public String getholidayDate() {
		return holidayDate;
	}

	public void setholidayDate(String HolidayDate) {
		this.holidayDate = HolidayDate;
	}

	public String getemail() {
		return email;
	}

	public void setemail(String Email) {
		this.email = Email;
	}

	public String getphoneNumber() {
		return phoneNumber;
	}

	public void setphoneNumber(String PhoneNumber) {
		this.phoneNumber = PhoneNumber;
	}

	public static int getCustomerID() {
		return customerID;
	}

	public void setCustomerID(int customerID) {
		Customer.customerID = customerID;

	}

	public String toString() {
		return this.customerID + "\t" + this.publicationID + "\t"
				+ this.firstName + "\t" + this.lastName + "\t" + this.region
				+ "\t" + this.email;
	}

	public boolean save(Connection con) {
		boolean result = false;
		// the mysql insert statement
		String query = " insert into customer (publicationID, firstName,lastName,houseAddress,streetAddress,region,email,holidayDate,phoneNumber,status)"
				+ " values (?, ?, ?, ?, ?, ? ,? ,? ,?,?)";
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		java.sql.Date sql = null;
		java.util.Date parsed;
		try {
			parsed = format.parse(getholidayDate());
			sql = new java.sql.Date(parsed.getTime());
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// create the mysql insert preparedstatement
		PreparedStatement preparedStmt;
		try {
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setInt(1, getpublicationID());
			preparedStmt.setString(2, getfirstName());
			preparedStmt.setString(3, getlastName());
			preparedStmt.setString(4, gethouseAddress());
			preparedStmt.setString(5, getstreetAddress());
			preparedStmt.setString(6, getregion());
			preparedStmt.setString(7, getemail());
			preparedStmt.setDate(8, sql);
			preparedStmt.setString(9, getphoneNumber());
			preparedStmt.setString(10, getStatus());

			// execute the preparedstatement
			preparedStmt.execute();
			result = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;

	}

	public boolean update(Connection con) {
		boolean result = false;

		String query = "update customer set publicationID = ?,  firstName = ?,lastName = ?, houseAddress = ?, streetAddress = ?, region = ?,email = ?,"
				+ "holidayDate = ?, phoneNumber = ?,status = ? where customerID = ?";
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
		java.sql.Date sql = null;
		java.util.Date parsed;
		try {
			parsed = format.parse(getholidayDate());
			sql = new java.sql.Date(parsed.getTime());
		} catch (ParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		PreparedStatement preparedStmt;
		try {
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setInt(1, getpublicationID());
			preparedStmt.setString(2, getfirstName());
			preparedStmt.setString(3, getlastName());
			preparedStmt.setString(4, gethouseAddress());
			preparedStmt.setString(5, getstreetAddress());
			preparedStmt.setString(6, getregion());
			preparedStmt.setString(7, getemail());
			preparedStmt.setDate(8, sql);
			preparedStmt.setString(9, getphoneNumber());
			preparedStmt.setString(10, getStatus());
			preparedStmt.setInt(11, getCustomerID());

			// execute the preparedstatement
			preparedStmt.execute();
			result = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;
	}

}