package Ziyad;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import Ziyad.Customer;

public class CustomerFrame extends JFrame implements ActionListener {

	private JTextField publicationText, fnameText, lnameText, houseaddText,
			streetaddText, regionText, emailText, hdateText, phoneText;
	private JButton save, cancel, update;
	private JRadioButton active, inactive;
	ButtonGroup bG;
	Customer customer = null;
	Connection con = null;

	public void createFrame() {

		add(new JLabel("Publication ID:"));
		add(publicationText = new JTextField(9));

		add(new JLabel("First Name:"));
		add(fnameText = new JTextField(9));

		add(new JLabel("Last Name:"));
		add(lnameText = new JTextField(9));

		add(new JLabel("House Address:"));
		add(houseaddText = new JTextField(9));

		add(new JLabel("Street Address:"));
		add(streetaddText = new JTextField(9));

		add(new JLabel("Region:"));
		add(regionText = new JTextField(9));

		add(new JLabel("Email:"));
		add(emailText = new JTextField(9));

		add(new JLabel("Holiday Date:"));
		add(hdateText = new JTextField(9));

		add(new JLabel("Phone Number:"));
		add(phoneText = new JTextField(9));

		add(new JLabel("Status:"));

		JPanel t = new JPanel(new GridLayout(1, 2));
		active = new JRadioButton("Active");
		inactive = new JRadioButton("InActice");
		bG = new ButtonGroup();
		bG.add(active);
		bG.add(inactive);
		t.add(active);
		t.add(inactive);
		active.setSelected(true);
		add(t);

	}

	public CustomerFrame(Connection con) {
		this.con = con;
		createFrame();
		setLayout(new GridLayout(12, 2));

		add(cancel = new JButton("Cancel"));
		add(save = new JButton("Add"));
		cancel.addActionListener(this);
		save.addActionListener(this);

		setSize(350, 400);
		setVisible(true);
	}

	public CustomerFrame(Connection con, Customer c) {
		this.con = con;
		this.customer = c;
		createFrame();
		setLayout(new GridLayout(12, 2));

		publicationText.setText("" + customer.getpublicationID());
		emailText.setText(customer.getemail());
		fnameText.setText(customer.getfirstName());
		lnameText.setText(customer.getlastName());
		hdateText.setText(customer.getholidayDate());
		regionText.setText(customer.getregion());
		houseaddText.setText(customer.gethouseAddress());
		phoneText.setText(customer.getphoneNumber());
		streetaddText.setText(customer.getstreetAddress());
		if (customer.getStatus().equals("active"))
			active.setSelected(true);
		else
			inactive.setSelected(true);
		add(cancel = new JButton("Cancel"));
		add(update = new JButton("Update"));
		cancel.addActionListener(this);
		update.addActionListener(this);

		setSize(350, 400);
		setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == save) {
			customer = new Customer();
			customer.setemail(emailText.getText());
			customer.setfirstName(fnameText.getText());
			customer.setlastName(lnameText.getText());
			customer.setholidayDate(hdateText.getText());
			customer.setpublicationID(Integer.parseInt(publicationText
					.getText()));
			customer.setregion(regionText.getText());
			customer.setphoneNumber(phoneText.getText());
			customer.sethouseAddress(houseaddText.getText());
			customer.setstreetAddress(streetaddText.getText());

			if (active.isSelected())
				customer.setStatus(active.getText());
			else
				customer.setStatus(inactive.getText());
			customer.save(con);
			JOptionPane.showMessageDialog(null, "Successfully saved.",
					"Successful", JOptionPane.INFORMATION_MESSAGE);
		}
		if (e.getSource() == update) {

			customer.setemail(emailText.getText());
			customer.setfirstName(fnameText.getText());
			customer.setlastName(lnameText.getText());
			customer.setholidayDate(hdateText.getText());
			customer.setpublicationID(Integer.parseInt(publicationText
					.getText()));
			customer.setregion(regionText.getText());
			customer.setphoneNumber(phoneText.getText());
			customer.sethouseAddress(houseaddText.getText());
			customer.setstreetAddress(streetaddText.getText());
			if (active.isSelected())
				customer.setStatus(active.getText());
			else
				customer.setStatus(inactive.getText());
			customer.update(con);
			JOptionPane.showMessageDialog(null, "Successfully saved.",
					"Successful", JOptionPane.INFORMATION_MESSAGE);
		}
		if (e.getSource() == cancel)
			this.dispose();
	}

	public Customer getCustomer() {
		// TODO Auto-generated method stub
		return customer;
	}
}
