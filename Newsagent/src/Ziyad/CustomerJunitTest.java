package Ziyad;

import java.util.ArrayList;

import junit.framework.TestCase;

import org.junit.Test;
import static org.junit.Assert.*;

import Ziyad.Customer;

public class CustomerJunitTest extends TestCase{
	ArrayList<Customer> clist = new ArrayList<Customer>();

	public CustomerJunitTest() {

		// assigning the values

		Customer c = new Customer();
		c.setfirstName("Ziyad");
		c.setlastName("Alotaibi");
		c.setemail("Zeedo.Ireland@gmail.com");
		c.setholidayDate("25112015");
		c.sethouseAddress("2 The Weir");
		c.setstreetAddress("Cityquarter");
		c.setphoneNumber("0834647910");
		c.setpublicationID(1);
		c.setregion("North");
		clist.add(c);

	}

	@Test
	public void testFirstName() {
		String fname = clist.get(0).getfirstName();
		assertTrue(fname == "Ziyad");
	}

	@Test
	public void testLastName() {
		String fname = clist.get(0).getlastName();
		assertTrue(fname == "Alotaibi");
	}

	@Test
	public void testEmail() {
		String fname = clist.get(0).getemail();
		assertTrue(fname == "Zeedo.Ireland@gmail.com");
	}

	@Test
	public void testPhone() {
		String fname = clist.get(0).getphoneNumber();
		assertTrue(fname == "0834647910");
	}

	@Test
	public void testPublication() {
		int pid = clist.get(0).getpublicationID();
		assertTrue(pid == 1);
	}

	@Test
	public void testHouseAddress() {
		String fname = clist.get(0).gethouseAddress();
		assertTrue(fname == "2 The Weir");
	}

	@Test
	public void testStreetAddress() {
		String fname = clist.get(0).getstreetAddress();
		assertTrue(fname == "Cityquarter");
	}

	@Test
	public void testHolidayDate() {
		String fname = clist.get(0).getholidayDate();
		assertTrue(fname == "25112015");
	}

	@Test
	public void testRegion() {
		String fname = clist.get(0).getregion();
		assertTrue(fname == "North");
	}

	@Test
	public void testupdateFirstName() {
		clist.get(0).setfirstName("Ziyad");
		String fname = clist.get(0).getfirstName();
		assertTrue(fname == "Ziyad");
	}

	@Test
	public void testupdateLastName() {
		clist.get(0).setlastName("Alotaibi");
		String lname = clist.get(0).getlastName();
		assertTrue(lname == "Alotaibi");
	}

	@Test
	public void testupdateEmail() {
		clist.get(0).setemail("Zeedo_ireland@yahoo.com");
		String email = clist.get(0).getemail();
		assertTrue(email == "Zeedo_ireland@yahoo.com");
	}

	@Test
	public void testupdatePhone() {
		clist.get(0).setphoneNumber("087777778");
		String p = clist.get(0).getphoneNumber();
		assertTrue(p == "087777778");
	}

	@Test
	public void testupdatePublication() {
		clist.get(0).setpublicationID(2);
		int pid = clist.get(0).getpublicationID();
		assertTrue(pid == 2);
	}

	@Test
	public void testupdateHouseAddress() {
		clist.get(0).sethouseAddress("Main Street");
		String h = clist.get(0).gethouseAddress();
		assertTrue(h == "Main Street");
	}

	@Test
	public void testupdateStreetAddress() {
		clist.get(0).setstreetAddress("2 Kildare House");
		String s = clist.get(0).getstreetAddress();
		assertTrue(s == "2 Kildare House");
	}

	@Test
	public void testupdateHolidayDate() {
		clist.get(0).setholidayDate("20111025");
		String d = clist.get(0).getholidayDate();
		assertTrue(d == "20111025");
	}

	@Test
	public void testupdateRegion() {
		clist.get(0).setregion("XYZ");
		String r = clist.get(0).getregion();
		assertTrue(r == "XYZ");
	}
}
