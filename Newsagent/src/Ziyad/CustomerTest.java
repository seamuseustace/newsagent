package Ziyad;

import Ziyad.Customer;
import junit.framework.*;

public class CustomerTest extends TestCase {

	Customer customer = new Customer();

	// assigning the values
	protected void setUp() {
		customer.setfirstName("Mick");
		customer.setlastName("Alen");
		customer.setpublicationID(98456);
		customer.setCustomerID(1);
		customer.setemail("abc@gmail.com");
		customer.setregion("North");
		customer.setStatus("active");
	}

	public void testFirstName() {
		assertTrue(customer.getfirstName() == "Mick");
	}

	public void testLastName() {
		assertTrue(customer.getlastName() == "Alen");
	}

	public void testPublicationID() {
		assertTrue(customer.getpublicationID() == 98456);
	}

	public void testCustomerID() {
		assertTrue(customer.getCustomerID() == 1);
	}

	public void testEmailID() {
		assertTrue(customer.getemail() == "abc@gmail.com");
	}

	public void testRegion() {
		assertTrue(customer.getregion() == "North");
	}

	public void testStatus() {
		assertTrue(customer.getStatus() == "active");
	}
}