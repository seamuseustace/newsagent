package Ziyad;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import Ziyad.DeliveryPerson;

public class DeliveryFrame extends JFrame implements ActionListener {

	private JTextField fnameText, lnameText, regionText, phoneText;
	private JButton save, cancel, update;
	DeliveryPerson delivery = null;
	Connection con = null;

	public void createFrame() {

		add(new JLabel("First Name:"));
		add(fnameText = new JTextField(9));

		add(new JLabel("Last Name:"));
		add(lnameText = new JTextField(9));

		add(new JLabel("Region:"));
		add(regionText = new JTextField(9));

		add(new JLabel("Phone Number:"));
		add(phoneText = new JTextField(9));

	}

	public DeliveryFrame(Connection con, DeliveryPerson dp) {
		this.con = con;
		delivery = dp;
		setLayout(new GridLayout(11, 2));
		createFrame();
		fnameText.setText(delivery.getfirstName());
		lnameText.setText(delivery.getlastName());
		regionText.setText(delivery.getRegion());
		phoneText.setText(delivery.getphoneNumber());

		add(cancel = new JButton("Cancel"));
		add(update = new JButton("Update"));
		cancel.addActionListener(this);
		update.addActionListener(this);
		setSize(350, 400);
		setVisible(true);
	}

	public DeliveryFrame(Connection con) {
		this.con = con;
		setLayout(new GridLayout(11, 2));

		createFrame();
		add(cancel = new JButton("Cancel"));
		add(save = new JButton("Add"));
		cancel.addActionListener(this);
		save.addActionListener(this);
		setSize(350, 400);
		setVisible(true);
	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == save) {
			delivery = new DeliveryPerson();

			delivery.setfirstName(fnameText.getText());
			delivery.setlastName(lnameText.getText());
			delivery.setregion(regionText.getText());
			delivery.setphoneNumber(phoneText.getText());
			delivery.save(con);
			JOptionPane.showMessageDialog(null, "Successfully saved.",
					"Successful", JOptionPane.INFORMATION_MESSAGE);

		}
		if (e.getSource() == update) {

			delivery.setfirstName(fnameText.getText());
			delivery.setlastName(lnameText.getText());
			delivery.setregion(regionText.getText());
			delivery.setphoneNumber(phoneText.getText());
			delivery.update(con);
			JOptionPane.showMessageDialog(null, "Successfully Updated.",
					"Successful", JOptionPane.INFORMATION_MESSAGE);
		}
		if (e.getSource() == cancel)
			this.dispose();
	}

	public DeliveryPerson getDeliveryPerson() {
		// TODO Auto-generated method stub
		return delivery;
	}
}
