package Ziyad;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DeliveryPerson {
	int deliveryPersonID;
	String firstName;
	String lastName;
	String phoneNumber;
	String region;

	public void setID(int id) {
		deliveryPersonID = id;
	}

	public int getDPersonID() {
		return deliveryPersonID;
	}

	public void setfirstName(String text) {
		firstName = text;

	}

	public String getfirstName() {
		return firstName;
	}

	public void setlastName(String text) {
		lastName = text;

	}

	public String getlastName() {
		return lastName;
	}

	public void setregion(String text) {
		region = text;

	}

	public String getRegion() {
		return region;
	}

	public void setphoneNumber(String text) {
		phoneNumber = text;
	}

	public String getphoneNumber() {
		return phoneNumber;
	}

	public String toString() {
		return this.deliveryPersonID + "\t" + this.firstName + "\t"
				+ this.lastName + "\t" + this.phoneNumber + "\t" + this.region;
	}

	public boolean save(Connection con) {
		boolean result = false;
		// the mysql insert statement
		String query = " insert into deliveryperson (firstName,lastName,phoneNumber,region)"
				+ " values (?, ?, ?, ?)";

		// create the mysql insert preparedstatement
		PreparedStatement preparedStmt;
		try {
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1, getfirstName());
			preparedStmt.setString(2, getlastName());
			preparedStmt.setString(3, getphoneNumber());
			preparedStmt.setString(4, getRegion());
			// execute the preparedstatement
			preparedStmt.execute();
			result = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;

	}

	public boolean update(Connection con) {
		boolean result = false;

		String query = "update deliveryperson set firstName = ?,lastName = ?, phoneNumber = ?, region = ? where deliverypersonID = ?";

		PreparedStatement preparedStmt;
		try {
			preparedStmt = con.prepareStatement(query);
			preparedStmt.setString(1, getfirstName());
			preparedStmt.setString(2, getlastName());
			preparedStmt.setString(3, getphoneNumber());
			preparedStmt.setString(4, getRegion());
			preparedStmt.setInt(5, getDPersonID());
			// execute the preparedstatement
			preparedStmt.execute();
			result = true;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			// e.printStackTrace();
		}
		return result;
	}
}
