package Ziyad;

import java.util.ArrayList;
import Ziyad.DeliveryPerson;
import org.junit.Test;

import static org.junit.Assert.*;

public class DeliveryPersonJunitTest {
	ArrayList<DeliveryPerson> clist = new ArrayList<DeliveryPerson>();

	public DeliveryPersonJunitTest() {

		// assigning the values

		DeliveryPerson c = new DeliveryPerson();
		c.setfirstName("John");
		c.setlastName("MacCormack");
		c.setphoneNumber("0876543210");
		c.setregion("South");
		clist.add(c);

	}

	@Test
	public void testFirstName() {
		String fname = clist.get(0).getfirstName();
		assertTrue(fname == "John");
	}

	@Test
	public void testLastName() {
		String fname = clist.get(0).getlastName();
		assertTrue(fname == "MacCormack");
	}

	@Test
	public void testPhone() {
		String fname = clist.get(0).getphoneNumber();
		assertTrue(fname == "0876543210");
	}

	@Test
	public void testRegion() {
		String fname = clist.get(0).getRegion();
		assertTrue(fname == "South");
	}

	@Test
	public void testupdateFirstName() {
		clist.get(0).setfirstName("John");
		String fname = clist.get(0).getfirstName();
		assertTrue(fname == "John");
	}

	@Test
	public void testupdateLastName() {
		clist.get(0).setlastName("MacCormack");
		String lname = clist.get(0).getlastName();
		assertTrue(lname == "MacCormack");
	}

	@Test
	public void testupdatePhone() {
		clist.get(0).setphoneNumber("0877654567");
		String p = clist.get(0).getphoneNumber();
		assertTrue(p == "0877654567");
	}

	@Test
	public void testupdateRegion() {

		String r = clist.get(0).getRegion();
		assertTrue(r == "South");
	}
}
