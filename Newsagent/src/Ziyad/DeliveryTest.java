package Ziyad;

import Ziyad.DeliveryPerson;
import junit.framework.*;

public class DeliveryTest extends TestCase {

	DeliveryPerson dp = new DeliveryPerson();

	// assigning the values
	protected void setUp() {
		dp.setfirstName("Tony");
		dp.setlastName("Alen");
		dp.setID(1);
		dp.setphoneNumber("+353834647910");
		dp.setregion("North");
	}

	public void testFirstName() {
		assertTrue(dp.getfirstName() == "Tony");
	}

	public void testLastName() {
		assertTrue(dp.getlastName() == "Alen");
	}

	public void testID() {
		assertTrue(dp.getDPersonID() == 1);
	}

	public void testPhone() {
		assertTrue(dp.getphoneNumber() == "+353834647910");
	}

	public void testRegion() {
		assertTrue(dp.getRegion() == "North");
	}

}