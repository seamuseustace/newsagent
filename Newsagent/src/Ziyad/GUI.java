package Ziyad;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import Ziyad.Customer;
import Ziyad.DeliveryPerson;
import Ziyad.Queries;

public class GUI extends JFrame implements ActionListener {
	private Connection con = null;
	private JButton viewCustomer, newCustomer, updateCustomer, removeCustomer,
			newDelivery, updateDelivery, removeDelivery, viewDelivery;

	public GUI() {
		initiate_db_conn();

		JPanel main = new JPanel(new GridLayout(3, 1));

		JPanel customerPanel = new JPanel(new GridLayout(1, 4));
		customerPanel.setBorder(BorderFactory
				.createTitledBorder("Customer Actions"));
		customerPanel.add(viewCustomer = new JButton("View"));
		customerPanel.add(newCustomer = new JButton("New"));
		customerPanel.add(updateCustomer = new JButton("Update"));
		customerPanel.add(removeCustomer = new JButton("Remove"));
		viewCustomer.addActionListener(this);
		newCustomer.addActionListener(this);
		updateCustomer.addActionListener(this);
		removeCustomer.addActionListener(this);

		main.add(customerPanel);

		JPanel deliveryPanel = new JPanel(new GridLayout(1, 4));
		deliveryPanel.setBorder(BorderFactory
				.createTitledBorder("Delivery Actions"));
		deliveryPanel.add(viewDelivery = new JButton("View"));
		deliveryPanel.add(newDelivery = new JButton("New"));
		deliveryPanel.add(updateDelivery = new JButton("Update"));
		deliveryPanel.add(removeDelivery = new JButton("Remove"));
		viewDelivery.addActionListener(this);
		newDelivery.addActionListener(this);
		updateDelivery.addActionListener(this);
		removeDelivery.addActionListener(this);

		main.add(deliveryPanel);

		add(main);

		setSize(500, 200);
		setVisible(true);
	}

	public void initiate_db_conn() {
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/NewsagentDB";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "admin");
			// Create a generic statement which is passed to the
			// TestInternalFrame1

			JOptionPane.showMessageDialog(null,
					"Connected to database successfully.", "Successful",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n"
					+ e.getMessage());
			JOptionPane.showMessageDialog(null,
					"Failed to connect to the database.",
					"Error Connecting to Database", JOptionPane.ERROR_MESSAGE);
		}
	}

	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if (e.getSource() == viewCustomer) {
			Queries q = new Queries();
			ArrayList<Customer> c = q.getAllCustomers(con);
			JFrame frame = new JFrame("All Customers");
			String records = "CustomerID\tPublicationID\t"
					+ "FirstName\tLastName\tRegion\tEmail\n";
			for (int i = 0; i < c.size(); i++)
				records += c.get(i).toString() + "\n";

			JTextArea textArea = new JTextArea(6, 50);
			textArea.setText(records);
			textArea.setEditable(false);

			// wrap a scrollpane around it
			JScrollPane scrollPane = new JScrollPane(textArea);

			// display them in a message dialog
			JOptionPane.showMessageDialog(frame, scrollPane);
		}
		if (e.getSource() == viewDelivery) {
			Queries q = new Queries();
			ArrayList<DeliveryPerson> c = q.getAllDeliveryPersons(con);
			JFrame frame = new JFrame("All DeliveryPersons");
			String records = "DeliveryID\t"
					+ "FirstName\tLastName\tPhone\tRegion\n";
			for (int i = 0; i < c.size(); i++)
				records += c.get(i).toString() + "\n";

			JTextArea textArea = new JTextArea(6, 40);
			textArea.setText(records);
			textArea.setEditable(false);

			// wrap a scrollpane around it
			JScrollPane scrollPane = new JScrollPane(textArea);

			// display them in a message dialog
			JOptionPane.showMessageDialog(frame, scrollPane);
		}
		if (e.getSource() == newCustomer) {
			new CustomerFrame(con);

		}
		if (e.getSource() == newDelivery) {
			new DeliveryFrame(con);
		}
		if (e.getSource() == removeCustomer) {
			Queries q = new Queries();
			ArrayList<Customer> c = q.getAllCustomers(con);
			if (c.size() > 0) {
				String[] list = new String[c.size()];
				for (int i = 0; i < c.size(); i++)
					list[i] = String.valueOf(c.get(i).getCustomerID()) + "|"
							+ c.get(i).getfirstName();
				JComboBox jcb = new JComboBox(list);
				jcb.setEditable(false);
				JOptionPane.showMessageDialog(null, jcb, "select customer",
						JOptionPane.QUESTION_MESSAGE);
				int index = jcb.getSelectedIndex();
				q.removeCustomer(c.get(index).getCustomerID(), con);
				JOptionPane.showMessageDialog(null, "Removed");
			} else {
				JOptionPane.showMessageDialog(null, "No Record Exist.");
			}
		}
		if (e.getSource() == removeDelivery) {
			Queries q = new Queries();
			ArrayList<DeliveryPerson> c = q.getAllDeliveryPersons(con);
			if (c.size() > 0) {
				String[] list = new String[c.size()];
				for (int i = 0; i < c.size(); i++)
					list[i] = String.valueOf(c.get(i).getDPersonID()) + "|"
							+ c.get(i).getfirstName();
				JComboBox jcb = new JComboBox(list);
				jcb.setEditable(false);
				JOptionPane.showMessageDialog(null, jcb,
						"select deliveryperson", JOptionPane.QUESTION_MESSAGE);
				int index = jcb.getSelectedIndex();

				q.removeDeliveryPerson(c.get(index).getDPersonID(), con);
				JOptionPane.showMessageDialog(null, "Removed");
			} else {
				JOptionPane.showMessageDialog(null, "No Record Exist.");
			}

		}
		if (e.getSource() == updateDelivery) {
			Queries q = new Queries();
			ArrayList<DeliveryPerson> c = q.getAllDeliveryPersons(con);
			if (c.size() > 0) {
				String[] list = new String[c.size()];
				for (int i = 0; i < c.size(); i++)
					list[i] = String.valueOf(c.get(i).getDPersonID()) + "|"
							+ c.get(i).getfirstName();
				JComboBox jcb = new JComboBox(list);
				jcb.setEditable(false);
				JOptionPane.showMessageDialog(null, jcb,
						"select deliveryperson", JOptionPane.QUESTION_MESSAGE);
				int index = jcb.getSelectedIndex();

				DeliveryFrame df = new DeliveryFrame(con, c.get(index));
			} else {
				JOptionPane.showMessageDialog(null, "No Record Exist.");
			}
		}
		if (e.getSource() == updateCustomer) {
			Queries q = new Queries();
			ArrayList<Customer> c = q.getAllCustomers(con);
			if (c.size() > 0) {
				String[] list = new String[c.size()];
				for (int i = 0; i < c.size(); i++)
					list[i] = String.valueOf(c.get(i).getCustomerID()) + "|"
							+ c.get(i).getfirstName();
				JComboBox jcb = new JComboBox(list);
				jcb.setEditable(false);
				JOptionPane.showMessageDialog(null, jcb, "select customer",
						JOptionPane.QUESTION_MESSAGE);
				int index = jcb.getSelectedIndex();

				CustomerFrame cf = new CustomerFrame(con, c.get(index));
			} else {
				JOptionPane.showMessageDialog(null, "No Record Exist.");
			}
		}
	}

}
