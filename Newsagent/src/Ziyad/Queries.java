package Ziyad;


import java.sql.*;
import java.util.ArrayList;

public class Queries {

	public void removeCustomer(int id, Connection conn) {
		try {
			String query = "delete from customer where customerID = ?";
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setInt(1, id);
			preparedStmt.execute();

		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
	}

	public ArrayList<Customer> getAllCustomers(Connection conn) {
		Statement statement;
		String query = "SELECT * FROM customer";
		ArrayList<Customer> list = new ArrayList<Customer>();
		Customer c = null;
		ResultSet rs = null;
		try {

			statement = conn.createStatement();
			rs = statement.executeQuery(query);
			while (rs.next()) {
				c = new Customer();

				c.setCustomerID(rs.getInt("customerID"));
				c.setpublicationID(rs.getInt("publicationID"));
				c.setfirstName(rs.getString("firstName"));
				c.setlastName(rs.getString("lastName"));
				c.sethouseAddress(rs.getString("houseAddress"));
				c.setstreetAddress(rs.getString("streetAddress"));
				c.setregion(rs.getString("region"));
				c.setemail(rs.getString("email"));
				c.setholidayDate(rs.getDate("holidayDate").toString());
				c.setphoneNumber(rs.getString("phoneNumber"));
				c.setStatus(rs.getString("status"));
				list.add(c);
			}
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
		return list;
	}

	public void removeDeliveryPerson(int id, Connection conn) {
		try {
			String query = "delete from deliveryperson where deliverypersonID = ?";
			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setInt(1, id);
			preparedStmt.execute();

		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}

	}

	public ArrayList<DeliveryPerson> getAllDeliveryPersons(Connection conn) {
		Statement statement;
		String query = "SELECT * FROM deliveryperson";
		ArrayList<DeliveryPerson> list = new ArrayList<DeliveryPerson>();
		DeliveryPerson dp = null;
		ResultSet rs = null;
		try {

			statement = conn.createStatement();
			rs = statement.executeQuery(query);
			while (rs.next()) {
				dp = new DeliveryPerson();

				dp.setID(rs.getInt("deliverypersonID"));
				dp.setfirstName(rs.getString("firstName"));
				dp.setlastName(rs.getString("lastName"));
				dp.setphoneNumber(rs.getString("phoneNumber"));
				dp.setregion(rs.getString("region"));

				list.add(dp);
			}
		} catch (Exception e) {
			System.err.println("Got an exception! ");
			System.err.println(e.getMessage());
		}
		return list;
	}
}
